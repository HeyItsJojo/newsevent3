% Categorical Trainer V1 - File for training with preprocessed data
% Data: pre-processed, labeled
% Model: binary categorical output
% Example: https://www.mathworks.com/help/textanalytics/ug/prepare-text-data-for-analysis.html#PrepareTextDataForAnalysisExample-4
% There was 20x more non bio subject than bio subject, so i duplicated bio
% data 20x. This made the model more aware of the KWs in bio subjects

model_name = "binary_cat1";

tic
if ~exist('data','var')
    disp('loading data');
    dir = "C:\Users\jorda\Documents\Stock Trading\NewsEvent3.0\Gathered_Data\";
    dt = "2020_04_02_23_01_45";
    path = strcat(dir,dt,"\",dt,"_parsed.mat")
    data = load(path).toWrite;
end
data = 

% Create class labels from bools
data.Category = categorical(data.BioSubject, [0,1], ["Other","Bio"]);

% Partition data (rule: don't touch/change test or validation set)
disp('Partitioning Data')
cvp = cvpartition(data.Category,'Holdout',0.1);
dataTest = data(cvp.test,:);
textDataTest = dataTest.Text;
YTest = dataTest.Category;

% Modify Training Data Only
dataTrain = data(cvp.training,:);   % index into data with vector arrays
dataTrain_p = dataTrain( dataTrain{:,"BioSubject"}, : );
p = dataTrain_p;
dataTrain_n = dataTrain( ~dataTrain{:,"BioSubject"}, : );
N = 21;
for i=1:N
    dataTrain_p = [dataTrain_p; p];
end
dataTrain = [dataTrain_n; dataTrain_p];

textDataTrain = dataTrain.Text;  % Use either "Text" or "Story"
YTrain = dataTrain.Category;


% Extract Features
disp('Extracting Features')
[docs_ngram, docs_1gram] = preprocessText(textDataTrain);
% docs_[]gram is a list of tokenizedDocuments, in order of original docs
disp('Creating Bags')
% (i only wanted to remove short words for the 1_grams--not the 2 and
% 3--grams
bag(1) = bagOfNgrams(docs_ngram,'Ngramlengths',[2,3]);
bag(2) = bagOfNgrams(docs_1gram,'NgramLengths',1);
bag2_ngrams = [ bag(2).Ngrams repmat("", bag(2).Ngrams.length, 2) ];  % Lx3
uniqGrams = [bag(1).Ngrams; bag2_ngrams];
counts = [bag(1).Counts bag(2).Counts];
bag = bagOfNgrams(uniqGrams, counts);  % create bag from multiple datasets
bag = removeInfrequentNgrams(bag,15);
[bag,idx] = removeEmptyDocuments(bag);
YTrain(idx) = [];
XTrain = bag.Counts;  % Indeces and counts of Ngrams per document. In original order

disp('Training Model')
t = templateLinear('Learner','svm','Regularization','lasso','Lambda',1/40000,'Verbose',1);
mdl = fitcecoc(XTrain,YTrain,'Learners',t)

% (0) use different data sets, one with short/stop words, to include those words in 2-gram or 3-gram models
% then join bags (1) do KW substitution for numbers, and completition for
% ... (like "meets prim...") and maybe replace trending things like covid..

% check how many "meets prim"

% Test Model
disp('Testing Model')
[documentsTest, dummy] = preprocessText(textDataTest);
XTest = encode(bag,documentsTest);
YPred = predict(mdl,XTest);
acc = sum(YPred == YTest)/numel(YTest)
sum(YTest == "Bio" & YPred == "Bio")
confusionchart( confusionmat(YTest,YPred) )

% Save Model
pth = strcat('C:\Users\jorda\Documents\Stock Trading\NewsEvent3.0\Models\', model_name, ".mat");
save(pth, 'mdl', 'bag');
disp("Saved data to folder");


% NOTES FROM JASON
%fitsvmposterior <- gets analog output of svm model

%area under roc <- to determine a statistically siginificant difference
%between performance

%singular value decomposition for classification (like PCA)...good for lots
%of input and simple model

%{
(1) .9421 acc, 2/174 positives detected: holdout .1, ecoc, using title only
remove ngrams of <2 occurrences
(2) .986 acc, 138/174 positives detected: holdout .1, ecoc, title only,
duplicated marked columns
(3) .978 acc, 119/175
25min   
(4) .987 acc, 141/175
 ecoc, title only, including stop/short words in ngrams, 300ms prediction
(5) .981 acc, 158/175
 same as before except -- ngram cuttoff at 100 instead of 2, 14ms
 prediction
(6) .986 acc, 140/174
 ngram cutoff at 50, 8ms
(7) .969 acc, 159/175
 ngram cutoff at 500, 5ms predict
(8) .99 acc, 142/175 (buuut i checked the false negatives and there were
actually about 4 that it wrongly missed--for the rest, the model knew
better than the data haha)
 ngram freq cutoff at 20, 5ms predict
(9) .986, 140/175 with cutoff at 50
(10) .984, 150/173 with cutoff at 75
(11) .986, 143/174 with cutoff at 30
(12) .986, 138/175 with cutoff at 15, 5-13ms prediction
(13) .974, 157/175
same as above, except logistic instead of svm
(14) changing to lasso and increasing lambda only gave worse acc

%} 

%{ 
% Looking at data for a single doc
docInd = 3;
textDataTrain(3)
vocabInd = bag.Counts(docInd, :) > 0;
bag.Ngrams(vocabInd,:)
% bag.Ngrams( bag.Counts(3,:) > 0, : )

% Looking up frequence for a specific ngram
logi = bag.Ngrams == ["report", "positive", ""];  % logical array
index = (logi(:,1) == true & logi(:,2) == true & logi(:,3) == true);
sum(bag.Counts(:,index))

% logi = bag.Ngrams == ["to", "announce", ""]; sum(bag.Counts(:,(logi(:,1) == true & logi(:,2) == true & logi(:,3) == true)))

%}



% FUNCTION DEFINITIONS
function [documents_ngram, documents_1gram] = preprocessText(input)
    disp("Preprocessing docs")
    % Convert to lowercase.
    documents = lower(input);

    % Tokenize the text.
    documents = tokenizedDocument(documents);

    % Remove a list of stop words then lemmatize the words. To improve
    % lemmatization, first use addPartOfSpeechDetails.
    documents = addPartOfSpeechDetails(documents);
    
    documents_1gram = removeStopWords(documents);
    documents_1gram = normalizeWords(documents_1gram,'Style','lemma');

    % Erase punctuation.
    documents_1gram = erasePunctuation(documents_1gram);

    % Remove words with 2 or fewer characters, and words with 15 or more
    % characters.
    documents_1gram = removeShortWords(documents_1gram,2);
    documents_1gram = removeLongWords(documents_1gram,15);
    
    % ngram
    documents_ngram = normalizeWords(documents,'Style','lemma');
    documents_ngram = erasePunctuation(documents_ngram);
    documents_ngram = removeLongWords(documents_ngram,15);
end