% In this script, I sort through the data in a particular
% dataset and label those that are bio subject
%
% I'm focusing on biotech companies that just release or are releasing
% test results for their product. This doesn't include plans to release
% results in the future.

dt = "2020_04_02_23_01_45";
if exist('d', 'var')
    d.reset();
else
    d = dataLabeler(dt);
end
d.addMatchCols(["phase"], "title");
marked = d.getMarked();

% (1) "phase" in title
% reviewed: IN FULL
disp("----1----")
rows_1 = d.d{:,"title:phase"};
d.markFromMatch("title:phase");
% unmark certain keyword matches - works
unmarkwords = ["tetraphase", "enphase","Stock alert","phasebio",...
    "phase-out", "recap and update", "prophase"];
d.addMatchCols(unmarkwords, "title");
unmarkwords = strcat("title:", unmarkwords);
for i = 1 : length(unmarkwords)
    s = unmarkwords(i);
    ind = d.d{:,s} & d.d{:,"title:phase"};
    d.d{ ind, 'BioSubject' } = false;
end
d.rmCols(unmarkwords);
tp = d.d( d.d{:,"title:phase"}, : );
% unmark certain indeces - works
unmarkI = [4960, 17516, 17521, 20979, 21499, 29111,...
    31597, 34011, 37906, 40167, 40419];
d.mark( unmarkI, false );
% unmark specific symbols
d.markSymbol( "enph", false );
disp( strcat("Marked: ", num2str(height(d.getMarked()))) );

% (2) "phase" cut off in title
% reviewed: IN FULL
disp("----2----")
d.addMatchCols(["phas...", "pha...", "ph...", " p..."], "title")
rows_2 = d.d{:,"title:phas..."} | d.d{:,"title:pha..."} | d.d{:,"title:ph..."};
d.d{ rows_2, "BioSubject" } = true;
unmarkI = [20901 ...%phas...
    23392, 38237, 40182, 40478, 40480, ... %pha...
    4908, 12863, 26878, 27431, 29568, 37117]; %ph...
tomarkI = [856, 2087, 2392, 2402, 3332, 3546, 4944, 5258, 5518, ...
    10027, 10123, 10153, 18884, 18974, 19131, 19766, ...
    20226, 26099, 26442, 26911, 30105, 30712, 31703, ...
    33848, 35716, 36000, 23327, 23556]; % p...
d.mark( unmarkI, false );
d.mark( tomarkI, true );
disp( strcat("Marked: ", num2str(height(d.getMarked()))) );

% (3) ~marked & ("endpoint" | "pivotal")
% reviewed: IN FULL
disp("----3----");
d.addMatchCols(["pivotal", "endpoint"], "title");
rows_3 = ~d.d{:,"BioSubject"} & (d.d{:,"title:pivotal"} ...
    | d.d{:,"title:endpoint"});
d.d{ rows_3 , "BioSubject" } = true;
unmarkI = [2275, 5564, 6693, 8572, 9485, 17600, ...
    22400, 33126, 35831, 35852, 38531];
d.mark( unmarkI, false );
disp( strcat("Marked: ", num2str(height(d.getMarked()))) );

% (4) (~marked & ~quart & ~financial & results) & phase #
% reviewed: IN FULL
disp("----4----");
d.addMatchCols(["quart", "financial", "results", "fiscal"], "title");
d.addMatchCols(["phase 1", "phase 2", "phase 3", "phase 4", "phase 5"], "story");
rows_phaseNum = d.d{:,"story:phase 1"} | d.d{:,"story:phase 2"} | ...
    d.d{:,"story:phase 3"} | d.d{:,"story:phase 4"} | d.d{:,"story:phase 5"};
rows_negate = d.d{:,"title:quart"} | d.d{:,"title:fiscal"} | d.d{:,"title:financial"};
rows_results = ~d.d{:,"BioSubject"} & d.d{:,"title:results"} & ~rows_negate;
rows_4 = rows_phaseNum & rows_results;
d.d{ rows_4, "BioSubject" } = true;
toUnmark = [6132;7013;7197;10147;13022;16735;19889;20959;21800;21801;...
    24600;25827;26660;31561;32692;34177];
d.mark( toUnmark, false)
disp( strcat("Marked: ", num2str(height(d.getMarked()))) );

% (5) ~prev & ~marked & ~quart & ~financial & results
% reviewed: IN FULL
d.addMatchCols(["stock alert", "q1 ", "q2 ", "q3 ", "q4 ", "full year", "half year"...,
    "year end", "voting results", "annual results", "quarterly results", "year-end"...
    ,"2019 results"], "title");
rows_prev = rows_1 | rows_2 | rows_3 | rows_4;
rows_negate =  rows_negate | d.d{:,"title:q1 "} | d.d{:,"title:q2 "}...
    | d.d{:,"title:q3 "} | d.d{:,"title:q4 "} | d.d{:,"title:stock alert"}...
    | d.d{:,"title:full year"} | d.d{:,"title:half year"} | d.d{:,"title:year end"}...
    | d.d{:,"title:annual results"} | d.d{:,"title:voting results"} | ...
    d.d{:,"title:quarterly results"} | d.d{:,"title:year-end"} | ...
    d.d{:,"title:2019 results"};
rows_5 = ~rows_prev & rows_results & ~rows_negate;
rows_prev = rows_prev | rows_5;
toMark = [10507;31657;818;1359;2298;2300;3106;3110;3281;3839;3871;3887;4206;4216;5521;5985;5986;8856;8859;9027;10260;10546;11005;11473;11559;12500;12688;13474;13967;14517;14551;14563;15009;15268;15881;16363;16725;18499;18843;22132;22721;22922;23478;23515;23521;23843;24295;24622;25064;25627;25815;27042;27141;31298;31315;31542;31581;31664;32856;32898;32899;33871;33874;34531;34594;36201;36214;36578;36608;37482;37486;37998;38027;38269;38500];
d.mark( toMark, true );
disp( strcat("Marked: ", num2str(height(d.getMarked()))) );

% (6) ~prev & ~negate & phase x
d.addMatchCols(["to pres", "to announ", "to repor", "to be "...
    ,"alert", "trading", "shareho", "stock", "licen", "agree"...
    ,"business", "corporate", "offering"], "title") 
rows_negate = rows_negate | d.d{:,"title:to pres"} | d.d{:,"title:to announ"} |...
    d.d{:,"title:to be "} | d.d{:,"title:alert"}...
    | d.d{:,"title:trading"} | d.d{:,"title:shareho"}...
    | d.d{:,"title:stock"} | d.d{:,"title:licen"} | d.d{:,"title:agree"}...
    | d.d{:,"title:business"} | d.d{:,"title:corporate"} | d.d{:,"title:offering"};
rows_6 = ~rows_prev & ~rows_negate & rows_phaseNum;
rows_prev = rows_prev | rows_6;
disp( strcat("Marked: ", num2str(height(d.getMarked()))) );
% Retrieved via intensive labeling
toMark = transpose([48;133;135;139;143;182;184;279;280;286;431;577;594;597;621;660;749;754;765;773;779;802;811;812;834;846;856;923;973;1002;1032;1044;1139;1140;1152;1159;1169;1179;1191;1192;1195;1199;1209;1220;1238;1255;1267;1291;1309;1335;1336;1447;1765;1768;1973;2040;2071;2077;2087;2094;2112;2187;2189;2278;2307;2352;2392;2402;2589;2593;2642;2658;2721;2722;2729;2730;2867;3003;3058;3132;3297;3312;3332;3419;3527;3540;3570;3583;3589;3591;3932;3936;3948;4047;4119;4152;4207;4211;4236;4263;4279;4294;4512;4549;4555;4558;4886;4892;4895;5092;5189;5265;5417;5418;5419;5437;5572;5656;5786;5945;5957;6292;6395;6436;6484;6497;6713;6746;6749;6789;6816;6832;6950;6992;6997;6998;7082;7131;7164;7170;7175;7207;7214;7518;7586;7594;7603;7612;7618;7864;7993;8227;8244;8289;8560;8615;8667;8699;8705;8756;8897;9145;9202;9227;9230;9371;9395;9464;9502;9507;9553;9554;9561;9572;9640;10027;10028;10087;10113;10116;10123;10126;10129;10153;10349;10533;10543;10572;10579;10604;10802;10862;10902;10908;10946;10969;11000;11148;11151;11157;11273;11324;11377;11562;11570;11582;11589;11609;11611;11661;11851;12046;12047;12345;12429;12432;12436;12504;12505;12606;12712;12772;12778;12779;12783;12833;12837;12860;12861;12862;12873;13136;13294;13398;13424;13436;13473;13508;13509;13835;13836;13872;14110;14202;14218;14512;14616;14619;14623;14624;14636;14644;14655;14704;15073;15103;15119;15158;15164;15171;15177;15338;15442;15445;15455;15462;15466;15479;15485;15486;15508;15518;15565;15566;15718;15860;15931;16020;16023;16026;16032;16169;16174;16192;16204;16224;16364;16366;16375;16417;16576;16732;16745;16808;17029;17063;17065;17072;17217;17246;17388;17415;17451;17578;17702;17887;17959;18044;18158;18189;18192;18374;18430;18447;18483;18503;18698;18722;18724;18745;18782;18804;18810;18815;18848;18853;18874;18908;18912;18923;18943;18945;18950;18951;18974;18976;18983;18988;18991;18995;19009;19019;19030;19039;19081;19185;19290;19323;19343;19473;19493;19674;19693;19756;19764;19814;19818;19829;20146;20160;20216;20238;20253;20277;20653;20699;20776;21004;21095;21129;21358;21470;21493;21497;21508;21509;21640;21711;21718;21726;21728;21732;21955;21963;21970;21994;22002;22165;22170;22274;22280;22402;22461;22471;22472;22502;22524;22594;22809;22813;22820;22824;22839;22841;22842;22844;22864;22870;22871;22877;22965;23187;23240;23281;23320;23349;23352;23435;23488;23506;23510;23517;23524;23531;23539;23541;23559;23592;23594;23599;23679;23807;23817;23857;24068;24080;24099;24125;24299;24491;24548;24549;24678;24719;24721;24856;24893;24933;24937;24989;25002;25007;25008;25009;25048;25065;25075;25099;25121;25126;25136;25140;25162;25179;25188;25213;25214;25215;25218;25220;25221;25225;25227;25228;25229;25230;25232;25235;25239;25241;25243;25364;25374;25421;25432;25433;25437;25439;25476;25492;25545;25837;26045;26092;26113;26381;26412;26418;26442;26461;26474;26488;26668;26911;27172;27480;27496;27572;27577;27631;27647;27696;27743;27860;27862;27874;27879;27935;27972;28016;28127;28140;28142;28176;28182;28495;28657;28757;28822;28838;28976;29110;29118;29180;29185;29453;29530;29532;29533;29595;29777;29781;29795;29800;29949;29950;30163;30308;30957;30983;31152;31171;31181;31210;31264;31344;31559;31599;31607;31623;31636;31638;31652;31683;31687;31693;31694;31697;31699;31702;31803;31823;31892;31906;31907;31908;31916;31930;31931;31947;31950;31951;31957;31968;32203;32310;32364;32636;32664;32693;32703;32749;32852;32909;32910;32914;33191;33207;33254;33286;33289;33320;33334;33375;33794;33848;33862;33920;33945;34448;34449;34493;34595;34753;35168;35319;35322;35323;35412;35451;35489;35620;35641;35758;35839;35874;35883;35885;35986;36008;36029;36160;36169;36585;36593;36641;36671;36686;36699;36704;36748;37070;37073;37156;37276;37322;37333;37663;37668;37767;37912;37919;37924;37958;38023;38034;38065;38159;38178;38211;38221;38235;38517;38578;38581;38589;38845;38901;38962;38964;38972;39005;39006;39223;39243;39315;39374;39396;39397;39411;39420;39562;39596;39653;39665;39694;39695;39867;39961;40225;40474]);
toUnmark = transpose([17;46;54;101;178;233;270;284;316;372;403;430;438;453;474;627;733;737;742;809;829;860;866;922;954;1156;1167;1170;1182;1273;1283;1346;1352;1384;1410;1518;1542;1575;1660;1666;1674;1751;1752;1761;1837;1914;1917;1927;1986;2005;2057;2180;2188;2287;2313;2343;2391;2408;2455;2458;2464;2500;2507;2525;2675;2700;2731;2761;2771;2819;2916;2993;2998;3043;3076;3294;3300;3353;3498;3580;3581;3606;3722;3731;3742;3787;3888;3914;3925;4023;4070;4075;4148;4322;4324;4339;4361;4398;4553;4557;4578;4624;4802;4805;4829;4833;4853;4859;4868;4910;4935;4965;4983;4995;5009;5019;5117;5131;5157;5227;5238;5244;5246;5247;5276;5277;5580;5644;5663;5709;5764;5897;5906;5908;5941;5979;6031;6102;6155;6187;6237;6268;6304;6378;6387;6483;6492;6765;6846;6881;6943;7138;7206;7209;7215;7240;7263;7303;7332;7353;7491;7497;7521;7532;7552;7553;7568;7653;7667;7689;7704;7715;7734;7735;7793;7799;7826;7851;7857;7859;7863;7867;7881;8186;8231;8310;8354;8403;8477;8491;8731;8742;8814;8842;8870;9132;9137;9252;9287;9432;9468;9471;9488;9505;9520;9529;9610;9660;9707;9723;9732;9820;9909;9911;9965;9997;10043;10073;10083;10134;10230;10348;10352;10371;10487;10574;10601;10607;10732;10803;10872;10964;11004;11030;11039;11068;11077;11097;11144;11189;11471;11489;11568;11584;11614;11997;12014;12141;12207;12208;12220;12354;12437;12448;12464;12537;12587;12589;12647;12716;12726;12739;12982;13003;13026;13095;13127;13261;13350;13365;13417;13443;13445;13503;13523;13526;13673;13804;13823;13833;13851;13861;13862;13863;14224;14255;14266;14268;14306;14442;14480;14504;14528;14534;14566;14572;14579;14592;14620;14630;14664;14669;14689;14834;14851;14852;14868;14916;15072;15183;15336;15399;15473;15478;15528;15532;15563;15656;15682;15730;15775;15785;16081;16166;16193;16207;16223;16227;16229;16332;16348;16372;16397;16466;16486;16507;16572;16587;16595;16633;16638;16672;16678;16687;16690;16718;16739;16794;16900;16922;16979;16983;17014;17055;17189;17190;17196;17292;17382;17441;17461;17473;17481;17553;17680;17708;17858;17915;17934;17985;18121;18182;18244;18364;18366;18433;18475;18481;18486;18548;18556;18643;18667;18717;18728;18730;18744;18746;18758;18768;18783;18793;18795;18812;18813;18824;18827;18831;18864;18905;18932;19014;19025;19026;19027;19028;19053;19064;19071;19075;19103;19152;19159;19166;19169;19170;19191;19209;19211;19233;19244;19309;19331;19337;19455;19500;19511;19516;19528;19530;19621;19672;19802;19806;19820;19828;19851;19886;19892;19988;20043;20044;20175;20180;20181;20191;20193;20254;20258;20303;20360;20378;20421;20556;20568;20593;20613;20635;20640;20661;20673;20680;20688;20700;20701;20702;20744;20747;20753;20754;20778;20848;20871;21002;21109;21173;21180;21279;21455;21466;21607;21701;21750;21782;21796;21824;21838;21921;21922;21953;21975;22010;22086;22186;22234;22311;22333;22345;22360;22486;22496;22509;22549;22612;22675;22733;22745;22789;22795;22805;22860;22882;22924;23053;23094;23119;23148;23153;23163;23184;23233;23234;23312;23369;23533;23556;23585;23586;23595;23601;23660;23685;23699;23750;23779;23780;23802;23832;23835;23892;23985;24065;24081;24139;24156;24163;24189;24205;24210;24218;24221;24360;24421;24428;24456;24471;24556;24617;24631;24664;24670;24714;24735;24744;24783;24786;24826;25006;25038;25090;25103;25109;25122;25125;25130;25138;25166;25176;25192;25193;25211;25257;25270;25283;25346;25382;25403;25404;25419;25420;25435;25436;25456;25489;25613;25690;25699;25775;25779;25780;25782;25902;25983;26120;26197;26214;26218;26325;26386;26416;26422;26459;26495;26605;26647;26680;26696;26723;26800;26810;26838;26839;26850;26893;26900;26903;26926;27016;27019;27054;27154;27156;27163;27217;27260;27272;27286;27288;27408;27416;27439;27460;27497;27500;27502;27639;27655;27672;27752;27775;27799;27919;27929;27938;27974;27982;27985;28117;28144;28169;28183;28260;28315;28379;28442;28444;28452;28457;28475;28482;28491;28605;28607;28651;28790;28844;28846;29209;29218;29313;29322;29323;29482;29507;29517;29519;29526;29548;29556;29565;29633;29683;29721;29767;29768;29869;30017;30130;30148;30270;30276;30282;30347;30396;30446;30490;30643;30669;30677;30700;30712;30780;30795;30918;30930;30994;31048;31180;31209;31259;31269;31311;31316;31333;31341;31345;31408;31529;31553;31640;31666;31732;31741;31746;31747;31804;31915;31956;32027;32110;32299;32302;32340;32346;32476;32478;32501;32557;32559;32632;32656;32660;32661;32666;32670;32675;32677;32679;32680;32681;32689;32697;32704;32705;32719;32728;32775;32786;32840;32887;32942;32973;33013;33244;33255;33312;33332;33338;33370;33430;33434;33516;33534;33610;33622;33633;33751;33757;33790;33795;33799;33804;33805;33820;33822;33833;33886;33888;33912;33998;34033;34176;34239;34242;34246;34247;34270;34273;34275;34343;34407;34413;34530;34559;34596;34599;34646;34657;34909;34951;34958;35425;35452;35513;35541;35555;35608;35613;35842;35865;35879;35887;35915;35923;35929;35932;35938;35951;36040;36064;36143;36161;36183;36226;36413;36428;36543;36557;36571;36626;36634;36701;36703;36755;36877;36942;36980;37039;37061;37092;37159;37170;37171;37184;37191;37219;37307;37319;37496;37500;37550;37604;37627;37636;37643;37648;37658;37828;37831;37856;37882;38014;38017;38047;38110;38185;38238;38244;38344;38345;38449;38544;38549;38601;38670;38684;38930;38955;38997;39021;39089;39339;39362;39375;39403;39417;39429;39446;39561;39624;39627;39664;39693;39719;39767;39859;39861;39968;40129;40165;40174;40206;40227;40362;40454;40463;40465;40470;40477]);
d.mark( toMark, true );
d.mark( toUnmark, false );
% Corrections while labeling
toMark = [5238, 6304, 18366, 25166, 30148, 36980, 37191, 39693];
toUnmark = [4119, 6992, 16576, 17217, 18853, 19493, 25374, 27935, 32340];

% MAKE SURE THERE ARE NO POSITIVE FALSES SO FAR -------------------------
% Further Refinement of intensive labeling

d.d{ rows_negate, "BioSubject" } = false;

% (6a) NEGATE all except actual results. TODO Later I might want to include
% FDA acceptance, fast track designation, etc. in marked list
d.addMatchCols(["fda"], "title");
rows_6b = d.d{:,"title:fda"};

% filter out "fast track"
d.addMatchCols([" fast"], "title");
rows_6b = rows_6b | d.d{:,"title: fast"};

% filter OUT (announces publication)
d.addMatchCols(["publica"], "title")
rows_6b = rows_6b | d.d{:,"title:publica"};

% keeping: title - "highlighted", (announces presentation), "initiat" |
% "begin"
% "initiat" | "begin" is exception to only results being marked

% filter other
d.addMatchCols(["approval", "clearance", " ind ", "submission", "accept", "to initiate"...
    ,"application", "fund", "(ind)", "orphan"], "title");
rows_6b = rows_6b | d.d{:,"title:approval"} | d.d{:,"title:clearance"} |...
     d.d{:,"title: ind "} | d.d{:,"title:submission"} | d.d{:,"title:accept"}...
     | d.d{:,"title:to initiate"} | d.d{:,"title:application"} | d.d{:,"title:fund"}...
     | d.d{:,"title:(ind)"} | d.d{:,"title:orphan"};

rows_6b = rows_6b & d.d{:, "BioSubject"}; % only ruling out matches from group 6
d.d{ rows_6b, "BioSubject" } = false;
rows_prev = rows_prev | rows_6b;

% LOOK THROUGH UNMARKED & ~PREVIOUS & ~NEGATE ... MAKE SURE THERE ARE NO FALSE
% NEGATIVES

rows_subgroup = ~rows_negate & ~rows_prev & ~d.d{:,"BioSubject"};  % subgroup of articles to look within
d.addMatchCols(["clinic","develop","update","clinical update","study","trial","data","observation"], "title");

d.mark([1715], true);
r = d.d{:,"title:clinical update"};
d.d{ r, "BioSubject" } = true;
rows_prev = rows_prev | rows_subgroup | r;

rows_update = rows_subgroup & d.d{:,"title:update"} & ( d.d{:,"title:clinic"} | d.d{:,"title:develop"} );
rows_6c = rows_subgroup & ( d.d{:,"title:study"} | d.d{:,"title:trial"} | d.d{:,"title:data"} );
rows_6c = rows_6c | rows_update;
rows_prev = rows_prev | rows_6c;

% intensive 6c: title:study -- title:trial -- title:data
d.d{ rows_6c, "BioSubject" } = false;
toMark = [23468;25249;26468;138;796;801;806;819;839;927;1222;1622;1731;1760;1782;1935;2062;3186;3187;3188;3544;3567;3813;3869;4130;4197;4269;4271;4274;4275;4402;4500;5214;5436;5914;5919;5921;6109;6374;6383;6437;6449;6466;6806;7589;8314;8746;9298;9588;9592;9960;9961;9980;10497;10531;10994;10995;11222;11224;11303;11537;11859;12340;12348;12705;12793;13505;14539;14633;14652;15161;16118;16155;16165;16178;16245;16284;16651;17050;17471;18500;18948;18992;19010;19402;19407;19490;19763;19790;19805;19810;19907;20133;20995;21092;21217;21461;21981;21982;22139;22245;22246;22525;22545;22801;22872;22873;22874;23174;23776;23787;23950;24037;24117;24333;24691;24829;24830;24942;25014;25117;25222;25223;25233;25296;25299;25521;26035;26899;26931;26938;27193;27540;27740;27749;28014;28050;28462;28811;28815;29113;29167;29175;29179;29272;29490;30071;31156;31167;31278;31631;31649;31668;31704;31891;31894;31905;31912;31944;31952;31954;31964;32742;32744;32779;32851;33240;33283;33860;33877;33931;33985;33986;34260;34261;34570;35142;35320;35476;35504;35510;35536;35820;35828;35829;35858;35990;36192;36225;36650;36676;36690;36782;36791;36973;37560;37590;37595;37614;37625;37631;37717;37838;37911;37915;38042;38043;38250;38577;38597;38611;38633;38692;39305;39306;39314;39372;39588;39589;39619;39631;39648;40204];
d.mark( toMark, true );

% corrections
d.mark( [1782, 22801, 27540, 19010, 21982, 32779, 19010, 21982, 21981], false );  % corrections... and first 4 of toMark

% I SKIMMED OVER ALL THE MARKED ARTICLE TITLES (1700) ---------
% corrections:
d.mark( [597;2593;4549;10053;3132;11851;...
    4119;10572;22170;27935], false);
d.addMatchCols(["authoriz", "app..."], "title")
d.d{ (d.d{:,"title:authoriz"} | d.d{:,"title:app..."}) ...
    & d.d{:,"BioSubject"} , "BioSubject" } = false;

% check for unseen marked articles
rows_blind = ~rows_prev & d.d{:,"BioSubject"};
d.d{ rows_blind, "BioSubject" } = true;
d.mark( [4944], false );
rows_prev = rows_prev | rows_blind;

% filter out future tense
d.addMatchCols(["to report"], "title");
rows_future = d.d{:, "title:to report"};
d.d{ rows_future, "BioSubject" } = false;
rows_prev = rows_prev | rows_future;

% NOW I AT LEAST HAVE CHECKED ALL OF THE POSITIVES, FILTER OUT FALSE NEGATIVES

% check ~rows_prev (not previously seen) for false negatives
% checked: positive, efficacy, interim, breakthrough, promising, topline,
% safety, presents, endpoint

% I've checked unseen, seen & marked. Lastly, check seen & unmarked.
% checked: (title:) positive dat, announces resul, efficacy, endpoint,
% interim,positiv,preliminary,data,preclinical,breakthrough,significant
% improves,effective,benefi
% TODO:results etcetc...seem to be alot of false negatives here
% story:new data
mark = [33889;5238;23779;26473;26493;33748;33889;34511;34697];
mark = [mark;828;3845;3850;11911;11934;24944;31250;33748;33889;34697;39243]; % from "positiv"
mark = [mark;11933;35519];  % from "preliminary"
mark = [mark;13018;32527;33272;2045;4280;1138;17081;16617;17377;18399;35825;19847;19849];
d.mark(mark, true);
d.addMatchCols(["positiv","highlights public"], "title");
rows_last = rows_prev & ~d.d{:,"BioSubject"};
d.d{ rows_last & d.d{:,"title:highlights public"}, "BioSubject" } = true;

% filter out "opinion", "publication'
d.addMatchCols(["opinion"], "title")
d.d{ d.d{:,"title:opinion"}, "BioSubject" } = false;

% DONE LABELING

% remove keyword indexing columns
toWrite = d.d(:,1:8);
% write parsed data
path = strcat("C:\Users\jorda\Documents\Stock Trading\NewsEvent3.0\Gathered_Data\"...
    , dt, "\", dt, "_parsed.mat");
save(path, 'toWrite');



% FUNCTIONS I USED WHILE PARSING

rows_subgroup = d.d{:,"BioSubject"};%rows_prev & ~d.d{:,"BioSubject"};%d.d{:,"BioSubject"};  % subgroup of articles to look within
check = d.d( rows_subgroup, : );

%{
% Chunk for realtime searching by keyword in title
% Add this to dataLabeler class
s = "title:";
toLook = "story";
while ( ~strcmp(s, "exit") )
    if strcmp(s,"all")
        results = checkO;
    else
        if ~contains(s, "title:") & ~contains(s, "story:")
            s = "title:--invalid input--";
        end
        s = char(s);
        toLook = string( s(1:5) );
        s = string( s(7:end) );
        d.addMatchCols([string(s)], toLook);
        rows = rows_subgroup & d.d{:, strcat(toLook,":",string(s))};
        results = d.d( rows, : );
        if height(results) > 40
            disp(results(1:40,:))
            disp(strcat("                                                     ",num2str(size(results))))
            s = input("enter 'more' for more rows.                              more?", 's');
            if strcmp(s,"more")
                disp(results(40:end,:))
            end
        else
            disp(results)
        end
        disp(strcat("                                                     ",num2str(size(results))))
    end
    s = input("enter 'exit' to stop. Search (title/story:kw) -->", 's');
end
%}


%{
% chunk for intensively labeling docs one by one
decisions = [];
next = "";
toMark = [];
toUnmark = [];
for i = 1 : height(check)
    row = check(i,:);
    disp(" ")
    disp(" ")
    br = "";
    for ii=1:200
        br = strcat(br,"-");
    end
    disp(br)
    disp( strcat(num2str(i), " out of ", num2str(height(check)) ) )
    disp(row.Index);
    S = string(row.Story);
    parts = regexp(S, '.{1,200}', 'match');
    if length(parts) > 14
        parts = parts(1:14);
    end
    fprintf('%s\n', parts{:});
    disp(" ")
    disp(strcat("Title: '",row.Text,"'"));
    disp(" ")
    index = row.Index;
    prompt = strcat(next," Press enter if it's a BioSubject, and press anything else if it's not: ");
    if ( isempty( input(prompt, 's') ) )
        next = "Marked as BioSubject.";
        toMark(end+1) = index;
        decisions(end+1) = true;
        d.d{index, "BioSubject"} = true;   
    else
        next = "Marked as NOT BioSubject.";
        toUnmark(end+1) = index;
        decisions(end+1) = false;
        d.d{index, "BioSubject"} = false;   
    end
end
decisions = transpose(decisions);
toUnmark = transpose(toUnmark);
toMark = transpose(toMark);
%}



