% backTest1
% assuming we only use first detected news article of event

% get data
d = dataPrepare("2020_05_23_18_16_52");
%"2020_05_23_18_16_52"2020_05_30_10_09_35"

% load model
mdl_name = "react1_p10r57_1";
pth = "C:\Users\jorda\Documents\Stock Trading\NewsEvent3.0\Models\"
loaded = load( strcat(pth,mdl_name,".mat") );
mdl = loaded.mdl;
bag = loaded.bag;
xvar = loaded.xvar;
yvar = loaded.yvar;

% price cutoff & simple kw filter
d = d(d.Start_Price < 10 , : );
simP = simpleKWPred(d.Text);
d.simp = simP;
d( ~strcmp(simP,"long"), : ) = [];

% prepare data
[documentsTest, dummy] = preprocessText(d{:,xvar});
XTest = full( encode(bag,documentsTest) );

% predict & measure results
YPred = predict(mdl,XTest);
R = corr2(YPred,d{:,yvar})

% plot
%{
scatter(YPred, d{:,yvar})
xlabel("Predicted");
ylabel("Actual");
xlim([0,70])
ylim([0,70])
title(string(R))
%}

d.err = abs(YPred-d{:,yvar});
d.missed = d.err > .35*YPred;
goody = d{~d.missed, yvar};
goodyhat = YPred(~d.missed);
bady = d{d.missed, yvar};
badyhat = YPred(d.missed);
scatter(goodyhat,goody);
hold on
scatter(badyhat,bady);
xlabel("Predicted");
ylabel("Actual");
xlim([0,70])
ylim([0,70])

look = sortrows(d, 'err', 'descend');
look = look(:,["Start_Price", "TenMinH", "err", "Text", "Story"]);

look = sortrows(d, "missed");
look = d(:, ["TenMinH", "missed", "simp", "Text"])


% needs to know these features: 
% - tense of main verb
% - if it is showing study results or something else


% Predicts categorical result (long, short, neutral) for inputed documents
% via simple keyword triggers.
function pred = simpleKWPred(strA)
    s = size(strA);
    pred = repmat("",s(1),1);  % string array
    verb = ["reports", "presents", "announces", "initiates", "outlines"];
    noun = ["complet", "results", "resu...", "resul...", "result...", ...
        "data", "safety", "study"];
    pos = ["positiv", "beneficial", "reduces risk", "reduced risk", "excellent"...
        ,"safe ","improve","breakthrough","effective", "meets end"...
        ,"success"];
    neg = ["did not meet", "stops", "fail", "did not succeed"];%, "to be present", "to present"];
            
    % Returns a logical array corresponding to whether each input event
    % contains any of the strings in the input keyphrase array.
    % input: list of keyphrase triggers, list of documents to check against
    function logI = containsAny(triggers, strA)
        s = size(strA);
        logI = repmat(0,s(1),1);
        for i=1:triggers.length
            keyphrase = lower(triggers(i));  % keyphrase
            logI = logI | contains(lower(strA), keyphrase);
        end
    end
    
    verbI = containsAny(verb, strA);
    nounI = containsAny(noun, strA);
    posI = containsAny(pos, strA);
    negI = containsAny(neg, strA);
    
    pred( verbI & posI ) = "long";
    pred( negI ) = "short";
end
function [documents_ngram, documents_1gram] = preprocessText(input)
    disp("Preprocessing docs")
    % Convert to lowercase.
    documents = lower(input);

    % Tokenize the text.
    documents = tokenizedDocument(documents);

    % Remove a list of stop words then lemmatize the words. To improve
    % lemmatization, first use addPartOfSpeechDetails.
    documents = addPartOfSpeechDetails(documents);
    
    documents_1gram = removeStopWords(documents);
    documents_1gram = normalizeWords(documents_1gram,'Style','lemma');

    % Erase punctuation.
    documents_1gram = erasePunctuation(documents_1gram);

    % Remove words with 2 or fewer characters, and words with 15 or more
    % characters.
    documents_1gram = removeShortWords(documents_1gram,2);
    documents_1gram = removeLongWords(documents_1gram,15);
    
    % ngram
    documents_ngram = normalizeWords(documents,'Style','lemma');
    documents_ngram = erasePunctuation(documents_ngram);
    documents_ngram = removeLongWords(documents_ngram,15);
end

%{

% good: assure present tense:
if it contains "present" "announce" "report" "show" "display" "outline" "submit(not submission)"
    assure that it is present tense, in verb form with (affix: "s")



%}