% First attempt at building model which predicts price reaction to first
% occurence of news_event from trainer_cat1
% pipeline: dataGatherer ->[cat1]-> dataPrepare [delete duplicates]->
%           [price filter]-> trainer_react -> [simpleKWPred-long] 

%{
[dTrain,dTest,bag,mdl] = loadModel("react1_fsrnca4");

logI = mdl.FeatureWeights > .001;
features = table(bag.Ngrams(logI,:), double(mdl.FeatureWeights(logI)),...
    transpose( sum(bag.Counts(:,logI) > 0) ), transpose(bag.Counts(:,logI)),...
    'VariableNames',["UniqueNgrams","Weight","DocCount","Counts"]);
features = sortrows(features,"Weight","descend");
% Remove selected features by first string of nGram
toRm = ["2020", "april", "march", "december", "friday", "paclitaxel", "december"];
for i = 1:toRm.length
    kw = toRm(i);
    toRmI = strcmp(features.UniqueNgrams(:,1), kw);
    features(toRmI,:) = [];
end  
s = size(features);
disp(strcat("Using ",string(s(1))," features."));
% Create bag with selected features
counts = transpose(features.Counts);
uniqueNgrams = features.UniqueNgrams;
newBag = bagOfNgrams(uniqueNgrams,counts)
%}

% testing/training new model
%
data = dataPrepare("2020_05_09_18_52_24");
data = data(data.Start_Price < 5 , : );%100:, 50:.5, 30:.54, 15:.57, 2:.1(12), 1:-.02

% insert title data into story but make ngrams unique
sz = size(data);
data.FullText = repmat("",sz(1),1);
for i = 1:sz(1)
    title_words = split( data.Text(i) );
    title_ft = strcat( "", title_words );  % optional: tag text features for uniqueness
    title_ft = strjoin( title_ft );
    data{i,"FullText"} = strcat( title_ft," ",data.Story(i) );
end
    
% tame outliers
data{ data{:,"OneHourH"} > 100, "OneHourH"} = 50;
data{ data{:,"TenMinH"} > 100, "TenMinH"} = 50;
data{ data{:,"TwoHourH"} > 100, "TwoHourH"} = 90;

% group
data( data.TenMinH == 0, "Group" ) = {"Dud"};
data( data.TenMinH > 15, "Group" ) = {"Winner"};
data( data.TenMinH < 15 & data.TenMinH > 0, "Group" ) = {"Between"};
%data = sortrows(data, "Group");
%data( :, ["Group", "TenMinH", "Text"] )

% neg/pos list
%data.simple = simpleKWPred(data.Text);

%data = sortrows(data, ["Group", "TenMinH"]);

%d = data( :, ["Group", "TenMinH", "simple", "Text"] )
    

yvar = "TenMinH";
xvar = "FullText";

%{
[docs_ngram, docs_1gram] = preprocessText(data{:,xvar}, data{:,"Text"});
bag(1) = bagOfNgrams(docs_ngram,'Ngramlengths',[2,3]);
bag(2) = bagOfNgrams(docs_1gram,'NgramLengths',1);
bag2_ngrams = [ bag(2).Ngrams repmat("", bag(2).Ngrams.length, 2) ];  % Lx3
uniqGrams = [bag(1).Ngrams; bag2_ngrams];
counts = [bag(1).Counts bag(2).Counts];
% creating bag
bag = bagOfNgrams(uniqGrams, counts);  % create bag from multiple datasets
bag = removeInfrequentNgrams(bag,3)
%}
trainModel2( data, xvar, yvar, docs_ngram, bag );

%ANALYZING FEATURE SELECTION
%{
logI = (mdl.FeatureWeights > .01);
mdl.FeatureWeights(logI) % see top weights
bag.Ngrams(logI,:)  % see top ngrams
sum(bag.Counts(:,logI) % see total counts of each nGram
sum(bag.Counts(:,logI) > 0) % num of doc appearences per ngram

features = table(bag.Ngrams(logI), double(mdl.FeatureWeights(logI)),...
    transpose( sum(bag.Counts(:,logI) > 0) ),...
    'VariableNames',["Ngram","Weight","DocCount"]);
features = sortrows(features, "Weight", 'descend');

% plotting
figure()
plot(mdl.FeatureWeights,'ro')
grid on
xlabel('Feature index')
ylabel('Feature weight')

% plotting
figure()
bar(mdl.FeatureWeights(logI))
grid on
xticklabels(bag.Ngrams(logI,:)
xlabel("Word")
ylabel('Feature weight')

%}

%---PROGRESS---
%{
-- dataset: 2020_05_09_18_52_24 --

 - N samples: 400
features: same as trainer_cat1
output: OneHourH
model: fitrlinear
result: unuseable

 - N samples: 400
features: fsrnca3 with 3-80 features
output: OneHourH
model: fitrlinear
result: no correlation

 - N samples: 400
features: fsrnca3 with 3-80 features
output: OneHourH
model: fitrlinear
result: no correlation

 - N samples: 400
features: fsrnca4 with all features
output: OneHourH or TenMinH?
model: fitrlinear
result: .23

 - N samples: 200 with price filter (<10)
features: all Title features
output: OneHourH or TenMinH?
model: fitrkernel
result: bad

 - N samples: 200 with price filter (<10)
features: all Title features
output: TenMinH
model: fitrsvm
result: .37 (where all predictable events are triggered in simpleKWPred as
long. If I only include these events, the correlation is .43 
practical results are viable)

 - N training samples: 200 with price filter (<10)
N used samples ("long" from simpleKWPred of title): 34
features: all Title features
output: TenMinH
model: fitrsvm
result: .43 (out of used samples)

 - N training samples: 200 with price filter (<10)
N used samples ("long" from simpleKWPred of title): 34
features: all Story features
output: TenMinH
model: fitrsvm
result: .49 (out of used samples)

 - N training samples: 200 with price filter (<10)
N used samples ("long" from simpleKWPred of title): 34
features: from title appended to story--unique tag on title's words
output: TenMinH
model: fitrsvm
result: .56 (out of used samples)

 - N training samples: 200 with price filter (<[15,10,5])
N used samples ("long" from simpleKWPred of title): [43,34,24]
features: from title and story --no unique tags on title's words
output: TenMinH
model: fitrsvm
result: [.57,.59,.64] (out of used samples)

 - N training samples: 400
N used samples ("long" from simpleKWPred of title): 68
features: from title and story --no unique tags on title's words
output: TenMinH
model: fitrsvm
result: .42 (out of used samples)

 - N training samples: 400
N used samples ("long" from simpleKWPred of title): 68
features: from title appended to story--no unique tags on title's words
          and start price
output: TenMinH
model: fitrsvm
result: .45 (out of used samples)

 - N training samples: price filter (<5)
N used samples ("long" from simpleKWPred of title): 24 -- 1/week
features: from title appended to story--no unique tags on title's words
output: TenMinH
model: fitrsvm
result: .64 (out of used samples) -- 95% of predictions <= actual. good!

 - N training samples: 200 with price filter (<10)
N used samples ("long" from simpleKWPred of title | story): 175
features: from title appended to story--no unique tags on title's words
output: TenMinH
model: fitrsvm
result: .26 (out of used samples)

 - N training samples: 200 with price filter (<10)
N used samples (no simpleKWPred Filter): 208
features: from title appended to story--no unique tags on title's words
output: TenMinH
model: fitrsvm
result: .34 -- but many false positives

%options: 
x adding start price as a feature
x fine tune start_price thresh (5 seems best, 10 is good)
x Use K-Fold correctly? Look it up
x Use simpleKWPred on story too
x Divide features by number of words in document
- Gather more data that i haven't seen, and test model on that

%}

%---SAVED MODELS/FEATURESETS---
%{
See "model descriptions" in models folder

%Title Bag->OneHourH trained feature selector:
%react1_fsrnca1
mdl = fsrnca(xTrain, dTrain.OneHourH, 'Verbose', 1)
partition: no data partition

%Story Bag->OneHourH trained feature selector:
%react1_fsrnca12
mdl = fsrnca(xTrain, dTrain.OneHourH, 'Verbose', 1)
%8 hours elapsed
partition: no data partition

%Story Bag->OneHourH trained feature selector:
%react1_fsrnca3
mdl = fsrnca(xTrain, dTrain.OneHourH, 'Verbose', 1)
partition: 90% train, 10% test
7.5 hrs

%Text Bag->OneHourH trained feature selector:
%react1_fsrnca4
mdl = fsrnca(xTrain, dTrain.OneHourH, 'Verbose', 1)
partition: 90% train, 10% test
5 min

%Story Bag->OneHourPHL trained feature selector:
%react1_fsrnca5
mdl = fsrnca(xTrain, dTrain{:,yvar}, 'Verbose', 1,'solver','sgd')
partition: 90% train, 10% test
5 min

%Story Bag->OneHourL trained feature selector:
%react1_fsrnca6
mdl = fsrnca(xTrain, dTrain{:,yvar}, 'Verbose', 1,'solver','sgd')
partition: 90% train, 10% test
5 min

%}


% train model with kfold and test
function trainModel2(data,xvar,yvar,docs_ngram,bag)
    % todo: do quick research on kfold to see how to get resulting model
    cvp = cvpartition(data{:,yvar},'KFold',10);
    
    %{
    [docs_ngram, docs_1gram] = preprocessText(data{:,xvar});
    bag(1) = bagOfNgrams(docs_ngram,'Ngramlengths',[2,3]);
    bag(2) = bagOfNgrams(docs_1gram,'NgramLengths',1);
    bag2_ngrams = [ bag(2).Ngrams repmat("", bag(2).Ngrams.length, 2) ];  % Lx3
    uniqGrams = [bag(1).Ngrams; bag2_ngrams];
    counts = [bag(1).Counts bag(2).Counts];
    
    % creating bag
    bag = bagOfNgrams(uniqGrams, counts);  % create bag from multiple datasets
    bag = removeInfrequentNgrams(bag,3)
    %}
    
    R = zeros(cvp.NumTestSets,1);
    yhat_full = [];
    y_full = [];
    
    %%% for predictability model -- copy empty version of data table
    d = data(1:2,:);  % 2 is arbitrary
    d.yhat = [-1; -1];
    newData = d;
    newData(:,:) = [];  % creates new table with correct columns
    newX = [];
    %%%
    for i = 1:cvp.NumTestSets
        % organize training data
        trI = cvp.training(i);
        xtr = bag.Counts(trI, :);
        %xtr(:,end+1) = data{trI, "Start_Price"}; % Price as feature
        ytr = data{trI, yvar};
        % train
        disp("filling data")
        xtr = full(xtr);
        disp("train model")
        mdl = fitrsvm(xtr,ytr);
        % organize test data
        teI = cvp.test(i);
        dat_te = data(teI, :);
        xte = encode(bag, docs_ngram(teI));
        %xte(:,end+1) = dat_te{:,"Start_Price"}; % Price as feature
        % get results
        yhat = predict(mdl, full(xte));
        % only care for results on "long"s
        yhat_simple = simpleKWPred(dat_te.Text);
        yhat( ~strcmp(yhat_simple,"long") ) = -99;
        %yhat( strcmp(yhat_simple,"long") ) = yhat( strcmp(yhat_simple,"long") ) + 10;
        yhat( strcmp(yhat_simple,"short") ) = 0;
        yact = data{teI, yvar};
        yhat_full = vertcat(yhat_full, yhat);
        y_full = vertcat(y_full, yact);
        R(i) = corr2(yhat, yact);
        
        %%% for predictability model
        dat_te.yhat = yhat;
        newData = [newData; dat_te];
        newX = [newX; xte];
        %%%
    end   
    disp(R)
    R_mean = mean(R)
    
    ii = yhat_full ~= -99;
    
    newData = newData(ii,:);
    
    %
    %d.err = abs(dat_te.yhat-d{:,yvar});
    newData.err = newData.yhat-newData{:,yvar};
    newData.sep = newData.err > .35*newData{:,yvar};
    goody = newData{~newData.sep, yvar};
    goodyhat = newData.yhat(~newData.sep);
    bady = newData{newData.sep, yvar};
    badyhat = newData.yhat(newData.sep);
    scatter(goodyhat,goody);
    hold on
    scatter(badyhat,bady);
    xlabel("Predicted");
    ylabel("Actual");
    xlim([0,70])
    ylim([0,70])
    
    look = sortrows(newData, 'err', 'descend');
    look = look(:,["err", "TenMinH", "Start_Price", "Text", "Story"]);
    
    %{
    scatter(yhat_full(ii), y_full(ii))  
    grid on;
    xlim([0,70])
    ylim([0,70])
    xlabel("y pred")
    ylabel("y actual")
    title(corr2(yhat_full(ii), y_full(ii)))
    %}
    
    sz = size(yhat_full(ii))
    mean_yact = mean(y_full(ii))
    median_yact = median(y_full(ii))
    xlim([-5,60])
    
    % train final model
    x = full( bag.Counts );
    y = data{: ,yvar};
    mdl = fitrsvm(x,y);
    % save model
    pth = "C:\Users\jorda\Documents\Stock Trading\NewsEvent3.0\Models\";
    %save( strcat(pth,"react1_p10r57_1",".mat") ,"mdl","bag","xvar","yvar");
    
    % predictability model

end

% train model with holdout
function mdl = trainModel(dTrain,dTest,bag,xvar,yvar)
    yTrain = dTrain{:,yvar};
    %yTrain(idx) = [];
    xTrain = bag.Counts;  % Indeces and counts of Ngrams per document. In original order

    disp('Training Model')
    [mdl,fitInfo] = fitrlinear(xTrain,yTrain,'solver','sgd');

    % Test Model
    disp('Testing Model')
    [documentsTest, dummy] = preprocessText(dTest{:,xvar});
    XTest = encode(bag,documentsTest);
    YPred = predict(mdl,XTest);
    R = corr2(YPred,dTest{:,yvar})
    scatter(YPred, dTest{:,yvar})
    xlabel("Predicted");
    ylabel("Actual");
    title(string(R))
    
    %m = max( max(YPred), max(dTest{:,yvar}) );
    %xlim([0,m]);
    %ylim([0,m]);

    %[m, order] = confusionmat(dTest.OneHourH,YPred)
    %confusionchart( m, order)
end

% Story features
% Feature Weight Cutoff: 1
% Num Features: 50
% no correlation
function mdl3()
    [dTrain,dTest,bag,mdl] = loadModel("react1_fsrnca3");

    logI = mdl.FeatureWeights > .8;
    features = table(bag.Ngrams(logI,:), double(mdl.FeatureWeights(logI)),...
        transpose( sum(bag.Counts(:,logI) > 0) ), transpose(bag.Counts(:,logI)),...
        'VariableNames',["UniqueNgrams","Weight","DocCount","Counts"]);
    features = sortrows(features,"Weight","descend");
    % Remove selected features by first string of nGram
    toRm = ["2020", "april", "march", "december", "friday", "paclitaxel", "december"];
    for i = 1:toRm.length
        kw = toRm(i);
        toRmI = strcmp(features.UniqueNgrams(:,1), kw);
        features(toRmI,:) = [];
    end  
    s = size(features);
    disp(strcat("Using ",string(s(1))," features."));
    % Create bag with selected features
    counts = transpose(features.Counts);
    uniqueNgrams = features.UniqueNgrams;
    newBag = bagOfNgrams(uniqueNgrams,counts)

    trainModel( dTrain,dTest,newBag,"Story","OneHourH" );
end

% extracted features are also saved as models
function [dTrain, dTest, bag, mdl] = loadModel(mdlName)
    pth = "C:\Users\jorda\Documents\Stock Trading\NewsEvent3.0\Models\"
    loaded = load( strcat(pth,mdlName,".mat") );
    bag = loaded.bag;
    dTest = loaded.dTest;
    dTrain = loaded.dTrain;
    mdl = loaded.mdl;
end

function [dTrain, dTest, bag, mdl] = extractFeatures(saveAs)
    data = dataPrepare("2020_05_09_18_52_24");
    yvar = "OneHourL";
    xvar = "Story";

    cvp = cvpartition(data{:,yvar} > 6,'Holdout',0.1);
    dTest = data(cvp.test,:);
    dTrain = data(cvp.training,:);

    [docs_ngram, docs_1gram] = preprocessText(dTrain{:,xvar});
    bag(1) = bagOfNgrams(docs_ngram,'Ngramlengths',[2,3]);
    bag(2) = bagOfNgrams(docs_1gram,'NgramLengths',1);
    bag2_ngrams = [ bag(2).Ngrams repmat("", bag(2).Ngrams.length, 2) ];  % Lx3
    uniqGrams = [bag(1).Ngrams; bag2_ngrams];
    counts = [bag(1).Counts bag(2).Counts];
    bag = bagOfNgrams(uniqGrams, counts);  % create bag from multiple datasets
    bag = removeInfrequentNgrams(bag,3);
    %[bag,idx] = removeEmptyDocuments(bag);
    
    yTrain = dTrain{:,yvar};
    %yTrain(idx) = [];
    xTrain = bag.Counts;  % Indeces and counts of Ngrams per document. In original order
    
    % find correlation
    %mat = [xTrain dTrain{:,yvar}];
    %[R]=corrcoef(mat);
    %maxR = max(max(triu(R,1))) % highest correlation
    %[row,col] = find(R==maxR,1,'first')
    
    % feature importance
    tic;
    mdl = fsrnca(xTrain, dTrain{:,yvar}, 'Verbose', 1,'solver','sgd')
    toc
    
    if exist('saveAs','var')
        pth = "C:\Users\jorda\Documents\Stock Trading\NewsEvent3.0\Models\"
        save( strcat(pth,saveAs,".mat") ,"mdl","dTrain","dTest","bag");
    end        
end

% predicts categorical result (long, short, neutral) for inputed documents
% Predicts categorical result (long, short, neutral) for inputed documents
% via simple keyword triggers.
function pred = simpleKWPred(strA)
    s = size(strA);
    pred = repmat("",s(1),1);  % string array
    verb = ["reports", "presents", "announces", "initiates", "outlines"];
    noun = ["complet", "results", "resu...", "resul...", "result...", ...
        "data", "safety", "study"];
    pos = ["positiv", "beneficial", "reduces risk", "reduced risk", "excellent"...
        ,"safe ","improve","breakthrough","effective", "meets end"...
        ,"success"];
    neg = ["did not meet", "stops", "fail", "did not succeed"];%, "to be present", "to present"];
            
    % Returns a logical array corresponding to whether each input event
    % contains any of the strings in the input keyphrase array.
    % input: list of keyphrase triggers, list of documents to check against
    function logI = containsAny(triggers, strA)
        s = size(strA);
        logI = repmat(0,s(1),1);
        for i=1:triggers.length
            keyphrase = lower(triggers(i));  % keyphrase
            logI = logI | contains(lower(strA), keyphrase);
        end
    end
    
    verbI = containsAny(verb, strA);
    nounI = containsAny(noun, strA);
    posI = containsAny(pos, strA);
    negI = containsAny(neg, strA);
    
    pred( verbI & posI ) = "long";
    pred( negI ) = "short";
end


% FUNCTION DEFINITIONS
%{
ouput:
documents_ngram - Tokenized document of words from 0 to 15 characters
documents_1gram - Tokenized document of words from 2 to 15 characters
%}
function [documents_ngram, documents_1gram] = preprocessText(input, title)
    disp("Preprocessing docs")
    % merge full text words into input
    
    % Convert to lowercase.
    documents = lower(input);

    % Tokenize the text.
    documents = tokenizedDocument(documents);

    % Remove a list of stop words then lemmatize the words. To improve
    % lemmatization, first use addPartOfSpeechDetails.
    documents = addPartOfSpeechDetails(documents);
    documents = normalizeWords(documents,'Style','lemma');
    %cat = repmat("",documents.length,1);
    %for i = 1:documents.length
    %    cat(i) = strcat( lower(title(i)), join(tokenDetails(documents(i)).Token) );
    %end
    %documents = tokenizedDocument( cat );
    
    documents_1gram = removeStopWords(documents);

    % Erase punctuation.
    documents_1gram = erasePunctuation(documents_1gram);

    % Remove words with 2 or fewer characters, and words with 15 or more
    % characters.
    documents_1gram = removeShortWords(documents_1gram,2);
    documents_1gram = removeLongWords(documents_1gram,15);
    
    % ngram
    documents_ngram = erasePunctuation(documents);
    %documents_ngram = removeShortWords(documents_ngram,2);
    documents_ngram = removeLongWords(documents_ngram,15);
end