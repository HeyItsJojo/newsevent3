%{
dt = "2020_04_02_23_01_45"
d = dataLabeler(dt);
d.addMatchCols(["bio"], "title");
d.markFromMatch("title:phase");
d.getMarked();
%}

classdef dataLabeler < handle
    
    properties
        dt;
        d; % the data
        raw_d;
    end
    
    methods
        
        function obj = dataLabeler(dtime)
            obj.dt = dtime;
            if ~exist('dtime', 'var')
                dtime = "2020_04_02_23_01_45";
                disp("No data set specified. Using April 2nd data.")
            end
            dir = "C:\Users\jorda\Documents\Stock Trading\NewsEvent3.0\Gathered_Data\";
            path = strcat(dir,dtime,"\",dtime,".mat");
            % get final structure of data
            disp("loading data")
            data = transpose( load(path).events );
            data = struct2table( data );
            sz = size(data);
            % make new columns
            data.BioSubject = repmat(false, sz(1), 1);
            data.Index = transpose(1:sz(1));
            % replace empty rows
            data.Story{31070} = ' ';
            data.Story{31071} = ' ';
            obj.raw_d = data;
            obj.d = data;
            %{
            disp("Tokenizing stories")
            tic;
            obj.d.Story_tok = tokenizedDocument(erasePunctuation(obj.d.Story), 'Language', 'en');
            toc
            %}
        end
        
        function reset(obj)
            obj.d = obj.raw_d;
        end
        
        % adds a column containing true or false if that row contains a
        % keyword.
        % toComp dictates what we compare to. can be title or story.
        % ex: addMatchCols(["keyword"], "title")
        function addMatchCols(obj, strs, toComp)
            sz_strs = size(strs);
            for i = 1:sz_strs(2)
                s = strs(i)
                indeces = [];
                if strcmp(toComp, "title")
                    indeces = contains(obj.d.Text, s, 'IgnoreCase', true);
                elseif strcmp(toComp, "story")
                    indeces = contains(obj.d.Story, s, 'IgnoreCase', true); 
                elseif strcmp(toComp, "~title")
                    indeces = ~contains(obj.d.Text, s, 'IgnoreCase', true);
                elseif strcmp(toComp, "~story")
                    indeces = ~contains(obj.d.Story, s, 'IgnoreCase', true); 
                elseif strcmp(toComp, "title & story")
                    i1 = contains(obj.d.Text, s, 'IgnoreCase', true);
                    i2 = contains(obj.d.Story, s, 'IgnoreCase', true);
                    indeces = i1 & i2;
                elseif strcmp(toComp, "title | story")
                    i1 = contains(obj.d.Text, s, 'IgnoreCase', true);
                    i2 = contains(obj.d.Story, s, 'IgnoreCase', true);
                    indeces = i1 | i2;
                elseif strcmp(toComp, "title & ~story")
                    i1 = contains(obj.d.Text, s, 'IgnoreCase', true);
                    i2 = ~contains(obj.d.Story, s, 'IgnoreCase', true);
                    indeces = i1 & i2;
                elseif strcmp(toComp, "story & ~title")
                    i1 = ~contains(obj.d.Text, s, 'IgnoreCase', true);
                    i2 = contains(obj.d.Story, s, 'IgnoreCase', true);
                    indeces = i1 & i2;
                else
                    error("invalid input for toComp");
                end
                colName = strcat(toComp,":",s);
                obj.d(:,colName) = table( indeces );
            end
        end
        
        % mark all rows with this match as true/false for biotech subject
        function markFromMatch(obj, colName, mark)
            if ~exist('mark', 'var')
                mark = true;
            end
            rows = table2array( obj.d(:,colName) );
            obj.d(rows, "BioSubject") = table(mark)
        end  
        
        %{
        function matchAndMark(obj, tocmp, kwds, tomark)
            sz = size(kwds);
            obj.addMatchCols(kwds, tocmp);
            for i = 1:sz(2)
                kw = kwds(i);
        %}        
        
        % return rows that are marked
        function out = getMarked(obj, col)
            out = obj.d( obj.d.BioSubject, : );
        end
        
        % mark the given indeces as true or false
        function mark(obj, indeces, toMark)
            obj.d(indeces, "BioSubject") = {toMark};
        end
        
        % returns the data that has matches with the given keyword
        function out = getMatches(obj, col)
            out = obj.d( obj.d{:,col}, : );
        end
        
        function markSymbol(obj, sym, toMark)
            ind = contains(upper(obj.d.Symbols), upper(sym));
            obj.d(ind, "BioSubject") = {toMark};
        end
        
        function rmCols(obj, cols)
            obj.d = removevars(obj.d,cols);
        end
        
        %function tokStoryMatch(kw)
            %any( strcmp( lower(obj.d.Story_tok.Vocabulary), lower(kw) ) );
        
    end
    
end


%{
function out = dataLabeler(dt)
    if ~exist('dt', 'var')
        dt = "2020_04_02_23_01_45";
        disp("No data set specified. Using April 2nd data.")
    end
    dir = "C:\Users\jorda\Documents\Stock Trading\NewsEvent3.0\Gathered_Data\";
    path = strcat(dir,dt,"\",dt,".mat");
    % get final structure of data
    d = transpose( load(path).events );
    d = struct2table( d );
    sz = size(d);
    % make new columns
    d.BioSubject = repmat(false, sz(1), 1);
    d.Index = transpose(1:sz(1));
    % replace empty rows
    d.Story{31070} = ' ';
    d.Story{31071} = ' ';
    % find matches
    disp("Index Matches")
    strs = ["test" "phase" "results" "bio" "therap" "gene"]; % strings to index
    sz_strs = size(strs);
    for i = 1:sz_strs(2)
        s = strs(i)
        logIndeces = contains(d.Story, s, 'IgnoreCase', true);
        d(:,s) = table( logIndeces );
    end
    
    
    
    %disp("writing table")
    %fullDir = strcat(dir,dt,"/myData.xlsx");
    %writetable(dat, strcat(dir,dt,"/myData.csv") ,'Delimiter',',*,*,','QuoteStrings',true);
    %writetable(dat,fullDir,'Sheet',1);
    out = d;
end
%}