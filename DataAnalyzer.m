%input: scraped event data, processed in dataPrepare
function DataAnalyzer(d)
    if ~exist('d','var')
        d = dataPrepare("2020_09_22_21_07_40");%2020_05_09_18_52_24");
    end
    %reactionsOverTime(d);
    %simpleTester(d);
    %timeComp(d)
    bigBulls(d);
end

function bigBulls(d)
    % price filter
    d = d( d.Start_Price < 10, :);
    
    % simple keyword filter
    logI = strcmp(simpleKWPred2(d.Text), "great");
    d = d( logI, : );
    
    size(d)
end

% a simple backtester that simulates actual investment results
function practicalBackTester(d)
    % price filter
    d = d( d.Start_Price < 10, :);
    
    % use my simple filter
    logI = strcmp(simpleKWPred(d.Text), "good");
    d = d( logI, : );

    highSell = 21.2;
    lowSell = -5;  % 5 was best for stop sell of all nonbio and non-neg events
    
    phlSell = d.TenMinPHL < lowSell;
    lowBelowSell = d.TenMinL < lowSell;
    hSell = d.TenMinH > highSell;
    over10 = d.TenMinH > 10;
    
    myTrade = repmat(-100,height(d),1);
    
    % 1x
    myTrade(phlSell) = lowSell - 1;  % -1 for bad fill price on stop sells
    
    % 0x (trailing stop)
    myTrade(~phlSell) = d.TenMinH(~phlSell) + lowSell - 1;
    
    % 01
    %logI = ~phlSell & hSell;  
    %myTrade(logI) = highSell;
    % 00
    %logI = ~phlSell & ~hSell; 
    %myTrade(logI) = d.TenMinH(logI)/2;  % trailing% stop
    
 
    %{
    myTrade = d.TenMinH;
    % case 1: phl sell triggered
    myTrade(phlSell) = lowSell;
    % case 2: phl not triggered, gains happening
    % (keep the High)
    % case 3: phl not triggered, no gain, low below sell
    myTrade(myTrade < 5 & lowBelowSell) = lowSell;
    % case 4: phl not triggered, no gain, low above sell
    logI = myTrade < 5 & ~lowBelowSell;
    myTrade(logI) = d.TenMinL(logI);
    % when i take the gains
    logI = myTrade > 5;% & myTrade < highSell; 
    myTrade( logI ) = myTrade(logI)/2;
    %myTrade( myTrade >= highSell ) = highSell;
    %}


    %{
    % price filter
    d = d( d.Start_Price < 10, :);
    
    % use my simple filter
    d = d( strcmp(simpleKWPred(d.Text), "long"), :);
    
    highSell = 21.2;
    lowSell = -2;
    
    phlSell = d.TenMinPHL < lowSell;
    lowBelowSell = d.TenMinL < lowSell;
 
    myTrade = d.TenMinH;
    % case 1: phl sell triggered
    myTrade(phlSell) = lowSell;
    % case 2: phl not triggered, gains happening
    % (keep the High)
    % case 3: phl not triggered, no gain, low below sell
    myTrade(myTrade == 0 & lowBelowSell) = lowSell;
    % case 4: phl not triggered, no gain, low above sell
    logI = myTrade == 0 & ~lowBelowSell;
    myTrade(logI) = d.TenMinL(logI);
    % when i take the gains
    logI = myTrade > 0 & myTrade < highSell; 
    myTrade( logI ) = lowSell;%myTrade(logI)/2;
    myTrade( myTrade >= highSell ) = highSell;
    %}
    
    % results
    avg = mean(myTrade)
    med = median(myTrade)
    s = size(d);
    % duration
    d = sortrows(d, 'Timestamp');
    weeks = days(d.Timestamp(end) - d.Timestamp(1))/7
    eventsPerWeek = s(1)/weeks
    strcat( string(floor(toSingleChange(myTrade))), "%")
    
    figure
    num = 40;
    histogram(myTrade,num,'facecolor','blue','facealpha',.5,'edgecolor','none')
    hold on
    %histogram(d.OneHourH,num,'facecolor','yellow','facealpha',.5,'edgecolor','none')
    %histogram(d.TwoHourH,num,'facecolor','green','facealpha',.2,'edgecolor','none')
    %histogram(d.FourHourH,num,'facecolor','blue','facealpha',.2,'edgecolor','none')
    box off
    axis tight
    legend boxoff
    %
end
    

% growth of initial investment per price cutoff (with a loss of x%)

% histogram type except average percent instead of counts


function showPlots(d)
end

% 1x2 hists: news releases per time of week and weekday
% will only work for datasets that contain all days of week
function timingHists(d)
    h = height(d);
    ts = d.Timestamp;

    figure;
    subplot(2,1,1);
    [day_numbers, days_of_week] = weekday(ts);
    days_of_week = reordercats( categorical(cellstr(days_of_week)),...
        {'Mon','Tue','Wed','Thu','Fri','Sat','Sun'} );%,...
        %{'Mon', 'Tue', 'Wen', 'Thu', 'Fri', 'Sat', 'Sun'} );
    histogram(days_of_week);
    xlabel('Day of week')
    title('Frequency and time of news releases throughout week')
    
    subplot(2,1,2);
    
    % define timestamp edges
    edges = datetime(2020,1,1,0,0,0) : minutes(30) : datetime(2020,1,2,0,0,0);
    % make all timestamps the same day
    ts = datetime(2020,1,1,hour(ts),minute(ts),second(ts));
    histogram(ts, edges);
    % add lines marking market hours
    reg = {datetime(2020,1,1,9,30,00), datetime(2020,1,1,16,0,0)};
    ext = {datetime(2020,1,1,9,30,00), datetime(2020,1,1,16,0,0)};
    line([reg{1}, reg{1}], [0, 100], 'Color','red','LineStyle','--');
    line([reg{2}, reg{2}], [0, 100], 'Color','red','LineStyle','--');
    % labels
    xlabel('Time of day (ET), 30min bars')    
end

% show frequency counts of events per their start_price
function priceHist(d)
    histogram( d.Start_Price(d.Start_Price<100), 20)
    xlabel("Start Price (USD)")
    ylabel("Amount of Events")
end

% scatterplot of start_price against oneHourHigh and PHL
function priceAffect(d)
    x = d.Start_Price;
    y = d.OneHourH;
    y_phl = d.OneHourPHL;
    scatter(x,y,'bo')
    hold on;
    scatter(x,y_phl,'ro')
    xlabel("Start Price (USD)");
    ylabel("1-hour change (%)");
    legend("High","Pre-high Low")
    title(corr2(x,y))
end

% for choosing price cutoff. Shows average reaction per price cutoffs
function priceAffect2(d)
    x = repmat(-5, 1001, 1);
    y = x;
    y2 = x;
    y_10 = x;
    y_L = x;
    y_PHL = x;
    i = 1;
    for price_cutoff=0:0.1:100
        x(i) = price_cutoff;
        lind = d.Start_Price < price_cutoff;
        y(i) = mean(d.OneHourH(lind));
        y2(i) = median(d.OneHourH(lind));
        y_10(i) = median(d.TenMinH(lind));
        y_L(i) = mean(d.OneHourL(lind));
        y_PHL(i) = median(d.OneHourPHL(lind));
        i = i+1;
    end
    
    % Display correlations
    disp( strcat("Correlation of median price to 10min H: ",...
    string(corr2(x,y_10))) )
    
    % Plot
    y(isnan(y)) = 0;
    plot(x,y)
    hold;
    plot(x,y2);
    plot(x,y_10);
    plot(x,y_L);
    plot(x,y_PHL);
    grid on;
    xlabel("Max Stock Start Price Allowed (USD)");
    ylabel("1-hour High (%)");
    legend("1HourMean", "1HourMedian", "10MinMedian","1HourLowMean","1HourPHLMedian");
    title("Affect of Stock Price on Stock Reaction")
end

function priceAffect4(d)
    x = repmat(-5, 1001, 1);
    y = x;
    y2 = x;
    y_10 = x;
    y_L = x;
    y_PHL = x;
    i = 1;
    for price_cutoff=2:4:100
        x(i) = price_cutoff;
        lind = (d.Start_Price < price_cutoff + 2) & (d.Start_Price > price_cutoff - 2);
        y(i) = mean(d.OneHourH(lind));
        y2(i) = median(d.OneHourH(lind));
        y_10(i) = median(d.TenMinH(lind));
        y_L(i) = mean(d.OneHourL(lind));
        y_PHL(i) = median(d.OneHourPHL(lind));
        i = i+1;
    end
        
    % Display correlations
    disp( strcat("Correlation of median price to 10min H: ",...
    string(corr2(x,y_10))) )
    
    % Plot
    y(isnan(y)) = 0;
    bar(x(1:20),y(1:20))
    hold;
    grid on;
    xlabel("Stock Price +- 0.5");
    ylabel("Mean 1-hour High (%)");
    legend("1HourMean", "1HourMedian", "10MinMedian","1HourLowMean","1HourPHLMedian");
    title("Affect of Stock Price on Stock Reaction")
end

% scatterplot of start_price against oneHourHigh and PHL
function priceAffect3(d)
    lind = d.Start_Price < 10;
    x = d.TenMinPHL(lind);
    y = d.TenMinH(lind);
    %y = reaction
    scatter(x,y);
    xlabel("1-hr PHL");
    ylabel("1-hr High");
    xlim([-100,0]);
    ylim([0,100])
    %legend("High","Pre-high Low")
    title(corr2(x,y))
end

% show histogram of 10 min highs
function histsH(d)
    figure
    num = 40;
    histogram(d.TenMinH,num,'facecolor','blue','facealpha',.5,'edgecolor','none')
    hold on
    %histogram(d.OneHourH,num,'facecolor','yellow','facealpha',.5,'edgecolor','none')
    %histogram(d.TwoHourH,num,'facecolor','green','facealpha',.2,'edgecolor','none')
    %histogram(d.FourHourH,num,'facecolor','blue','facealpha',.2,'edgecolor','none')
    box off
    axis tight
    legend boxoff
end

% show histogram of frequency counts of One Hour reactions
function histsOneHour(d)
    figure
    num = 40;
    histogram(d.OneHourH,num,'facecolor','red','facealpha',1,'edgecolor','black')
    hold on
    histogram(d.OneHourPHL,num,'facecolor','yellow','facealpha',1,'edgecolor','none')
    histogram(d.OneHourL,num,'facecolor','green','facealpha',1,'edgecolor','none')
    box off
    axis tight
    legend boxoff
end

% 2x2 plots of Highs/Lows over time (line graph)
function reactionsOverTime(d)
    figure
    subplot(2,2,1)
    style = '-';
    t = d.Timestamp;
    plot(t,d.TenMinH, strcat('g',style))
    hold on
    plot(t,d.TenMinPHL, strcat('b',style))
    plot(t,d.TenMinL, strcat('r',style))
    ylim([-100,400])
    ylabel('Event Change (%)');
    xlabel('Date-time');
    title('Ten Minute High/Low')
    
    subplot(2,2,2)
    plot(t,d.OneHourH, strcat('g',style))
    hold on
    plot(t,d.OneHourPHL, strcat('b',style))
    plot(t,d.OneHourL, strcat('r',style))
    ylabel('Event Change (%)');
    xlabel('Date-time');
    title('One Hour High/Low')
    %hold on
    
    subplot(2,2,3)
    plot(t,d.TwoHourH, strcat('g',style))
    hold on
    plot(t,d.TwoHourPHL, strcat('b',style))
    plot(t,d.TwoHourL, strcat('r',style))
    ylabel('Event Change (%)');
    xlabel('Date-time');
    title('Two Hour High/Low')
    
    subplot(2,2,4)
    plot(t,d.FourHourH, strcat('g',style))
    hold on
    plot(t,d.FourHourPHL, strcat('b',style))
    plot(t,d.FourHourL, strcat('r',style))
    ylabel('Event Change (%)');
    xlabel('Date-time');
    title('Four Hour High/Low')
end

% Dot representation of 10-min, 1-hr, 2-hr, 4-hr High
function timeComp(d)
    figure
    style = '.';
    t = d.Timestamp;
    plot(t,d.TenMinH, strcat('b',style))
    hold on
    plot(t,d.OneHourH, strcat('g',style))
    plot(t,d.TwoHourH, strcat('r',style))
    plot(t,d.FourHourH, strcat('c',style))
    title('Time Comparison of Highs')
    ylabel('Event Change (%)');
    xlabel('Date-time');
    %hold on
end

% HELPER FUNCTIONS

% same criteria as realtime NewsEvent big bull detector
function pred = simpleKWPred2(strA)
    s = size(strA);
    pred = repmat("",s(1),1);  % string array
    pred = repmat("",s(1),1);  % string array
    verb = ["reports", "presents", "announces", "initiates", "outlines"];
    noun = ["complet", "results", "resu...", "resul...", "result...", ...
        "data", "safety", "study"];
    pos = ["positiv", "beneficial", "reduces risk", "reduced risk", "excellent"...
        ,"safe ","improve","breakthrough","effective", "meets end"...
        ,"success"];
    neg = ["did not meet", "stops", "fail", "did not succeed", "no benefit", "not work"...
        ,"not benefit", "plunge", "discontinue", "will not"];%, "to be present", "to present"];
    nonbio = ["stock", "surge", "shares", "company", "aquire", "aquisition", "plans"];
    
    % Returns a logical array corresponding to whether each input event
    % contains any of the strings in the input keyphrase array.
    % input: list of keyphrase triggers, list of documents to check against
    function logI = containsAny(triggers, strA)
        s = size(strA);
        logI = repmat(0,s(1),1);
        for i=1:triggers.length
            keyphrase = lower(triggers(i));  % keyphrase
            logI = logI | contains(lower(strA), keyphrase);
        end
    end
    
    verbI = containsAny(verb, strA);
    nounI = containsAny(noun, strA);
    posI = containsAny(pos, strA);
    negI = containsAny(neg, strA);
    nonbioI = containsAny(nonbio, strA);
    
    %pred( posI & verbI & nounI ) = "long";
    %pred( negI ) = "short";
    %pred( nonbioI ) = "nonbio";
    %pred( ~nonbioI & ~negI & posI ) = "good";
    %pred( ~negI & ~nonbioI ) = "good";
    %pred( ~nonbioI & ~negI & posI & verbI & nounI ) = "good";
    pred( ~negI & ~nonbioI & posI & verbI & nounI ) = "great";
end
        
% predicts categorical result (long, short, neutral) for inputed documents
% Predicts categorical result (long, short, neutral) for inputed documents
% via simple keyword triggers.
function pred = simpleKWPred(strA)
    s = size(strA);
    pred = repmat("",s(1),1);  % string array
    pred = repmat("",s(1),1);  % string array
    verb = ["reports", "presents", "announces", "initiates", "outlines"];
    noun = ["complet", "results", "resu...", "resul...", "result...", ...
        "data", "safety", "study"];
    pos = ["positiv", "beneficial", "reduces risk", "reduced risk", "excellent"...
        ,"safe ","improve","breakthrough","effective", "meets end"...
        ,"success"];
    neg = ["did not meet", "stops", "fail", "did not succeed", "no benefit", "not work"...
        ,"not benefit", "plunge", "discontinue", "will not"];%, "to be present", "to present"];
    nonbio = ["stock", "surge", "shares", "company", "aquire", "aquisition", "plans"];
    
    % Returns a logical array corresponding to whether each input event
    % contains any of the strings in the input keyphrase array.
    % input: list of keyphrase triggers, list of documents to check against
    function logI = containsAny(triggers, strA)
        s = size(strA);
        logI = repmat(0,s(1),1);
        for i=1:triggers.length
            keyphrase = lower(triggers(i));  % keyphrase
            logI = logI | contains(lower(strA), keyphrase);
        end
    end
    
    verbI = containsAny(verb, strA);
    nounI = containsAny(noun, strA);
    posI = containsAny(pos, strA);
    negI = containsAny(neg, strA);
    nonbioI = containsAny(nonbio, strA);
    
    %pred( posI & verbI & nounI ) = "long";
    %pred( negI ) = "short";
    %pred( nonbioI ) = "nonbio";
    %pred( ~nonbioI & ~negI & posI ) = "good";
    %pred( ~negI & ~nonbioI ) = "good";
    pred( ~nonbioI & ~negI & posI & verbI & nounI ) = "good";
end

% converts array of percent changes into single ending percent change
function endChange = toSingleChange(changes)
    % need changes in form 1.1 for 10% and .9 for -10%
    changes = changes/100 + 1;
    % multiply all those together to get ending "unit change"
    endNormValue = prod(changes);
    % convert back to Percent Increase form
    endChange = (endNormValue - 1)*100;
    % extra info
    disp( strcat("After these ",string(length(changes))," trades, $1000 becomes $", string(1000*endNormValue)) );
end
    
%{
d = sortrows(d,'OneHourH','descend');
head(d(:,["Text","OneHourH"]),10)
%}
