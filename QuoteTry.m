%total volume = total daily volume up to the quote bar
%interval size: in seconds
%newsList = IQML('news', 'DataType','headlines', 'Timeout', 60, 'GetStory', false, 'MaxItems',10000, 'UseParallel', 1, 'Date', pubDate);
%news = newsList(1);

news.Symbols = ['NAII'];
news.Timestamp = 2.020030513000000e+13;%20200305130000
news.ID = 2.225487610400000e+10;

news.Story = IQML('news', 'DataType','story', 'ID', news.ID).Text;



try
    news.Price = IQML('history', 'symbol',news.Symbols, 'dataType','interval', 'intervalSize', 60, 'LabelAtBeginning', false, ...
        'BeginDateTime', news.Timestamp, 'EndDateTime', news.Timestamp, 'MaxItems', 1)
    disp(news.Price)
catch err
    if strcmp(err.identifier,'IQML:IQFeedError')
        if ismember('NO_DATA', err.message)
            disp('caught no data');
        end
    else
        error("Error caught");
    end
end

try
    news.Five_Min_Vol = IQML('history', 'symbol',news.Symbols, 'dataType','interval', 'intervalSize', 60*5, 'LabelAtBeginning', true, ...
        'BeginDateTime', news.Timestamp, 'EndDateTime', news.Timestamp, 'MaxItems', 1).PeriodVolume;
catch msg
    error("Error caught")
end

data = IQML('history', 'dataType','ticks', 'UseParallel', false, 'symbol', 'NAII', 'EndDateTime', 20200305130000, 'MaxItems', 1)