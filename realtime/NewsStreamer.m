classdef NewsStreamer < handle
    % When new events are found on the IQML internal buffer, they are saved in
    % current_ev_batch. In subsequent calls to poll_event(), the oldest event in
    % current_ev_batch will be returned until the batch is emptied. If the batch
    % is empty, calls to poll_event will first check for new batches in the IQML
    % buffer, then return the oldest unpolled event (if any) in the buffer.
    
    % TODO: create testing class to ensure correct functioning after any
    % changes or IQML updates
        
    properties (Constant)
        % Size of internal IQML news event cyclic buffer
        %   Once full, the operation of adding to it will take the time of 
        %   shifing the previous events up. To avoid this, make it big
        %   enough for one day of events then clear the buffer during low
        %   event activity (todo: turn frequency hist into LUT and use it).
        EV_BUF_SZ = 5000; % size of IQML news event stream's internal cyclic buffer
        % When back testing, the amount of events to load into buffer
        %NUM_BT_EV = 1000; % When back testing, the amount of events to load into buffer (1000 max)
        BT_START = datetime(2020,6,1);  % start day for backtesting day (must be less than 6mo ago)
        NUM_PRE_SYMS = 1000; % Number of symbols with which to pre-populate symbol map at start
        SRC_ENUM = containers.Map(...
            struct2table(IQML('news', 'DataType','config').Majors).Source,...
            struct2table(IQML('news', 'DataType','config').Majors).Description);  % News source abbreviation enum
    end
    
    properties (SetAccess = private)  % GetAccess remains public
        n_polled_ev;  % Number of events that the client has polled (may briefly be less than the amount IQML has seen)
        stream_started;  % Bool indicating if this object has initialized an IQML news stream
        current_ev_batch;  % A stack for unpolled-by-client events
        back_test;  % Bool: For streaming past data instead of realtime data
        bt_day; % Day from which the backtested data is currently pulled from
        hold;  % When holding, the articles will go into queue without being processed
        symbol_map;  % Map of symbols to the time of their newest article mention
    end
    
    % PUBLIC METHODS
    methods
        
        % constructor
        function obj=NewsStreamer(arg)
            obj.n_polled_ev = 0;
            obj.stream_started = false;
            obj.current_ev_batch = [];
            obj.back_test = false;
            obj.bt_day = obj.BT_START;
            obj.hold = false;
            obj.symbol_map = containers.Map('KeyType', 'char', 'ValueType', 'any');
            
            if ~exist('arg', 'var')
                % do nothing
            elseif strcmp(arg,'back_test')
                obj.back_test = true;
            end
        end
                       
        function start_stream(obj)
            % Unless backtesting--loads recent data to symbol map for 
            % duplicate testing, then restarts the poll-able news data 
            % stream with a cleared buffer.

            if ~obj.back_test
                % Pre-populate symbol-list
                obj.add_to_symbol_map( flip( IQML('news', 'MaxItems', obj.NUM_PRE_SYMS), 1 ) );
                % Start IQML stream
                obj.start_IQML_stream();
            end
        end
                
        function event = poll_event(obj)
            % output = newest unpolled event (if no hold)
            % There may be newer unpolled events in current_ev_batch, but we
            % only want to grab one at a time, oldest to newest. Also
            % attach time of 
            % See description at head of file for more details.
            event = [];
            if ~obj.hold
                % First, check for data
                if isempty(obj.current_ev_batch)
                    if obj.back_test
                        % For backtesting                            
                        if obj.bt_day > datetime
                            disp("NewsStreamer: Past data exhausted. Switching to real-time data.")
                            obj.back_test = false;
                            obj.start_IQML_stream();
                        else
                            disp(strcat("NewsStreamer: Getting new batch from ",string(obj.bt_day), "..."))
                            try
                                obj.current_ev_batch = flip( IQML('news', 'DataType',...
                                    'headlines', 'Timeout', 10, 'GetStory', false, 'MaxItems',...
                                    inf, 'UseParallel', 1, 'Date', obj.bt_day), 1 );
                            catch err
                                disp('Failed. Trying again...')
                                obj.current_ev_batch = flip( IQML('news', 'DataType',...
                                    'headlines', 'Timeout', 10, 'GetStory', false, 'MaxItems',...
                                    inf, 'UseParallel', 1, 'Date', obj.bt_day), 1 );
                            end
                            %obj.current_ev_batch = IQML('news', 'NumOfEvents', obj.NUM_BT_EV);
                            %obj.n_polled_ev = 0;
                            disp("NewsStreamer: Done.")
                            obj.bt_day = obj.bt_day + days(1);
                        end
                    else
                        % For real-time streaming
                        obj.poll_event_batch()
                    end
                end
                % If there's data, grab it
                if ~isempty(obj.current_ev_batch)               
                    % Get latest unpolled event
                    event = obj.current_ev_batch(1);
                    % update fields
                    obj.n_polled_ev = obj.n_polled_ev + 1;
                    obj.current_ev_batch = obj.current_ev_batch(2:end);
                end
                % Simulating sometimes no event for past data
                %if obj.back_test && ~mod(obj.n_polled_ev,2)
                %    event = [];
                %end
            end
            if ~isempty(event)
                % Add to symbol map and mark last mention time (empty if long ago)
                event.last_mention = obj.add_to_symbol_map(event);
                event.back_test = obj.back_test;
                % Change news source data to full description if possible
                if isKey(obj.SRC_ENUM, event.Source)
                    event.Source = obj.SRC_ENUM(event.Source);
                end
            end
        end        
        
        function hold_on(obj)
            % While holding, the articles will go into queue without being
            % processed by me.
            obj.hold = true;
            timeDisp("NewsStreamer hold ON.");
        end
        
        function held=hold_off(obj)
            % If called when holding, normal processing of articles will 
            % resume after flushing out and returning all held articles.
            held = obj.flush();
            obj.add_to_symbol_map(held);
            obj.hold = false;
            timeDisp("NewsStreamer hold OFF.");
        end
        
        function flushed_ev=flush(obj)
            % Throws out and returns all current and past news events, so
            % next call to 'poll' will only get new events.
            
            % Get all new unpolled events
            cur_batch = obj.current_ev_batch;
            obj.poll_event_batch();
            new_batch = obj.current_ev_batch;
            obj.current_ev_batch = [];
            % Mark them as polled
            amt = length(cur_batch) + length(new_batch);
            obj.n_polled_ev = obj.n_polled_ev + amt;
            flushed_ev = [cur_batch; new_batch];
            disp( strcat("Flushed ",string(amt), " events.") );
        end    
        
        function show_events(obj, to_show)
            % Displays given events
            for i=1:length(to_show)
                % Extract
                event = to_show(i);
                et = string(event.Timestamp);
                local_time = datetime;
                syms = event.Symbols;
                text = event.Text;
                id = event.ID;
                % Time
                ipf = 'yyyyMMddHHmmss';
                if contains( et, " " )
                    ipf = 'yyyyMMdd HHmmss';
                end
                event_time = datetime( et, 'InputFormat', ipf);    
                % Display
                disp(newline)
                timeDisp( strcat("Article latency: ",...
                    string(seconds(local_time + hours(3) - event_time)), " sec") )
                disp(strcat("Title: ", text))
                disp(strcat("Symbols: ", string(syms)))
                disp(strcat("ID: ", string(id)))
                % Process
                % story = IQML('news', 'DataType','story', 'ID', event.ID)
            end
        end
        
    end
    
    methods (Access = private)
        
        function start_IQML_stream(obj)
            % Restarts the poll-able news data stream with a
            % cleared buffer.
            obj.stop_IQML_stream();
            obj.n_polled_ev = 0;
            % Restart stream
            IQML('news', 'NumOfEvents', inf, 'MaxItems', obj.EV_BUF_SZ);
            obj.stream_started = true;
            assert( obj.snapshot().EventsProcessed == 0 );
            timeDisp("IQML stream restarted.");
        end
    
        function poll_event_batch(obj)
            % If you don't care about getting multiple-event batches, this is
            % the quickest way to poll from buffer (output = current_ev_batch).
            assert(obj.stream_started, "Cannot poll events without starting event stream");
            % Get instantaneous status of event stream & buffer
            stream_snapshot = obj.snapshot();
            if ~stream_snapshot.isActive
                % This should only happen if EventsProcessed >= EventsToProcess
                % EventsToProcess is set to inf
                warning("NewsStreamer: Event stream is inactive. Restarting");
                obj.start_IQML_stream();
            end
            % IQML's EventsProcessed var refers to num of events streamed to
            % its internal buffer so far
            n_unpolled_ev = stream_snapshot.EventsProcessed - obj.n_polled_ev;
            if n_unpolled_ev > 0
                if n_unpolled_ev >= obj.EV_BUF_SZ
                    disp(obj.n_polled_ev);
                    disp(stream_snapshot);
                end
                % Handle new events
                % index of newest event in buffer 
                i_top = min(obj.EV_BUF_SZ, stream_snapshot.EventsProcessed);
                % index of oldest unprocessed event in buffer
                i_bot = i_top + 1 - n_unpolled_ev;
                % Final output
                obj.current_ev_batch = stream_snapshot.Buffer(i_bot:i_top);
            else
                % should never be less than 0
                assert(n_unpolled_ev == 0, strcat("Error 1 -> ",string(n_unpolled_ev)));
            end
        end
        
        function recent = add_to_symbol_map(obj, batch)
            % The symbols and associated timestamps of given event batch
            % will be added to the symbol map. If only one event is passed
            % and it has 1 symbol, the time of the last article mention of 
            % the symbol will be returned. If last time was too long ago,
            % will return empty.
            willReturn = true;  % true if only 1 event in batch and it has exactly 1 symbol
            recent = [];
            if length(batch) > 1 || isempty(batch)
                willReturn = false;
            end
            for i = 1:length(batch)
                ev = batch(i);
                syms = unique( upper(string(ev.Symbols)) );  % for timetable column
                t = datetime(string(ev.Timestamp),'InputFormat','yyyyMMddHHmmss'); % event time
                num_syms = length(syms);
                if num_syms ~= 1
                    willReturn = false;
                end
                if num_syms < 6 % to ignore super general articles
                    for ii = 1:num_syms
                        % add all event syms to symbol_map
                        sym = syms(ii);
                        if isKey(obj.symbol_map, sym)
                            val = obj.symbol_map(sym);
                            if t >= val
                                recent = val;
                                obj.symbol_map(sym) = t;
                            else
                                % Remove this "else" if you only want to return
                                % most recent IFF the time is equal or
                                % before the current event's time. Keeping
                                % this will allow the function to return a
                                % time that comes AFTER the time of the
                                % currently polled event.
                                warning("NewsStreamer: Newer event already seen for below event's symbol...")
                                disp(NewsEvent(ev));
                                recent = val;
                            end
                        else
                            obj.symbol_map(sym) = t;
                        end
                    end
                end
            end
            if ~willReturn
                recent = [];
            end
        end
        
        %{
        function start_IQML_stream_old(obj)
            % Start a poll-able news data stream (or restart, if already started)
            %   NumOfEvents = inf (so stream doesn't stop after a certain # of events)
            %   MaxItems (size of the event buffer)
            %       Event buffer holds events from oldest to newest
            
            % This wasn't detecting that the stream was already started, so
            % I just made the "restart_stream" function the "start"
            % function.
            
            try 
                IQML('news', 'NumOfEvents', inf, 'MaxItems', obj.EV_BUF_SZ);
                timeDisp("Activated IQML event stream.");
            catch err
                timeDisp("Event stream is already active. Restarting.")
                obj.start_IQML_stream();
            end
            obj.n_polled_ev = 0;    
            obj.stream_started = true;
        end
        %}
        
    end
    
    methods (Static)
        
        function stop_IQML_stream(obj)
            % Setting tha param to 0 stops the stream
            pause(2);
            IQML('news', 'NumOfEvents', 0);
        end
        
        function snap=snapshot(obj)
            % Get IQML stream snapshot
            snap = IQML('news', 'NumOfEvents', -1);  % (7ms avg)
        end
        
    end

end

function timeDisp(str)
        t = string(datetime);
        disp( strcat("(", t, " PST) ", str) );
end
