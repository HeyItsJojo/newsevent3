% NewsEventMain
% Currently, this and all used classes are hard-coded to work with NASDAQ
% bio stocks.

% Run this file with "Pause on Errors" checked to go into debug on error.

% "Initializer" function
function NewsEventMain()

    % User-specified Params
    back_test = false;  % if true, the loop will run over repeating old data and only alert me
    alert_me = true;  % text alert (for big_bull)
    alert_others = true;  % text alert (for big_bull)

    % Shutdown handle function
    cleanupObj = onCleanup(@cleanMeUp);
    
    % Handle Params
    args = containers.Map(...
        {'back_test', 'alert_me', 'alert_others'},...
        {back_test  , alert_me  , alert_others}...
        );
    
    % Run Desired Function
    main(args)  % use 'back_test' for past-data testing
    %test()
    %test_alert();
    %test_broker()
end

% Main function
function main(args)
    % handle args
    if ~exist('args', 'var')
        % Default operation
        args = containers.Map(...
            {'back_test', 'alert_me', 'alert_others'},...
            {false      , true      , true}...
            );
    end
    option = '';
    if args('back_test')
        option = 'back_test';
    end
    
    % Object for handling news event stream
    n_stream = NewsStreamer(option);
    % Security Data object for quick symbol viability checking
    sd = SecurityData();
    % Load categorical model
    cat_mdl = CategoryModel();
    % Interactive Brokers
    if args.back_test
        % todo: implement BrokerSim
        broker = BrokerSim();
    else
        broker = Boker();
    end
    
    % Save incoming events for next poll_events()
    n_stream.start_stream();
    % Download newest version of symbol data if newer is available (~3 min)
    % todo: call this function every night at inactive time
    sd.update();

    timeDisp("Kernel started.");
    while (true)
        
        broker.main();  % If shares are owned, this ensures a sell order
        news = n_stream.poll_event();  % (7ms avg)
        % New Article?
        if isempty(news)
            % If there isn't a new news release
            continue
        end
        event = NewsEvent(news);  % (1.5ms avg)      
        % Filter Basic Viability 
        if ~event.viable(sd)  % (~15ms max, 3ms avg)
            % If the event simply isn't tradeble or if I'm not interested
            continue
        end
        % Filter Major Keyword Negations
        if strcmp(event.simple_kw_pred(), "skip")
            continue
        end
        % Filter Duplicates
        if event.time_since_mention < hours(2)
            % most false duplicates occur for larger cap -- which we care
            % less about
            %warning(strcat("A DUPLICATE BELOW?   ",string(event.time_since_mention)))
            %disp(strcat("Skipping duplicate non-categorized event about ",event.symbols))
            continue
        end
        % Filter Category
        if event.get_category(cat_mdl) ~= "Bio" % (100ms avg)
            continue
        end
        % Add Start Price and check if tradeable (~300ms)
        last = get_last(event.symbols);
        if isempty(last)
            % Means that the symbol isn't tradeable
            continue
        end
        event.set_start_price( last );
        big_bull = event.start_price < 10 && strcmp( event.simple_kw_pred, "great" );
           
        % BUY
        shares_bought = broker.buy(event, 1500);
        
        % Display (and alert if backtesting)
        if big_bull
            disp("BIG BULL")
            if args('alert_me')
                send_news_alert( event, true );
            end
            if ~args('back_test') && args('alert_others')
                send_news_alert( event, false );
            end
        end
        disp(event)        
    end
end

% todo: 
% - Sign up for IB nasdaq realtime lvl1 data  and see if it is faster and
% better than my up-to-330ms IQML quotes. This is because sometimes it's
% just not working right now to get a simple quote. Try getting quote from
% IB.

% HANDLE EVENT

% make class for specific cat model. Make the file a general
% categorical prediction model, which will be updated to better
% models via git version control, with the version in comment. Time
% the model to check speed of text preprocessing and prediction.

% predict category and show prediction in show_events. do a toc of
% end-to-end time



% add flag indicating if it was first trade of the day. if so,
% maybe i should wait more than 10min (10min from second
% trade?)

%{ 
Notes:
-Shouldn't consider tickers that have already had an event within the last
hour
-For buying, i should use a limit order to put a limit on how bad a price
i'm willing to take, or a mkt order to get fastest possible fill.
-For selling, i should use a smart, stop or stop-limit for safety. the stop triggers a mkt
 order, and the limit specifies the worst price i'm willing to take
%}

function test_alert()
    % Fabricate an IQML news object
    iqml_news = struct(...
        "Text", {"This is the title."},...
        "Symbols", {"TSLA"},...
        "Timestamp", {"20200624110000"},...
        "ID", {3243533},...
        "Source", {"Business Wire"} );
    % Create a NewsEvent object
    event = NewsEvent(iqml_news);
    % Add Start Price and check if tradeable (~300ms)
    last = get_last(event.symbols);
    if isempty(last)
        error("Invalid symbol")
    end
    event.set_start_price( last );
    % Alert
    send_news_alert(event, true);
    % Success
    disp("Alert test done.")
end  

function test_broker()
    % Fabricate an IQML news object
    %{
    iqml_news = struct(...
        "Text", {"headline text..."},...
        "Symbols", {"AAPL"},...
        "Timestamp", {"20200624110000"},...
        "ID", {3243533},...
        "Source", {"Business Wire"} );
    %}
    iqml_news = struct(...
        "Text", {"headline text..."},...
        "Symbols", {"aapl"},...
        "Timestamp", {"20200624110000"},...
        "ID", {3243533},...
        "Source", {"Business Wire"} );
    % Create a NewsEvent object
    event = NewsEvent(iqml_news);
    % Add Start Price and check if tradeable (~300ms)
    p = get_last(event.symbols);
    assert(~isempty(p))
    event.set_start_price( p );
    % Trade
    b = Broker();
    n_shares = b.buy(event, 1500)
    disp("Calling main() twice")
    %pause(2);
    %b.main()
    %b.main()
    b.delete
    % Success
    disp("Test #1 done.")
end        

function test2()    
    stream = NewsStreamer('back_test');
    sd = SecurityData();
    
    stream.start_IQML_stream();
    
    disp('started')
    while true
        ev = stream.poll_event();
        if isempty(ev)
            continue
        end
        event = NewsEvent( ev );
        tic;
        v = event.viable( sd );
        t = toc
        if t > .1
            disp(t)
        end
    end 
end

function last=get_last(sym)
    % Return latest trade, or empty if stock isn't available to IQML
    try
        snap = IQML('quotes', 'symbol', sym, 'NumOfEvents', 1);
    catch err
        % last remains empty
        warning("Symbol not available to IQML.")
    end
    if exist('snap','var')
        last = snap.Most_Recent_Trade;
    else
        last = [];
    end
end

function send_news_alert(event, admin)
    % Send NewsEvent text alert to admin (me), or everyone else.
    if ~exist('admin', 'var')
        admin = true;
    end
    
    % Create Message
    message = strcat(...
        "Biotech news for ", upper(event.symbols),...
        " ($", string(event.start_price), "/share)",...
        " released ", string(event.release_time - hours(3)), ' PST --- "',...
        event.headline, '"'...
    );
    
    % Create list of recipients
    recipients = struct(...
        'name',    {'Jeff C'},...
        'number',  {'4257858370'},...
        'carrier', {'verizon'}...
    );
    if admin
        recipients = struct(...
            'name',    {'Jordan C'},...
            'number',  {'4255309554'},...
            'carrier', {'xfinity'}...
        );
    end
    
    disp("Sent alert to:")
    
    % Send Texts
    for i=1:length(recipients)
        r = recipients(1,i);
        send_text_message(r.number, r.carrier, message);
        timeDisp(r.name)
    end
end        

% display string with timestamp preceding it
function timeDisp(str)
        t = string(datetime);
        disp( strcat("(", t, " PST) ", str) );
end

% shutdown handle
function cleanMeUp()
    NewsStreamer.stop_IQML_stream();
    disp("NewsEventMain ended.")
end