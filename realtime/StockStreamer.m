classdef StockStreamer < handle & matlab.mixin.CustomDisplay
    % After creating object, start_stream(sym) for desired symbol. Get
    % subsequent last price data from get_last(sym). Stop stream to make it
    % inactive.
    
    % Use "doc NewsStreamer" to get overview
    
    % Inheritance:
    %   handle - for making the class behave like I'm used to
    %   CustomDisplay - for overriding some "toString" methods
    
    properties (SetAccess = private)
        sym;
        %stream_started;
        back_test;
        stream_start;  % time of last stream start. For making sure it isn't stopped too soon
    end
    
    methods
        
        function obj=StockStreamer(arg)
            obj.sym = [];  % use TST$Y for testing outside market hours
            %obj.stream_started = false;
            obj.back_test = false;
            
            if ~exist('arg', 'var')
                % do nothing
            elseif strcmp(arg,'back_test')
                obj.back_test = true;
            end
        end
        
        function delete(obj)
            % DESTRUCTOR
            % This function is called implicitly by matlab before it
            % it garbage collects a Broker object.
            obj.stop_stream();
            disp("StockStreamer object destructed")
        end
        
        function start_stream(obj, symbol)
            % Unless backtesting--restarts stock data stream for specified
            % symbol. Must wait a short while after this call before
            % accessing stream data.
            obj.sym = upper( symbol );
            obj.stream_start = datetime;
            %if ~obj.back_test
            %    % Start IQML stream
            %    obj.start_IQML_stream();
            %end
            obj.start_IQML_stream();
        end 
        
        function last=get_last(obj, symbol)
            % Get last trade price
            last = obj.get_last_helper(symbol, datetime, true);           
        end            
        
    end
    
    methods (Access = private)
        function start_IQML_stream(obj)
            % Initiates symbol in an active IQML stream.
            % Will print IQFeed error message for non-tradeable symbols.
            IQML('quotes', 'Symbol', obj.sym, 'NumOfEvents', inf);
            %obj.stream_started = true;
        end
        function snap=snapshot_(obj, symbols)
            % Return snapshot of object's saved symbol if none specified,
            % or for specific symbols if specified.
            if ~exist('symbols', 'var')
                snap = obj.snapshot(obj.sym);
            else
                snap = obj.snapshot(symbols);
            end
        end
        function last = get_last_helper(obj, symbol, start, first)
            % Recursive helper
            % start: datetime start of recursive calls
            % first: boolean indicating if this is the first call
            snap = obj.snapshot_(); % todo later: add capability for streaming multiple symbols
            assert( strcmpi( symbol, snap.Symbol ), "Requested symbol did not get a start_stream()" );
            latest = snap.LatestData;
            if isempty(latest) && ~snap.isActive
                % This can occur in one of these cases:
                %   (1) The symbol is not tradeable. It threw an IQMLerror
                %       so it was deactivated and didn't grab data.
                %   (2) The stream was stopped immediately after it started
                %       (~100ms) so it didn't get a chance to grab any data
                %       before ending
                last = [];
            elseif isempty(latest)
                % This can occur if you request data for a tradeable or 
                % non-tradeable stock too soon after starting the symbol's stream.
                if (datetime - start > seconds(.3))
                    % By now, a tradeable stock should've gotten data,
                    % and a non-tradeable stock should've become inactive.
                    error(strcat("StockStreamer: quote is empty for ",upper(symbol))); 
                else 
                    pause(.005);
                    last = obj.get_last_helper(symbol, start, false);
                    if first
                        disp(strcat("StockStreamer: Took ",...
                            string(milliseconds(datetime - start)),...
                            "ms for stock stream to recieve quote data for ",...
                            upper(symbol), " then for me to retrieve it."));
                    end
                end
            else
                last = latest.Most_Recent_Trade;
            end
        end
    end
    
    methods (Static)
        function stop_stream(symbols)
            % Stop streams for the specified symbols, or all symbols if
            % none specified. Stopping a symbol stream within 100ms of 
            % starting it can cause problems (see get_last_helper). This
            % should be called when done with a stock, becuase multiple
            % active ("streaming") symbols can use CPU. Also, IQML limits
            % amount to 500.
                
            if ~exist('symbols', 'var')
                IQML('quotes', 'NumOfEvents', 0);
                %obj.stream_started = false;
            else
                IQML('quotes', 'symbol', symbols, 'NumOfEvents', 0);
            end
        end
        function snap=snapshot(symbols)
            % Get IQML snapshot for active and inactive stock quote
            % streams or for specified stock(s)
            
            % todo: to get faster quotes, use "AssignTo" IQML field
            if ~exist('symbols', 'var')
                snap = IQML('quotes', 'NumOfEvents', -1);
            else
                snap = IQML('quotes', 'symbol', symbols, 'NumOfEvents', -1);
                if ~snap.isActive
                    % If the stream is inactive, its data isn't updating
                    % Occurs also for non-tradeable symbols
                    %warning("StockStreamer: Retrieved snapshot for inactive stock stream")
                end
            end     
        end
    end
    
end
    

