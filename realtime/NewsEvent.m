classdef NewsEvent < handle & matlab.mixin.CustomDisplay
    % This class represents a news event, which contains a news release and
    % associated stock data.
    % 
    % NewsEvent Public Methods:
    %   viable(SecurityData=None) - returns true if event is viable
    %   get_headline() - returns event's headline
    
    % Use "doc NewsEvent" to get overview
    
    % Inheritance:
    %   handle - for making the class behave like I'm used to
    %   CustomDisplay - for overriding the "toString" method
    
    % TODO: create story accessor    
    properties(Constant)
        %DUPLICATE_T = hours(1);  % Max duration between article mentions of a symbol for them to be considered duplicates
    end
    
    properties (SetAccess = private)
        headline;
        symbols;
        release_time;  % Eastern Time
        id;
        source;   % Press release company source
        last_mention;
        viability_status;  % Last reason for denying viability
        start_price;
        back_test;
    end
    
    properties (Access = private)
        is_viable;  % bool
        category;
        story;
        kw_pred;  % "skip", "fine", "great"
    end
    
    methods
        
        function obj=NewsEvent(iqml_news)
            % Constructor
            % NewsEvent(iqml_news)
            obj.headline = iqml_news.Text;
            obj.symbols = iqml_news.Symbols;
            obj.release_time = datetime(num2str(iqml_news.Timestamp),'InputFormat','yyyyMMddHHmmss');
            obj.id = iqml_news.ID;
            obj.source = iqml_news.Source;
            obj.last_mention = "not specified on construction";  % default
            
            obj.start_price = [];
            if any(contains(fields(iqml_news), 'Start_Price'))
                obj.start_price = iqml_news.Start_Price;
            end
            
            if any(contains(fields(iqml_news), 'last_mention'))
                obj.last_mention = iqml_news.last_mention;
            end
            
            obj.back_test = false;
            if any(contains(fields(iqml_news), 'back_test'))
                obj.back_test = iqml_news.back_test;
            end
            
            obj.story = [];  % initialize to empty
            if any(contains(fields(iqml_news), 'Story'))
                obj.story = iqml_news.Story;
            end
            obj.is_viable = [];
            obj.category = [];
            obj.kw_pred = [];
            obj.viability_status = "UNKNOWN";
        end
        
        % SETTERS
        
        function set_start_price(obj, p)
            obj.start_price = p;
        end
        
        % ACCESSORS
                
        function result=viable(obj, sd)
            % Input: SecurityData object (if first call of function)
            % Output: true if the event is for one symbol that fits minimum
            % qualifications of a stock that I'd consider trading
            result = obj.is_viable;
            if isempty(result)
                % First time function has been called
                if ~exist('sd', 'var')
                    error("NewsEvent.viable(): Must supply SecurityData object for first query of viability.")
                end
                [result, status] = obj.determine_viability(sd);
                obj.is_viable = result;
                obj.viability_status = status;
            end
        end
        
        function result=get_category(obj, mdl)
            % Input: CategoryModel object (if first call of function)
            % Output: category
            result = obj.category;
            if isempty(result)
                % First time function has been called
                if ~exist('mdl', 'var')
                    error("NewsEvent.get_category(): Must supply CategoryModel object for first query of category.")
                end
                result = mdl.predict(obj.headline);
                obj.category = result;
            end
        end
        
        function pred=simple_kw_pred(obj)
            % Get simple keyword prediction. OK to call repetitively
            if isempty(obj.kw_pred)
                obj.kw_pred = obj.calc_simple_kw_pred();
            end
            pred = obj.kw_pred;                
        end
        
        function result=time_since_mention(obj)
            % Return the duration since the last article came out that
            % mentions this symbol, or inf if last mention is unknown
            if isempty(obj.last_mention)
                result = days(inf);
            else
                result = obj.release_time - obj.last_mention;
            end
        end
        
        function hl=get_headline(obj)
            % For prediction
            hl = obj.headline;
        end
        
        function result=highlight_symbol(obj,sym)
            % Print event and return true iff it contains the given symbol
            result = any(contains(obj.symbols, upper(sym)));
            if result
                disp(obj)
            end
        end                
    end
    
    methods (Access = private)
        function pred=calc_simple_kw_pred(obj)
            % Return true if keywords are present that would suggest this
            % event is not bio OR that it's bearish. The negation of this
            % is ~A & ~B
            strA = obj.headline;
            s = size(strA);
            pred = repmat("",s(1),1);  % string array
            verb = ["reports", "presents", "announces", "initiates", "outlines"];
            noun = ["complet", "results", "resu...", "resul...", "result...", ...
                "data", "safety", "study"];
            pos = ["positiv", "beneficial", "reduces risk", "reduced risk", "excellent"...
                ,"safe ","improve","breakthrough","effective", "meets end"...
                ,"success"];
            neg = ["did not meet", "stops", "fail", "did not succeed", "no benefit", "not work"...
                ,"not benefit", "plunge", "discontinue", "will not"];%, "to be present", "to present"];
            nonbio = ["stock", "surge", "shares", "company", "aquire", "aquisition", "plans"];

            % Returns a logical array corresponding to whether each input event
            % contains any of the strings in the input keyphrase array.
            % input: list of keyphrase triggers, list of documents to check against
            function logI = containsAny(triggers, strA)
                s = size(strA);
                logI = repmat(0,s(1),1);
                for i=1:triggers.length
                    keyphrase = lower(triggers(i));  % keyphrase
                    logI = logI | contains(lower(strA), keyphrase);
                end
            end

            verbI = containsAny(verb, strA);
            nounI = containsAny(noun, strA);
            posI = containsAny(pos, strA);
            negI = containsAny(neg, strA);
            nonbioI = containsAny(nonbio, strA);

            %pred( posI & verbI & nounI ) = "long";
            %pred( negI ) = "short";
            if ~negI && ~nonbioI
                pred = "fine";  % avg/med = +4.3%/+1.5%
                if posI && verbI && nounI
                    pred = "great";  % avg/med = +14%/+21%
                end
            else
                pred = "skip";
            end
        end
        
        function [viable, v_status]=determine_viability(obj, sd)
            % returns true if the event is for one symbol that fits minimum
            % qualifications of a stock that I'd consider trading
            v_status = "VIABLE";
            viable = true;
            % filter by amount of relevant tickers
            if length( obj.symbols ) ~= 1
                v_status = "WRONG SYMBOL COUNT";
                viable = false;
            end
            % filter out non-euquities and non-nasdaqs
            if viable
                v_status = "WRONG SECURITY";
                viable = sd.is_viable( obj.symbols );
            end 
            % check if latency is ok
            if ~obj.back_test
                lat = datetime - obj.release_time;
                if lat > 3
                    warning(strcat("Latency is ",string(lat),"sec for event:"))
                    disp(obj)
                end
                if lat > 10
                    v_status = "LATENCY ABOVE 10s";
                    viable = false;
                end
            end
            % filter by language (this part takes the longest...10-20ms)
            if viable
                hl = erasePunctuation(obj.headline);
                lan = corpusLanguage(hl);
                if lan ~= "en"
                    v_status = "NOT ENGLISH";
                    viable = false;
                end
            end
        end
    end
    
    methods (Access = protected)
        % Override part of Matlab's "toString" equivalent    
        function propgrp = getPropertyGroups(obj)
            if ~isscalar(obj)
                propgrp = getPropertyGroups@matlab.mixin.CustomDisplay(obj);
            else
                release_t = strcat( string(obj.release_time), " ET" );
                release_t_pst = strcat( string(obj.release_time - hours(3)), " PST" );
                mention_t = strcat( string(obj.last_mention), " ET" );
                propList = struct('headline',obj.headline,...
                    'symbols',string(obj.symbols),...
                    'start_price',string(obj.start_price),...
                    'release_time',release_t,...
                    'release_t_pst',release_t_pst,...
                    'last_mention',mention_t,...
                    'source',obj.source,...
                    'id',string(obj.id));
                propgrp = matlab.mixin.util.PropertyGroup(propList);
            end
        end
    end
    
end

function tt()
    t = toc;
    disp(t)
    if t > .1
        disp('bad-----------')
    end
    tic
end 


    

