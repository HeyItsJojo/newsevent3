classdef Broker < handle & matlab.mixin.CustomDisplay
    % IB TWS handle
    % Warning, if IB TWS loses connection, there will be indefinite
    % blocking in its functions--I don't have an external timeout. 
    % This isn't designed to keep working properly after manual portfolio
    % changes. Stop program before making manual changes.
    % Note: always "Broker.delete" before using again. Otherwise, you'll
    % have to restart MATLAB
    
    % Must have >$25k in account to engage in PDT ("pattern day trading" = more than 3 day trades / 4 days)
    
    % Use methods(ib.Handle) to see available TWS methods
    
    % Use "doc NewsEvent" to get overview
    
    % Inheritance:
    %   handle - for making the class behave like I'm used to
    %   CustomDisplay - for overriding some "toString" methods
        
    properties (Constant)
        %MAX_ACTIONS = 50;  % max actions per second allowed by TWS API
        
        %MAX_PARALLEL_TRADES = 1;  % max amount of different symbols to trade at once
        
        %SELL_STOP = -2;  % Percent under trade price for stop sell order
        
        MIN_RATE = 15;  % (Hz) fastest allowed rate for running main functions
        
        MAX_WAIT_TIME = 5;  % (sec) after this much time of not running main functions, run once
        
        SELL_TRAILING = 3;  % Trailing percent sell margin
        
        MAX_RATE_MSG = "Max rate of messages per second has been exceeded"; % just part of it
        
        PORT = 7497;  % 4001 for ibgw, 7497 for TWS
                
        PAPER_ACCTNUM = 'DU1828038';
        
        LIVE_ACCTNUM = 'U3437118';
    end
    
    properties (SetAccess = private)
        ib;  % Interactive Brokers TWS Handle
        contracts;  % store created contracts for later use
        balanced;  % the IB data isn't always accurate immediately after changes are made, causing double sells. This map will keep track of what has a sell order in. If true, that means no need to sell again. If false, a sell order will be allowed. The underlying issue may be fixed now that I use permId instead of orderID in get_orders. I think presubmitted orders have an ID of 0, so I was deleting them like duplicates.
        % On start: check for positions without sell orders and unfilled
        % buy orders.
        % If unfilled buy order, keep checking until filled. Else, don't
        % check until a change is made by a function being called.
        should_check;  % true if we should check for new filled stocks to sell
        %positions; % map of symbols to events of unfinished positions
        last_update;
        conn_err_tgl;  % a toggle that becomes true when a connection error is found, so i can print once only
        acctnum;
        port;
        is_paper;
    end
    
    methods
        
        function obj=Broker()
            % CONSTRUCTOR
            
            obj.port = obj.PORT;
            disp(strcat("Initiating ibtws on port ",string(obj.port),"..."))
            obj.ib = ibtws('',obj.port);
            disp("Done")
            obj.contracts = containers.Map('KeyType', 'char', 'ValueType', 'any');
            obj.balanced = containers.Map('KeyType', 'char', 'ValueType', 'any');
            %obj.positions = containers.Map('KeyType', 'char', 'ValueType', 'any');
            obj.should_check = true;  % initial call should check portfolio
            obj.last_update = datetime - hours(1);
            obj.conn_err_tgl = false;
        end
        
        function main(obj)
            % Not a loop. Should be called recurrently from an external loop. Maintains portfolio.
            
            % maybe todo: should i also set a timer that assures should_check is
            % true every x seconds? This would help if a submitted order is
            % not detectedm, making should_check = false.
            
            % TODO: add code for handling and updating positions
            % differently for regular-trading-hours vs. ext-trading-hours
            % TODO: maybe implement wrappers/message-handling for
            % orderExecution functions, at least for max-rate. Then I can
            % have this kernel run faster maybe... though reconnecting
            % takes some time
            % TODO: implement my own trailing stop function that uses a
            % limit to execute so that it works in ETH
            
            % Limit update rate (15hz calculated via avg. of 3 IB operations in
            % update_sells)
            time_since_check = milliseconds(datetime - obj.last_update);
            min_wait = time_since_check > 1000/obj.MIN_RATE;  % minimum wait time (for limiting rate)
            % Also ensure a minimum update rate is maintained
            max_wait = time_since_check > 1000*obj.MAX_WAIT_TIME;  % make sure i check every now and then
            
            % if we are within check rate-limits or if we're monitoring something
            if max_wait || (obj.should_check && min_wait)
                obj.update_sells();  % this also updates should_check (~50ms if no order submitting)
                obj.last_update = datetime;
            end
        end
            
        
        function delete(obj)
            % DESTRUCTOR -- This function is called implicitly by matlab before it garbage collects a Broker object.
            obj.close()
            disp("Broker object destructed")
        end
        
        function close(obj)
            % Disconnect the connection handle to Interactive Brokers
            close(obj.ib);
        end
        
        function n_shares=buy(obj, ev, aprox_usd)
            % Submit order to purchase aprox_usd of stocks and set 
            % should_check to true. Return number of shares specified in 
            % order (0 = no order submitted). The amount of shares is
            % calculated by making the total price as close to, but not 
            % more, than aprox_usd. However, the actual order price may be 
            % higher than the current stock price--making the total cost 
            % higher than aprox_usd.
            
            % Extract Data
            sym = upper( string(ev.symbols) );
            p = ev.start_price;
            n_shares = floor(aprox_usd/p);
            if n_shares > 0
                % Check Input
                err_msg = strcat( "Broker: Critical error for symbol ", sym );
                assert( ~isempty(p), err_msg );
                assert( p > 0, err_msg );
                assert( length( sym ) == 1, err_msg );
                %assert( ~isKey(obj.positions, sym), err_msg );  % todo: finish this check
                % TODO: assert that i don't already have open orders for this symbol
                % Print Message
                msg = strcat("SUBMIT BUY ",string(n_shares)," ",sym," -- last: ",string(p));
                obj.tradeDisp(msg); 
                % Create IB Contract
                contr = obj.contract(sym);
                % Create IB Order
                %{
                ibOrder = obj.ib.Handle.createOrder;
                ibOrder.action = 'BUY';
                ibOrder.totalQuantity = n_shares;
                ibOrder.orderType = 'MKT';  % TODO: refine order type
                ibOrder.timeInForce = 'GTC';  % time-in-force
                id = obj.get_orderid();
                subP = p;
                %}
                ibOrder = obj.ib.Handle.createOrder;
                ibOrder.action = 'BUY';
                ibOrder.totalQuantity = n_shares;
                ibOrder.orderType = 'LMT';
                ibOrder.timeInForce = 'GTC';  % time-in-force
                ibOrder.outsideRth = true;
                % ibOrder.lmtPrice specified in submit_var_res_order
                orderPercentDiff = .5;  % stock price plus this percent change
                % Submit Order
                disp("Submiting order...")
                %obj.ib.createOrder(contr, ibOrder, id); % todo: create handler, @ibExampleOrderEventHandler)
                try 
                    subP = obj.submit_var_res_order(contr, ibOrder, p, orderPercentDiff); % submitted price 
                catch err
                    % debug
                    warning("critical order placement error. Trying again")
                    disp(err)
                    send_text_message('4255309554', 'xfinity', "Critical order placement error. Trying again...")
                    disp("Associated event:")
                    disp(ev)
                    disp(aprox_usd)
                    disp("Trying again...")
                    subP = obj.submit_var_res_order(contr, ibOrder, p, orderPercentDiff);
                    % If this doesn't fix, try changing line of createOrder.m:
                    %c.Handle.placeOrderEx(int32(orderid), s, order, c.Handle.createTagValueList)
                end
                disp(strcat("Done. Placed order @",string(subP)));
                % Mark that the order should be monitors until a sell order
                % is placed
                obj.should_check = true;
                obj.balanced(upper(sym)) = false;
                % Attach New Data
                %{
                st = struct();
                st.event = ev;
                st.start_price = p;
                st.id = id;
                st.contract = contr;
                obj.positions(sym) = st; 
                %}
            end
        end
        
        function update_sells(obj)  % todo: add timeout or something because orders just blocks forever if there's nothing to return.
            % Check portfolio for owned shares. For any shares which
            % don't have an associated sell order, submit one. Also, update
            % should_check.
            % Position < 0 for short and > 0 for long
            % Order < 0 for sell order and > 0 for buy order
            % BALANCE = LONG POSITIONS - SHORT POSITIONS - SELL-ORDERS + BUY-ORDERS
            % FILLED = (LONG POSITIONS + SHORT POSITIONS) > (SELL ORDERS + BUY ORDERS)
            
            obj.should_check = false;
            
            % Tally/trade-info management
            balances = containers.Map('KeyType', 'char', 'ValueType', 'any');
            function balances = add_to_balance(balances, s)
                % For keeping tally of shares owned/selling
                % Must always enter a valid price. If updating price, only
                % do so if overwrite is true.
                if isKey(balances, s.sym)
                    % There WILL be a price if I am here
                    old_s = balances(s.sym);
                    new_s = struct();
                    % Sum
                    new_s.position = old_s.position + s.position;  % sum
                    new_s.order = old_s.order + s.order;
                    % Extrapolate
                    new_s.balance = new_s.position + new_s.order;
                    new_s.filled = abs(new_s.position) > abs(new_s.order);
                    % update price
                    if s.price > 0 || s.price == -1
                        if s.overwrite
                            % Then overwrite
                            new_s.price = s.price;  % use most recent price
                        else
                            new_s.price = old_s.price;
                        end
                    else
                        % Not good--this shouldn't happen
                        error(strcat( s.sym, ' is $', s.price, '/share' ))
                    end
                    balances(s.sym) = new_s;
                else
                    new_s = s;
                    % Extrapolate
                    new_s.balance = s.position + s.order;
                    new_s.filled = abs(new_s.position) > abs(new_s.order);
                    % Update
                    balances(s.sym) = new_s;
                end
            end
            
            % Error flag
            conn_err = false;
            
            % Tally current owned stocks
            p = obj.get_portfolio();
            if strcmpi(p, "connection error")  % this is logic for telling if it's -1 or not
                conn_err = true;
            end
            if ~conn_err && ~isempty(p) && ~isempty(p.contract)
                for i = 1:length(p.contract)  % struct is the transpose of subPected
                    sym = p.contract{i}.symbol;
                    num_shares = p.position{i};
                    price = p.averageCost{i}; % todo
                    s = struct(...
                        'sym', sym,...
                        'position', num_shares,...
                        'order', 0,...
                        'overwrite', true,...
                        'price', price);
                    balances = add_to_balance(balances, s);
                end
            end
            
            % Factor-in current sell orders
            o = obj.get_orders();
            if strcmpi(o, "connection error")
                conn_err = true;
            end
            if ~conn_err && ~isempty(o)
                for i = 1:length(o)
                    ord = o(i);
                    if isempty(ord)
                        continue
                    end
                    try
                        shouldContinue = ~strcmp(ord.Type, 'openOrderEx') || isempty(ord.contract);
                    catch err
                        warning('Dont know why booleans didnt work for:')
                        disp(ord)
                        continue
                    end
                    if shouldContinue
                        continue
                    end
                    % Process order
                    sym = ord.contract.symbol;
                    if strcmp( ord.order.action, 'SELL' )
                        unfilled_sell = ord.order.totalQuantity;
                        amt_filled = ord.order.filledQuantity;
                        if amt_filled > 1.7976e+308
                            amt_filled = 0;
                        end
                        unfilled_sell = unfilled_sell - amt_filled;
                        % add to balance
                        s = struct(...
                            'sym', sym,...
                            'position', 0,...
                            'order', unfilled_sell*-1,...
                            'overwrite', false,...
                            'price', -1);
                        balances = add_to_balance(balances, s);
                    elseif strcmp( ord.order.action, 'BUY' )
                        % Should keep watching for this to get filled
                        obj.should_check = true;
                        amt = ord.order.totalQuantity;
                        price = ord.order.trailStopPrice;  % not sure if this is needed at all
                        s = struct(...
                            'sym', sym,...
                            'position', 0,...
                            'order', amt,...
                            'overwrite', false,...
                            'price', price);
                        balances = add_to_balance(balances, s);
                    end
                end
            end
            
            if conn_err
                if ~obj.conn_err_tgl
                    % Warning message on first disconnect
                    warning("Connectivity error. Not updating sells.")
                end
                obj.conn_err_tgl = true;
                obj.should_check = true;
            else
                if obj.conn_err_tgl
                    % Message on first connection after disconnect
                    warning("Connectivity regained.")
                end
                obj.conn_err_tgl = false;
                
                % --- Go through balances map and execute decisions ---
                kk = keys(balances);
                for i = 1:length(kk)
                    sym = upper(kk{i});
                    position = balances(sym).position;
                    order = balances(sym).order;
                    balance = balances(sym).balance;
                    filled = balances(sym).filled;
                    if ~isKey(obj.balanced, sym)
                        % This happens when there are pre-existing
                        % positions. If they are balanced, n_shares
                        % should be accurate in this case so the balanced
                        % flag isn't needed.
                        obj.balanced(sym) = false;
                    end
                    
                    verbose = false;  % for debugging/testing
                    if verbose
                        disp(sym)
                    end
                    
                    if position > 0 && order > 0
                        % a buy order for already bought stock --- keep
                        % watching and wait to sell
                        if verbose
                            disp('a buy order for already bought stock')
                        end
                        obj.should_check = true;
                    elseif order < 0 && balance < 0
                        % a short order -- cancel it
                        if verbose
                            disp('cancel entire sell-short order')
                        end
                        obj.cancel_orders(sym);
                    elseif balance > 0 && filled% ~obj.balanced(sym)
                        % unsettled long position -- sell after rise.
                        % Submit sell orders
                        if verbose
                            disp('unsettled long pos -- submit trailing sell')
                        end
                        contr = obj.contract(sym);
                        % Get Fill Price
                        fill_price = balances(sym).price;
                        % Create Stop Sell Order
                        %{
                        ibOrder = ib.Handle.createOrder;
                        ibOrder.action = 'SELL';
                        ibOrder.totalQuantity = 4;
                        ibOrder.orderType = 'STP';
                        ibOrder.auxPrice = obj.setPDiff(fill_price,-2);  % stop price
                        ibOrder.triggerMethod = 2;  % 2 is 'last' for stop orders
                        ibOrder.timeInForce = 'GTC';
                        ibOrder.outsideRth = true;
                        id = obj.get_orderid();
                        %}
                        % Create Trailing Stop Order
                        ibOrder = obj.ib.Handle.createOrder;
                        ibOrder.action = 'SELL';
                        ibOrder.orderType = 'TRAIL';
                        ibOrder.totalQuantity = abs(balance);
                        ibOrder.trailingPercent = obj.SELL_TRAILING;
                        ibOrder.triggerMethod = 2;  % 2 is 'last' for stop orders
                        ibOrder.timeInForce = 'GTC';
                        ibOrder.outsideRth = true;  % works for limit or stop-limit orders
                        id = obj.get_orderid();
                        % Print
                        msg = strcat("SUBMIT TRAIL STOP SELL: ", string(abs(balance)),...
                            " ", sym, " @", string(obj.SELL_TRAILING), "%");
                        obj.tradeDisp(msg); 
                        % Submit
                        obj.ib.createOrder(contr, ibOrder, id);
                        disp("Order submitted.")
                        obj.balanced(sym) = true;
                    elseif balance < 0 && filled
                        % unsettled short positions -- mistake: buy back now
                        if verbose
                            disp('submit market buy-back')
                        end
                        contr = obj.contract(sym);
                        % Create Cover Buy Order
                        ibOrder = obj.ib.Handle.createOrder;
                        ibOrder.action = 'BUY';
                        ibOrder.orderType = 'MKT';
                        ibOrder.totalQuantity = abs( balances(sym).balance );
                        ibOrder.triggerMethod = 2;  % 2 is 'last' for stop orders
                        ibOrder.timeInForce = 'GTC';
                        ibOrder.outsideRth = true;  % works for limit or stop-limit orders
                        id = obj.get_orderid();
                        % Print
                        msg = strcat("SUBMIT COVER-BUY: ", string(abs(balances(sym).balance)),...
                            " ", sym);
                        obj.tradeDisp(msg); 
                        % Submit
                        obj.ib.createOrder(contr, ibOrder, id);
                        disp("Order submitted.")
                        %obj.balanced(sym) = true;
                    elseif balance > 0 && ~filled
                        % unfilled long order -- keep watching
                        if verbose
                            disp('unfilled long order -- keep watching')
                        end
                        obj.should_check = true;
                    elseif balance < 0 && ~filled
                        % unfilled short order -- immediately cancel
                        if verbose
                            disp('unfilled short order -- cancel')
                        end
                        obj.cancel_orders(sym);
                    elseif balance == 0 && ~filled
                        if verbose
                            disp('do nothing and stop checking')
                        end
                    end
                  
                end
            end
        end
 
        function avail_funds = get_available_funds(obj)
            % Return amount of available USD in my account for
            % non-leveraged trading
            data = obj.get_account_data();
            avail_funds = str2double( data.AvailableFunds );
        end
        
        function data = get_account_data(obj)
            % Wrapper for ib accounts() function that generates a unique
            % order id for a new order
            try
                data = accounts(obj.ib, '');
            catch err
                warning(err.message)
                obj.reconnect()
                data = accounts(obj.ib, obj.acctnum);
            end
            
            if ischar(data) || isstring(data)
                if strcmp(data, 'Not connected')
                    % This has happenned before
                    obj.handle_not_connected('get_account_data');
                    data = obj.get_account_data();
                elseif strcmp(data, 'API client has been unsubscribed from account data.')
                    % Not sure if this is needed. Added for good measure
                    data = obj.get_account_data();
                elseif contains(data, obj.MAX_RATE_MSG)
                    % Not sure if this is needed. Added for good measure
                    % According to docs, TWS will likely disconnect
                    disp('Broker.get_account_data: Max rate reached.')
                    obj.reconnect();
                    data = obj.get_account_data();
                else
                    % If it's some other message, we have a critical error
                    % and I should know
                    error(data)
                end                    
            end
            if isempty(data)
                error('Empty account_data')
            end
        end
        
        function id = get_orderid(obj)
            % Wrapper for ib orderid() function that generates a unique
            % order id for a new order
            try
                id = orderid(obj.ib);
            catch err
                warning(err.message)
                obj.reconnect()
                id = orderid(obj.ib);
            end
            
            if ischar(id) || isstring(id)
                if strcmp(id, 'Not connected')
                    % This has happenned before
                    obj.handle_not_connected('get_orderid');
                    id = obj.get_orderid();
                elseif strcmp(id, 'API client has been unsubscribed from account data.')
                    % Not sure if this is needed. Added for good measure
                    id = get_orderid(obj);
                elseif contains(id, obj.MAX_RATE_MSG)
                    % Not sure if this is needed. Added for good measure
                    % According to docs, TWS will likely disconnect
                    disp('Broker.get_orderid: Max rate reached.')
                    obj.reconnect();
                    id = obj.get_orderid();
                else
                    % If it's some other message, we have a critical error
                    % and I should know
                    error(id)
                end                    
            end
            if isempty(id)
                error('Empty ID')
            end
        end
        
        function data = get_portfolio(obj)
            % Wrapper for ib portfolio() function that handles IB messages and
            % then removes duplicates. Note that returned data can be empty.
            
            % This should be the only place in the file that calls this
            % function directly. If IB is disconnected, this will be hung
            % up indefinitely
            try 
                data = portfolio(obj.ib);
            catch err
                warning(err.message)
                obj.reconnect();
                data = portfolio(obj.ib);
            end
            
            % IB "Codes" Handler (twin to that of get_order function)
            if ischar(data) || isstring(data)
                if strcmp(data, 'Not connected')
                    obj.handle_not_connected('get_portfolio');
                    data = obj.get_portfolio();
                elseif strcmp(data, 'API client has been unsubscribed from account data.')
                    % This usually occurs once per function call.
                    % According to docs, this happens because tws is
                    % already subscribed to a different "account".
                    data = obj.get_portfolio();
                elseif contains(data, obj.MAX_RATE_MSG)
                    % According to docs, TWS will likely disconnect
                    disp('Broker.get_portfolio: Max rate reached.')
                    obj.reconnect();
                    data = obj.get_portfolio();
                elseif contains(data, "has been lost")  % Connectivity lost
                    warning(data)
                    data = "connection error";
                elseif contains(data, "connection is broken")
                    warning(data)
                    obj.reconnect();
                    data = obj.get_portfolio();
                elseif contains(data, "OK")
                    disp(datetime)
                    warning(data)
                    data = [];
                else%if ~contains(data, "is OK")  % don't care if stuff is ok
                    disp(datetime)
                    warning(data)
                    obj.reconnect();
                    data = obj.get_portfolio();
                end
            end            
        end
        
        function o = get_orders(obj, remove_duplicates)
            % Wrapper for ib orders() function that handles IB messages and
            % then removes duplicates. Note that returned data can be empty.
            % or -1. Empty due to recursion, or -1 due to connection loss.
            % When calling this function externally, remove_duplicates 
            % should be true or nonexistant.
            if ~exist('remove_duplicates', 'var')
                remove_duplicates = true;
            end
            
            % This should be the only place in the file that calls this
            % function directly.
            try
                o = orders(obj.ib, false);  % sometimes this returns duplicates. Check order IDs
            catch err
                warning(err.message)
                obj.reconnect();
                o = orders(obj.ib, false);
            end
            
            % IB "Codes" Handler (twin to that of get_portfolio function)
            if ischar(o) || isstring(o)
                if strcmp(o, 'Not connected')
                    obj.handle_not_connected('get_orders');
                    o = obj.get_orders(false);
                elseif strcmp(o, 'API client has been unsubscribed from account data.')
                    % This usually occurs once per function call.
                    % According to docs, this happens because tws is
                    % already subscribed to a different "account".
                    o = obj.get_orders(false);
                elseif contains(o, obj.MAX_RATE_MSG)
                    disp('Broker.get_orders: Max rate reached.')
                    obj.reconnect();
                    o = obj.get_orders(false);
                elseif contains(o, "has been lost")  % Connectivity lost
                    warning(o)
                    o = "connection error";
                elseif contains(o, "connection is broken")
                    warning(o)
                    obj.reconnect();
                    o = obj.get_portfolio();
                elseif contains(o, "OK")
                    disp(datetime)
                    warning(o)
                    o = [];
                else
                    warning(o)
                    obj.reconnect();
                    o = obj.get_orders(false);
                end
            end
            
            % Remove duplicates
            if ~isempty(o) && remove_duplicates
                ids_seen = [];  % unique ids  
                dup_rows = [];  % to remove
                for i = 1:length(o)
                    ord = o(i);
                    % Filter out
                    if isempty(ord)
                        continue
                    end
                    if ~isfield(ord, 'order') || isempty(ord.order)
                        continue
                    end
                    id = ord.order.permId;
                    if isempty(id)
                        continue
                    end
                    % Deal with order
                    if ismember(id, ids_seen)
                        dup_rows(end+1) = i;
                    else
                        ids_seen(end+1) = id;
                    end
                end
                o(dup_rows) = [];
            end
        end
        
        function result = cancel_orders(obj, sym)
            % Cancel all orders for the symbol "sym". Return number of orders
            % canceled, or -1 if error.
            sym = upper(sym);
           
            o = obj.get_orders();
            ids_to_cancel = [];
            % Populate IDs to cancel
            if ~isempty(o)
                for i = 1:length(o)
                    ord = o(i);
                    if isempty(ord)
                        continue
                    end
                    if ~strcmp(ord.Type, 'openOrderEx') || isempty(ord.contract)
                        continue
                    end
                    ord_sym = ord.contract.symbol;
                    ord_id = ord.orderId;%ord.order.permId;%ord.orderId;
                    if strcmpi( sym, ord_sym ) && ord_id ~= 0
                        % add order ID
                        ids_to_cancel = [ ids_to_cancel ord_id ];
                    end
                end
            end
            % Cancel the IDs
            for i = 1:length(ids_to_cancel)
                this_id = ids_to_cancel(i);
                obj.ib.Handle.cancelOrder(this_id);  % This gives no feedback
            end
            % Set output
            result = length(ids_to_cancel);
            if result > 0  % debug, todo: make it so presubmitted orders can be cancelled too. then this wont happen. They seem to have an id of 0, causing them to not be cancelable.
                msg = strcat("CANCEL ",string(result), " ORDERS FOR: ", sym);
                obj.tradeDisp(msg); 
            end
        end  
        
        %{
        function sell_order(obj, sym)
            % Submit order(s) for selling
            sym = upper(sym);
            if isKey( obj.positions, sym )
                data = obj.positions( sym );
                ev = data.ev;
                p = data.start_price;
            else
                disp(strcat("Broker: nothing to neutralize for symbol ", sym))
            end
        end
        %}
        
        %{
        function submit_buy(obj, id)
            possible order types:
                auction
                discretionary - probably use this. It's a limit order with
              amount off limit that is acceptable (but the amount off is
              invisible to the market)
                pegged-to-primary = 
                sweep-to-fill - for fastest trade. Chooses immediate best
              price and trades at that price's associated quantity
                limit
        end
        %}
        
        %{
        function neutralize_positions(obj)
            % Sell all held stock and buy back any shorted stock
        end
        %}
        
    end
    
    methods (Access = private)
        %{
        function id = get_order_id(obj, ord)
            % Input: order --- Output: ID or empty if invalid
            % Filters out a single order extracted from a the struct
            % returned by "orders()". After using this function, check
            % "isempty(id)".
            valid = true;
            id = [];
            if isempty(ord)
                valid = false;
            end
            if valid && ~isfield(ord, 'orderId')
                valid = false;
            end
            if valid
                id = ord.orderId;
            end
        end
        %}
        function reconnect(obj)
            % First close the connection (if it is connected), then reconnect
            disp("Broker: Reconnecting...")
            close(obj.ib);
            obj.ib = ibtws('',obj.port);
            disp("Done.")
        end
        
        function handle_not_connected(obj, caller)
            % Handle the "not connected" error returned by IBTWS
            % caller is a token identifying who called this function, so I
            % can trace the error
            warning(strcat('Broker: not connected to IBTWS. Caller: ',caller));
            obj.ib = ibtws('',obj.port);
        end
        
        function lmtP = submit_var_res_order(obj, ibContract, ibOrder, stockPrice, percentOff)
            % Orders won't submit if limit price is too high of resolution (too many decimals places).
            % This function repeats the submittal with decreasing
            % resolution until successful. It returns the final successful
            % price.
            % Will work at least for limit sell and limit buy orders.
            resubmit = true;
            res = 1000;  % price resolution of limit order (1/1000th)
            lmtP = [];
            while resubmit
                resubmit = false;
                % Get Order ID
                id = obj.get_orderid();
                lmtP = obj.setPDiff(stockPrice, percentOff, res);
                ibOrder.lmtPrice = lmtP;
                msg = obj.ib.createOrder(ibContract, ibOrder, id);  % SUBMIT
                if strcmp(msg, "The price does not conform to the minimum price variation for this contract.")
                    resubmit = true;
                    res = res/10;
                    if res < 1
                        error(strcat("Couldn't find adequate price resolution for stock", sym))
                    else
                        warning( strcat("Limit price, ",string(lmtP),...
                            ", is too hich of resolution for stock of price ",...
                            string(stockPrice), ". Decreasing resolution and trying again.") )
                    end
                elseif strcmp(id, 'Not connected')
                    obj.handle_not_connected('submit_var_res_order(2)')
                    resubmit = true;
                else 
                    disp('Buy message:')
                    disp(msg)
                end
            end
        end
        
        function ibContract=contract(obj,sym)
            % Get pre-made contract or create new one if needed
            if ~isKey(obj.contracts, sym)
                % Create contract (~12ms)
                contr = obj.ib.Handle.createContract;
                contr.symbol = upper(sym);
                contr.secType = 'STK';  % ("security type = stock")
                contr.currency = 'USD';
                contr.exchange = 'SMART';  % Should include this for all stocks
                contr.primaryExchange = 'ISLAND';  % "ISLAND" = NASDAQ
                % Store contract
                obj.contracts(sym) = contr;
            end
            ibContract = obj.contracts(sym);                
        end
        function tradeDisp(obj,str)
            % Print Important Message Surrounded by Blank Space
            disp(newline);
            timeDisp(str);
            disp(newline);
        end        
    end
    
    methods (Static)
        function result=setPDiff(num,diff,res)
            % Retrieve num + [diff] percent of num
            % Form of diff: (diff = 5 for 5%, diff = -7 for -7%)
            % Outputs num adjusted by [diff] percent, rounded to 1/res of
            % num, or not rounded if res isn't specified.
            if ~exist('res', 'var')
                res = 0;
            end
            
            assert(diff >= -100);
            result = (diff/100 + 1)*num;
            
            if res > 0
                N = num/res;
                dec = 0;  % number of decimals to round to to get 1/res max resolution. TODO: know exactly how many decimals I'm allowed
                while N < 1
                    N = N*10;
                    dec = dec + 1;
                end
                %disp(dec)
                result = round(result, dec);
            end
            
        end
        function portfolio_balance_demo(position, order)
            % Demo for balancer logic
            % Position < 0 for short and > 0 for long
            % Order < 0 for sell order and > 0 for buy order
            % BALANCE = LONG POSITIONS - SHORT POSITIONS - SELL-ORDERS + BUY-ORDERS
            % FILLED = (LONG POSITIONS + SHORT POSITIONS) > (SELL ORDERS + BUY ORDERS)
            % 
            balance = position + order;
            filled = abs(position) > abs(order);
            if position > 0 && order > 0
                % buy already bought stock -- keep watching and wait to
                % sell
                disp('keep checking and be ready to sell')
            elseif order < 0 && balance < 0
                % a short order -- cancel it
                disp('cancel entire sell-short order')
            elseif balance > 0 && filled
                % unsettled long position -- sell after rise
                disp('submit trailing sell')
            elseif balance < 0 && filled
                % unsettled short positions -- mistake: buy back now
                disp('submit market buy-back')
            elseif balance > 0 && ~filled
                % unfilled long order -- keep watching
                disp('set should_check = true so a sell order is immediately submitted when ready')
            elseif balance < 0 && ~filled
                % unfilled short order -- immediately cancel
                disp('cancel entire sell-short order')
            elseif balance == 0 && ~filled
                disp('do nothing and stop checking')
            end
            disp(balance)
        end
    end
    
    
end

% display string with timestamp preceding it
function timeDisp(str)
    t = string(datetime);
    disp( strcat("(", t, " PST) ", str) );
end

% Extended-Hours-Trading Limit order
% (EHT orders must be limit or stop-limit orders)
%{
ibOrder = ib.Handle.createOrder;
ibOrder.action = 'BUY';
ibOrder.totalQuantity = 1;
ibOrder.orderType = 'LMT';
ibOrder.lmtPrice = 371
ibOrder.outsideRth = true;
% use "fieldnames(ibOrder)" to see customization options
%}
    

