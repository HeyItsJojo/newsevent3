classdef SecurityData < handle
    % The main purpose of this class is to quickly and accurately determine
    % if a given symbol is a real NASDAQ stock that is not OTC or OTCBB.
    % This is the purpose of is_viable(sym).
    %
    % Currently, the class is based on the assumption that the files already
    % exist. ie, the data has already been downloaded and resaved once, and
    % with a timestamp file created.
    %
    % A security, in this case, is a tradeable financial asset. A security can
    % be an equity (stock), an option, a swap, a future, etc.
    %
    % This file not only handles the storage and updating of the security data
    % file, but it also formats and provides functions for utilizing it.
    %
    % EXAMPLE USAGE
    % 
    %sd = SecurityData(); % loads current saved data
    %e = sd.is_viable('aapl');  % check if stock is viable (nasdaq)
    %sd.update_data();  % checks for new cloud data and updates local file if needed (~3 min)
    %e = sd.is_viable('aapl'); % check again. Maybe it went out of business today?
    
    properties (Constant=true, Hidden=true)
        main_link = "http://www.iqfeed.net/symbolguide/index.cfm?symbolguide=lookup&displayaction=support&section=guide&web=iqfeed";  % link to webpage containing symbol search
        dl_link = "http://www.dtniq.com/product/mktsymbols_v2.zip";  % link for direct download of market symbol data
        dirpath = "C:\Users\jorda\Documents\Stock Trading\NewsEvent3.0\assets\symbol_lookup";
        txtfilepath = "C:\Users\jorda\Documents\Stock Trading\NewsEvent3.0\assets\symbol_lookup\mktsymbols_v2.txt";
        mvarpath = "C:\Users\jorda\Documents\Stock Trading\NewsEvent3.0\assets\symbol_lookup\mvar.mat";
    end
    
    properties (SetAccess = private)
        data;  % actual symbols data
        last_dl_dt;  % last download datetime of text file from iqfeed website
        newest_ver_dt;  % datetime of most up-to-date version of text file on website
    end
    
    methods
        
        function obj=SecurityData()
            % Constructor
            obj.last_dl_dt = [];
            obj.newest_ver_dt = [];
            obj.load_data(); 
        end
        
        function result=is_viable(obj, sym)
            % Returns true if symbol represents a NASDAQ equity
            assert( length(sym) == 1 );
            sym = strcat(upper(sym), " ");
            result = any( contains( obj.data.SYMBOL, sym ) );
        end
        
        function mkt=listed_market(obj, sym)
            % Return listed market for specified symbol (market is
            % separate from exchange)
            dat = obj.sym_dat(sym);
            mkt = strtrim( dat.LISTED_MARKET );
        end
        
        function dat=sym_dat(obj, sym)
            % Return all data about the specified symbol
            % If symbol isn't present, throw error
            sym = strcat(upper(sym), " ");
            dat = obj.data( contains(obj.data.SYMBOL, sym), : );
            assert(~isempty(dat), "This symbol may have been removed by format_data()");
        end
        
        function sym=get_random_sym(obj)
            % Returns a random viable symbol
            sz = size(obj.data.SYMBOL);
            r = round(rand*sz(1));
            sym = strtrim( obj.data.SYMBOL(r) );
        end         
                      
        function update(obj, force)
            % Update symbols file if it is out of date
            if ~exist('force', 'var')  % force is for force-updating from web
                force = false;
            end
            % extract download time if needed
            if isempty(obj.last_dl_dt)
                obj.get_download_time();
            end
            try 
                obj.peek_newest_ver();  
            catch 
                disp("SecurityData: couldn't get HTML. Trying again.")
                obj.peek_newest_ver();
            end
            if (obj.newest_ver_dt > obj.last_dl_dt) || force
                tic
                if force
                    disp("Forced update requested. Re-downloading...")
                else
                    disp("Newer symbol list available. Re-downloading...");
                end
                % Download and unzip
                unzip(obj.dl_link,obj.dirpath);
                % Save download datetime to file and to field
                fid = fopen(strcat(obj.dirpath,'\download_time.txt'),'w');
                dt = datetime;
                fwrite(fid,string(dt));
                fclose(fid);
                obj.last_dl_dt = dt;
                % Load data from text file
                disp("Loading data to Matlab...")
                data = obj.format_data( tdfread( obj.txtfilepath ) );
                obj.data = data;
                % Save loaded matlab variable for faster future access
                disp("Saving matlab variable...")
                save(obj.mvarpath,"data")
                toc
            else
                disp("Symbol list already up-to-date.");
            end
        end
        
        function reformat_data(obj)
            % Loads saved matlab var of security data, formats it, then resaves
            obj.load_data();
            data = obj.format_data(obj.data);
            save(obj.mvarpath,"data");
            obj.data = data;
        end
        
    end
    
    methods (Access = private)
        
        function dl_dt=get_download_time(obj)
            % Extract last download datetime from file (PST)
            fid = fopen(strcat(obj.dirpath,'\download_time.txt'),'r');
            dl_dt = datetime( fscanf(fid, "%c") );  % pst
            fclose(fid);
            obj.last_dl_dt = dl_dt;
        end
        
        function dat_dt=peek_newest_ver(obj)
            % Get last updated time from website
            html = webread(obj.main_link);
            s_before = '(file is .zip to provide a smaller download).  File Updated: ';
            s_after = ' central time.<B></B><BR><BR>';
            list = extractBetween(html, s_before, s_after); % central time
            dat_dt = datetime(list{1}, 'InputFormat', 'MM/dd/yyyy hh:mm:ss a') - hours(2);
            obj.newest_ver_dt = dat_dt;
        end
        
        function load_data(obj)
            % load currently saved data
            obj.data = load(obj.mvarpath).data;
        end
        
        function dat=format_data(obj, data)
            % Narrow data down from 3.2M cols to 4k
            
            % Convert to table
            if isstruct(data)
                dat = struct2table(data);
            else
                assert( istable(data) );
                dat = data;
            end
            % Delete non-equity listings
            dat.SECURITY_TYPE = string( dat.SECURITY_TYPE );
            dat( ~contains(dat.SECURITY_TYPE, "EQUITY") , :) = [];
            % Format Columns
            dat.SYMBOL = string(dat.SYMBOL);
            dat.LISTED_MARKET = string(dat.LISTED_MARKET);
            % Delete stocks traded under OTC and OTCBB markets
            dat( contains(dat.LISTED_MARKET, "OTC") , :) = [];
            dat.EXCHANGE = string(dat.EXCHANGE);
            % Delete non-nasdaq listings
            dat( ~contains(dat.EXCHANGE, "NASDAQ") , :) = [];
        end
        
    end
    
end


    

