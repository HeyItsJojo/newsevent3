% Model for Predicting News Category
% Currently using binary_cat1.mat

% 100% of the prediction time is in the actual predict function (100ms)

classdef CategoryModel < handle
    
    properties (Constant)
        models_path = "C:\Users\jorda\Documents\Stock Trading\NewsEvent3.0\Models\";
        cat_model_name = "binary_cat1";
    end
    
    % PROPERTIES
    properties (SetAccess = private)
        mdl;
        bag;
    end
    
    % PUBLIC METHODS
    methods
        
        % constructor
        function obj=CategoryModel()
            obj.load_models();
            disp("Category Model Loaded");
        end
        
        % Initial one-time workload
        function load_models(obj)
            saved_data = load( strcat( obj.models_path,...
                obj.cat_model_name, ".mat" ) );
            obj.mdl = saved_data.mdl;
            obj.bag = saved_data.bag;
        end
        
        % Predict the category
        function category = predict( obj, str )
            % "Bio" or "Other"
            docs = obj.preprocessText(str);
            x = encode(obj.bag,docs);
            category = predict(obj.mdl,x);  % (100ms avg)
        end
        
    end
    
    methods (Access = private)
        
        function documents = preprocessText(obj, input)
            %disp("Preprocessing docs")
            % Convert to lowercase.
            documents = lower(input);

            % Tokenize the text.
            documents = tokenizedDocument(documents);

            % Remove a list of stop words then lemmatize the words. To improve
            % lemmatization, first use addPartOfSpeechDetails.
            documents = addPartOfSpeechDetails(documents);

            documents_1gram = removeStopWords(documents);
            try
                documents_1gram = normalizeWords(documents_1gram,'Style','lemma');
            catch err
                % for when it throws an error, thinking it's not english
                documents_1gram = normalizeWords(documents_1gram,'Style','stem');
            end

            % Erase punctuation.
            documents_1gram = erasePunctuation(documents_1gram);

            % Remove words with 2 or fewer characters, and words with 15 or more
            % characters.
            documents_1gram = removeShortWords(documents_1gram,2);
            documents_1gram = removeLongWords(documents_1gram,15);

            % ngram
            try
                documents_ngram = normalizeWords(documents,'Style','lemma');
            catch err
                documents_ngram = normalizeWords(documents,'Style','stem');
            end
            documents_ngram = erasePunctuation(documents_ngram);
            documents_ngram = removeLongWords(documents_ngram,15);
        end
        
    end       
        
                
end