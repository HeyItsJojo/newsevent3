function out=iqmlHelpers()
    out=5
end

% symbol, datetime (yyyymmddhhmmss)
% ticks are the most detailed form quote history provided
% for a single quote, it will get the last trade of that second
function out=tickHist(sym, dtStart, dtEnd, maxcnt)
    usePar = true;
    dataD = 1;
    if ~exist('dtEnd', 'var')  % just single quote
        maxcnt = 1;
        usePar = false;
        dtEnd = dtStart;
        dataD = -1;
    else  % datetime range
        maxcnt = 10000;
    end
    
    out = IQML('history', 'dataType','ticks', 'UseParallel',usePar, ...
     'symbol',sym ,... % single-symbol date-range parallelized
     'BeginDateTime',dtStart, ...
     'EndDateTime', dtEnd, ...
     'MaxItems', maxcnt);
end

function testF()
    disp("test success");
end
