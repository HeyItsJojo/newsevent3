% prepares data, gathered by a dataGatherer,  for DataAnalyzer

function data = dataPrepare(dt)
    dir = "C:\Users\jorda\Documents\Stock Trading\NewsEvent3.0\Gathered_Data\"
    if ~exist('dt','var')
        dt = "2020_05_09_18_52_24";
    end
    path = strcat(dir,dt,"\",dt,".mat")

    % get final structure of data
    if ~exist('dat','var')
        dat = transpose( load(path).events )
    
    d = struct2table( dat );
    
    % Remove Columnn
    d = removevars(d, "HLSubjTrgrs");

    % Changing timestamp to matlab datetime
    d.Timestamp = datetime(num2str(d.Timestamp),'InputFormat','yyyyMMddHHmmss');

    % if N/A is in table
    disp('Removing N/As');
    [R,C] = size(d);
    naRows = [];
    for r = 1:R
        for c = 1:C
            if strcmp( d{r,c}, "N/A" )
                naRows(end+1) = r;
            end
        end
    end
    naRows = unique(naRows);
    % separate N/A data
    d_na = d(naRows,:);  % events containing "N/A"
    size(d)
    d(naRows,:) = [];  % other events
    size(d)

    % unpack nested cells
    disp('Unpacking nested cells')
    d.Symbols = string(d.Symbols);
    %d.TenMinH = cell2mat(d.TenMinH);
    %d.TenMinPHL = cell2mat(d.TenMinPHL);
    %d.TenMinL = cell2mat(d.TenMinL);
    try
        d.OneHourH = cell2mat(d.OneHourH);
        d.OneHourPHL = cell2mat(d.OneHourPHL);
        d.OneHourL = cell2mat(d.OneHourL);
        d.TwoHourH = cell2mat(d.TwoHourH);
        d.TwoHourPHL = cell2mat(d.TwoHourPHL);
        d.TwoHourL = cell2mat(d.TwoHourL);
        d.FourHourH = cell2mat(d.FourHourH);
        d.FourHourPHL = cell2mat(d.FourHourPHL);
        d.FourHourL = cell2mat(d.FourHourL);
    catch 
    end
    
    
    % Add Columns
    % insert title data into story but make ngrams unique
    sz = size(d);
    d.FullText = repmat("",sz(1),1);
    for i = 1:sz(1)
        title_words = split( d.Text(i) );
        title_ft = strcat( "", title_words );  % optional: tag text features for uniqueness
        title_ft = strjoin( title_ft );
        d{i,"FullText"} = strcat( title_ft," ",d.Story(i) );
    end
    d.Score = d.TenMinH;
    bad = abs(d.TenMinL) > d.TenMinH;
    d.Score(bad) = d.TenMinL(bad);

    % Remove duplicate events
    disp('Removing duplicates');
    [R,C] = size(d);
    d = sortrows(d, ["Symbols", "Timestamp"], 'ascend');    
    %There are different ways to handle articles near the same time, because their effects could 
    %have an interaction. The effects could also contradict eachother. Using all of them for the
    %same event could cause false positives/negatives
    lastDt = datetime();
    lastSym = "none";
    rows = [];
    for r = 1:R
        sym = d.Symbols(r);
        dt = d.Timestamp(r);
        if strcmp(sym,lastSym) & ((dt - lastDt) < hours(2))
            rows = [rows, r];
        end    
        lastDt = dt;
        lastSym = sym;
    end
    s = size(d);
    del = repmat(false,s(1),1);
    del(rows) = true;
    % Analyze duplicated that will be deleted
    %{
    size(rows)
    reps = d;
    reps.Del = del;
    reps = reps(:, ["Source","Symbols","Del","Timestamp","Text"]);
    openvar('reps');
    %}
    d(rows,:) = [];
    d = sortrows(d, "Timestamp", 'descend');
    data = d;
end