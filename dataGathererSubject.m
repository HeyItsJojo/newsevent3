% This file was used for getting all articles, without filtering by
% subject, and use the data to create a subject model.

%{
COMING UP:
-better-handle non-market-hours events. Rn, MyIQML marks non-hours events
as non-viable and skips them
-save search results to a file
-try older than 8 days
-show plots for search results
-choose relevant stock reaction data (distinguish buyable/sellable areas)
-get ready to collect training data
%}

%hlList = IQML('news', 'DataType','headlines', 'Timeout', 5, 'GetStory', false, 'MaxItems',10, 'UseParallel', 1, Date, datetime('today', 'TimeZone', 'ET'));
%myNow = datetime('now', 'TimeZone', 'America/New_York')

function out=dataGathererSubject()
    out = gather(178);
    %kword(["house", "there", "cat", "dog"], ["hey", "there", "house"])
end 

% todo: research the error that occured abundantly in the terminal
% todo: maybe pause gathering on (9:30-16:30 US Eastern time, Mon-Fri)

% Gather news events
% input: number of days before today to start gather
function events = gather(daysAgoFrom, daysAgoTo)
    if ~exist('daysAgoTo', 'var')
        daysAgoTo = 0;
    end
    if daysAgoFrom < daysAgoTo
        temp = daysAgoFrom;
        daysAgoFrom = daysAgoTo;
        daysAgoTo = temp;
    end
    events = struct('Source', {}, 'ID', {}, 'Symbols', {}, 'Timestamp', {}, 'Text', {}, 'Story', {});
    
    % make directory
    dt = datetime();
    folderNm = datestr(dt, 'yyyy_mm_dd_hh_MM_ss');
    dirPath = strcat('C:\Users\jorda\Documents\Stock Trading\NewsEvent3.0\Gathered_Data\', folderNm);
    [status, msg, msgID] = mkdir(dirPath);
    dirPath = strcat(dirPath, '\');
    % insert info
    fid = fopen(strcat(dirPath,'info.txt'),'w');
    fwrite(fid,strcat("daysAgoFrom-daysAgoTo: ", ...
        num2str(daysAgoFrom), "-", num2str(daysAgoTo)));
    fid = fclose(fid);
    
    for daysAgo = daysAgoTo:daysAgoFrom
        % Handle datetime
        dayNum = now + (3/24) - daysAgo; % current time in days since 0/0/0, to EST, to daysAgo
        pubDate = datetime(dayNum,'ConvertFrom','datenum'); % publish date
        % Fetch events
        disp("Fetching data")
        try
            newsList = IQML('news', 'DataType','headlines', 'Timeout', 60, 'GetStory', false, 'MaxItems',10000, 'UseParallel', 0, 'Date', pubDate);
        catch err
            disp("Error in dataGatherer when fetching NewsList")
            disp(err)
        end
        disp("Data fetched.")

        numHeadlines = length(newsList);
        start = 1;
        
        %{
        if numHeadlines > 2000
            start = 2000;
            disp('skipped');
        end
        %}    
        
        for i = start:numHeadlines
            
            if mod(i,50) == 0
                disp('-');
                dt = pubDate;
                disp( strcat("days ago: ", num2str(daysAgo), " -- article ", num2str(i), " out of ", num2str(numHeadlines), " -- date: ", num2str(dt.Month), "/", num2str(dt.Day) ) );
            end
            news = newsList(i);
            news.Symbols = upper(news.Symbols);
            %{
            % filter by amount of relevant tickers
            if length( news.Symbols ) ~= 1
                continue
            end
            % filter by existance of ticker in database
            % TODO: this fund filter filters out some viable events.
            fund = [];
            try
                fund = IQML('fundamental', 'symbol', news.Symbols);
            catch err
                % disp('catch 1')
                if ~strcmp(err.identifier,'IQML:IQFeedError')
                    disp(err)
                    error("Error caught")
                end
                continue
            end
            if isempty(fund)
                % happens when fundmental call times out (5 sec)
                disp(strcat("Couldnt get fundamental data for ",news.Symbols ...
                    ,". Continuing."));
                continue
            end
            % filter by exchange
            goodExch = ( contains( lower(fund.Exchange_Description), "nasdaq" ) && ~contains( lower(fund.Listed_Market_Description), "otc" ) );
            if ~goodExch
                continue
            end
            %}
            % todo: only nasdaq
            % filter by language
            hl = erasePunctuation(news.Text);
            lan = corpusLanguage(hl);
            if lan ~= "en"
                continue
            end
            %disp("attaching story")
            story = [];
            try
                story = IQML('news', 'DataType','story', 'ID', news.ID);
            catch err
                if strcmp(err.identifier,'IQML:IQFeedError')
                    if ismember('NO_DATA', err.message)
                        % expected error
                        story = "";
                    end
                else
                    % unexpected error
                    obj.dispSelf();
                    disp(err)
                    error("Catch 8: Unexpected Error");
                end
            end
            if isempty(story)
                disp('Empty story')
                news.Story = [];
            else
                news.Story = story.Text;
            end
            % save news
            events(end + 1) = news;
        end
    end
    
    % save fetched data to file
    if ~isempty(events)
        t = struct2table(events);
        %writetable(t, strcat(dirPath,folderNm,".txt"));
        filename = strcat(dirPath,folderNm,".mat");
        save(filename, 'events');
        disp(strcat("Saved data to folder ",folderNm));
    end

end

% input: string array, lemmatized string array
% output: boolean
% Returns true is there exists at least one lemmatized string in triggers that is also
% in tokens.
function out = kword(triggers, tokens)
    triggers = tokenizedDocument(triggers);
    triggers = normalizeWords(triggers,'Style','lemma').Vocabulary;
    out = intersect( lower(triggers), lower(tokens) );
end





%username = '486960';
%password = 'A1c3B2DNc*';
%c = iqf(username,password);
