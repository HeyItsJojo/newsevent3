%{
COMING UP:
-better-handle non-market-hours events. Rn, MyIQML marks non-hours events
as non-viable and skips them
-save search results to a file
-try older than 8 days
-show plots for search results
-choose relevant stock reaction data (distinguish buyable/sellable areas)
-get ready to collect training data
%}

%hlList = IQML('news', 'DataType','headlines', 'Timeout', 5, 'GetStory', false, 'MaxItems',10, 'UseParallel', 1, Date, datetime('today', 'TimeZone', 'ET'));
%myNow = datetime('now', 'TimeZone', 'America/New_York')

function out=dataGatherer()
    out = gather(178,2);
    %kword(["house", "there", "cat", "dog"], ["hey", "there", "house"])
end 

% todo: pause gathering on (9:30-16:30 US Eastern time, Mon-Fri)

% Gather news events
% input: number of days before today to start gather
function events = gather(daysAgoFrom, daysAgoTo)

    % Added Sep 2020
    sd = SecurityData();
    sd.update()

    disp('Loading categorical model');
    cat_model_pth = "C:\Users\jorda\Documents\Stock Trading\NewsEvent3.0\Models\binary_cat1.mat";
    saved_cat_model = load(cat_model_pth);
    mdl = saved_cat_model.mdl;
    bag = saved_cat_model.bag;
    
    if ~exist('daysAgoTo', 'var')
        daysAgoTo = 0;
    end
    if daysAgoFrom < daysAgoTo
        temp = daysAgoFrom;
        daysAgoFrom = daysAgoTo;
        daysAgoTo = temp;
    end
    events = struct('Source', {}, 'ID', {}, 'Symbols', {}, 'Timestamp', {}, 'Text', {}, 'Story', {} ...
        , 'HLSubjTrgrs', {}, 'Start_Price', {}, 'Hist_Volatility', {}, 'PE', {}, 'Avg_Volume', {} ...
        , 'Timeseries', {} ...
        , 'TenMinH', {}, 'TenMinPHL', {}, 'TenMinL', {} ...
        , 'OneHourH', {}, 'OneHourPHL', {}, 'OneHourL', {} ...
        , 'TwoHourH', {}, 'TwoHourPHL', {}, 'TwoHourL', {} ...
        , 'FourHourH', {}, 'FourHourPHL', {}, 'FourHourL', {} ...
        , 'EightHourH', {}, 'EightHourPHL', {}, 'EightHourL', {} );
    
    % make directory
    dt = datetime();
    folderNm = datestr(dt, 'yyyy_mm_dd_hh_MM_ss');
    dirPath = strcat('C:\Users\jorda\Documents\Stock Trading\NewsEvent3.0\Gathered_Data\', folderNm);
    [status, msg, msgID] = mkdir(dirPath);
    dirPath = strcat(dirPath, '\');
    % insert info
    fid = fopen(strcat(dirPath,'info.txt'),'w');
    fwrite(fid,strcat("daysAgoFrom-daysAgoTo: ", ...
        num2str(daysAgoFrom), "-", num2str(daysAgoTo)));
    fid = fclose(fid);
    
    for daysAgo = daysAgoTo:daysAgoFrom
        % Handle datetime
        dayNum = now + (3/24) - daysAgo; % current time in days since 0/0/0, to EST, to daysAgo
        pubDate = datetime(dayNum,'ConvertFrom','datenum'); % publish date
        % Fetch events
        disp("Fetching data")
        try
            newsList = IQML('news', 'DataType','headlines', 'Timeout', 60, 'GetStory', false, 'MaxItems',10000, 'UseParallel', 1, 'Date', pubDate)
        catch err
            disp("Error in dataGatherer when fetching NewsList")
            disp(err)
        end
        disp("Data fetched.")

        numHeadlines = length(newsList);
        start = 1;
        
        %{
        if numHeadlines > 2000
            start = 2000;
            disp('skipped');
        end
        %}    
        
        for i = start:numHeadlines
            
            if mod(i,50) == 0
                disp('-');
                dt = pubDate;
                disp( strcat("days ago: ", num2str(daysAgo), " -- article ", num2str(i), " out of ", num2str(numHeadlines), " -- date: ", num2str(dt.Month), "/", num2str(dt.Day) ) );
            end
            news = newsList(i);
            news.Symbols = upper(news.Symbols);
            % filter by amount of relevant tickers
            if length( news.Symbols ) ~= 1
                continue
            end
            % filter out non-equities and non-nasdaqs
            if ~sd.is_viable( news.Symbols )
                continue
            end
            % filter by language (takes the longest)
            hl = erasePunctuation(news.Text);
            lan = corpusLanguage(hl);
            if lan ~= "en"
                continue
            end
            % filter by event category (based on title)
            doc = preprocessText(hl);
            x = encode(bag,doc);
            y = predict(mdl,x);
            if y ~= "Bio"  % strcmp doesn't work on categorical arrays
                % based on training data & model, should get 10/day
                continue
            end   
            % attach other data 
            % (attaching the story takes .25 seconds)
            
            stk = MyIQML(news.Symbols, news.Timestamp);
            if ~stk.viable
                continue
            end
            
            % Try to get last price. If it fails, stock isn't
            % viable.(~300ms)
            last = get_last(news.Symbols);
            if isempty(last)
                % Means that the symbol isn't tradeable
                continue
            end
            
            %disp("attaching data")
            story = [];
            try
                story = IQML('news', 'DataType','story', 'ID', news.ID);
            catch err
                if strcmp(err.identifier,'IQML:IQFeedError')
                    if ismember('NO_DATA', err.message)
                        % expected error
                        story = [];
                    end
                else
                    % unexpected error
                    obj.dispSelf();
                    disp(err)
                    error("Catch 8: Unexpected Error");
                end
            end
            if isempty(story)
                disp('Empty story')
                stk.dispSelf();
                news.Story = "";
            else
                news.Story = story.Text;
            end
            news.HLSubjTrgrs = "N/A"; % deprecated -- todo: remove
            last = stk.lastTradeHighRes();
            if isempty(last)
                continue
            end
            news.Start_Price = last.Price;
            news.Hist_Volatility = -1;  % deprecated
            news.PE = -1;  % deprecated
            news.Avg_Volume = -1;  % deprecated
            try
                [news.TenMinH, news.TenMinPHL, news.TenMinL] ...
                    = stk.eventReaction(0,10);
                [news.OneHourH, news.OneHourPHL, news.OneHourL] ...
                    = stk.eventReaction(1,0);
                [news.TwoHourH, news.TwoHourPHL, news.TwoHourL] ...
                    = stk.eventReaction(2,0);
                [news.FourHourH, news.FourHourPHL, news.FourHourL] ...
                    = stk.eventReaction(4,0);
                [news.EightHourH, news.EightHourPHL, news.EightHourL] ...
                    = stk.eventReaction(8,0);
            catch err
                stk.dispSelf();
                warning('Couldnt get event reaction!!')
                continue
            end
            if ~stk.viable
                continue
            end
            % todo: add hour of day and daynumber of week, factor in
            % BUYABLE price
            
            % plot event
            news.Timeseries = stk.plotEvent2(dirPath);
                        
            % save news
            events(end + 1) = news;
            disp("Relevant NewsEvent added:")
            disp(news.Text)
        end
    end
    
    % save fetched data to file
    if ~isempty(events)
        t = struct2table(events);
        %writetable(t, strcat(dirPath,folderNm,".txt"));
        filename = strcat(dirPath,folderNm,".mat");
        save(filename, 'events');
        disp(strcat("Saved data to folder ",folderNm));
    end

end

% input: string array, lemmatized string array
% output: boolean
% Returns true is there exists at least one lemmatized string in triggers that is also
% in tokens.

%username = '486960';
%password = 'A1c3B2DNc*';
%c = iqf(username,password);

% FUNCTION DEFINITIONS
function out = kword(triggers, tokens)
    triggers = tokenizedDocument(triggers);
    triggers = normalizeWords(triggers,'Style','lemma').Vocabulary;
    out = intersect( lower(triggers), lower(tokens) );
end
function documents = preprocessText(input)
    %disp("Preprocessing docs")
    % Convert to lowercase.
    documents = lower(input);

    % Tokenize the text.
    documents = tokenizedDocument(documents);

    % Remove a list of stop words then lemmatize the words. To improve
    % lemmatization, first use addPartOfSpeechDetails.
    documents = addPartOfSpeechDetails(documents);
    
    documents_1gram = removeStopWords(documents);
    try
        documents_1gram = normalizeWords(documents_1gram,'Style','lemma');
    catch err
        % for when it throws an error, thinking it's not english
        documents_1gram = normalizeWords(documents_1gram,'Style','stem');
    end

    % Erase punctuation.
    documents_1gram = erasePunctuation(documents_1gram);

    % Remove words with 2 or fewer characters, and words with 15 or more
    % characters.
    documents_1gram = removeShortWords(documents_1gram,2);
    documents_1gram = removeLongWords(documents_1gram,15);
    
    % ngram
    try
        documents_ngram = normalizeWords(documents,'Style','lemma');
    catch err
        documents_ngram = normalizeWords(documents,'Style','stem');
    end
    documents_ngram = erasePunctuation(documents_ngram);
    documents_ngram = removeLongWords(documents_ngram,15);
end
function last=get_last(sym)
    % Return latest trade, or empty if stock isn't available to IQML
    try
        snap = IQML('quotes', 'symbol', sym, 'NumOfEvents', 1);
    catch err
        % last remains empty
        warning("Symbol not available to IQML.")
    end
    if exist('snap','var') && ~isempty(snap)
        last = snap.Most_Recent_Trade;
    else
        last = [];
    end
end