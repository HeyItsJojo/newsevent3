---cat1 model---
events without reactions gathered with dataGathererSubject.m 4/2020
that data parsed and labeled in dataLabelUse1.m
model created with trainer_cat1.m
model added for use in dataGatherer.m

---data analyzation and react1_model---
events & reactions gathered with dataGatherer.m and MyIQML.m, using cat1 model to filter subject
this data pre-processed in dataPrepare.m
pre-processed data analyzed in DataAnalyzer.m
pre-processed data used in trainer_react1.m to create model
