% An IQML-integrated file for retrieving price reaction around event time

% Inheriting handle changes this from a value class to a handle class,
% meaning you don't have to create a copy of the class to change a
% property.

%{
FUNCTIONS
    Public:
        MyIQML(symbol, eventtime) - constructor
        plotEvent([path to save])
        dispSelf()
        eventReaction(hour, minute)
        eventChange(hour, minute) - returns stock price percent change
        lastTrade([iqdt]) - returns data about last trade. (Last = price)
    Private:
        lastTradeHelper(iqdt,iqdt)
        lastTradeMinute(iqdt)
        nextTrade([iqdt])
        intervalHist(intrSz, start, stop, max, [latBeg])
%}

classdef MyIQML < handle
    
    properties
        symbol;
        eventTime;
        viable;
        fourHrReact;
    end
    
    methods
        
        % CONSTRUCTOR
        % et must be char array (yyyyMMddHHmmss)
        function obj=MyIQML(symbol, et)
            if ~(length(num2str(et)) == 14)
                disp(num2str(et));
                error("MyIQML Constructor: invalid datetime input");
            end
            obj.eventTime = et;
            obj.symbol = upper(symbol);
            obj.viable = true;
            obj.fourHrReact = [];
        end

        % CRUCIAL FUNCTIONS
        
        % Returns price and time of last trade before specified time (yyyyMMddhhmmss)
        % Directs to correct private lastTrade function
        % Forces tick resolution
        % out.Price = last traded price
        % This will return empty when lastTradeMinute returns empty
        function out = lastTradeHighRes(obj, iqdt)
            if ~exist('iqdt', 'var')
                iqdt = obj.eventTime;
            end
            Timestamp = "unnasigned";
            Datenum = "unnasigned";
            Price = "unnasigned";
            TotalVolume = "unnasigned";
            Symbol = "unnasigned";
            Resolution = "minute";
            data = [];
            % Try calling recursive helper for tick-resolution function
            data = obj.lastTradeHelper(iqdt, iqdt);
%             if isempty(data)
%                 pause(0.2);
%                 data = obj.lastTradeHelper(iqdt, iqdt);
%             end
            if isempty(data)
                out = [];
            else
                Resolution = "tick";
                Price = data.Last;
                Timestamp = data.Timestamp;
                Symbol = data.Symbol;
                Datenum = data.Datenum;
                TotalVolume = data.TotalVolume;
                out = table(Price, Timestamp, Datenum, TotalVolume, Symbol, Resolution);
            end
        end
        
        % input: (optional) path of folder in which to save plots
        % output: timeseries data of trade prices
        % action: Efficiently retrieves and plots event-relevant
        % timeseries price data
        function data=plotEvent2(obj, saveTo)
            if ~exist('saveTo', 'var')
                saveTo = "None";
            end
            
            et = obj.eventTime;
            m_et = datetime(num2str(et),'InputFormat','yyyyMMddHHmmss');
            %minutesBefore = 5;
            minutesAfter = 60;  % approximately how many minutes after event time to show non-precise data
            precision_window = 5; % (minutes) window of time around event time to populate with tick data
            main_d_intr_sz = 20; % (seconds) bar interval size of non-precise data
            
            %assert( obj.highResAvail(et) );
            
            function out=dtShift(base, minute_shift)
                % Get new timestamp with a minute(s) difference from base
                % for IQML history queries
                % input:
                % base - reference time ("yyyyMMddHHmmss" double)
                % minute_shift - desired minute difference from base
                % output:
                % shifted time (IQML date string)
                shifted = string( obj.subtractMinutes(et,-1*minute_shift) );
                mdt = datetime(shifted, 'InputFormat', 'yyyyMMddHHmmss');
                out = datestr(mdt, 'yyyymmddHHMMss');
            end
            
            % get shifted times
            precise_start = dtShift(et, precision_window*-1);
            precise_stop = dtShift(et, precision_window);
            start = precise_stop;
            stop = dtShift(et, 6.5*60);
            
            % get data
            data_precise = IQML('history', 'symbol', obj.symbol, 'dataType','ticks',... 
                'BeginDateTime', precise_start, 'EndDateTime',...
                precise_stop, 'MaxItems', 4000, 'Timeout', 10);
            data_main = IQML('history', 'symbol', obj.symbol, 'dataType','interval',... 
                'intervalSize', main_d_intr_sz, 'BeginDateTime', start, 'EndDateTime',...
                stop, 'MaxItems', 4000, 'Timeout', 10);
            
            % shorten data to decrease gathered_data files
            max_items = ceil(60/main_d_intr_sz)*minutesAfter;
            data_main = data_main( 1:min(length(data_main),max_items) );
            
            % concatenate data
            if isempty(data_precise)
                d_precise = [];
            else
                tries = 0;
                while isempty(data_precise) || ~any(contains(fields(data_precise), 'Last'))
                    % This happens unpredictably... try again
                    obj.dispSelf()
                    warning('No Last field. Trying again. If no error is thrown, ignore this')
                    data_precise = IQML('history', 'symbol', obj.symbol, 'dataType','ticks',... 
                        'BeginDateTime', precise_start, 'EndDateTime',...
                        precise_stop, 'MaxItems', 4000, 'Timeout', 10);
                    assert(tries < 10)
                    tries = tries + 1;
                end
%                 ydat = [];
%                 try
%                     ydat = {data_precise.Last};
%                 catch
%                     ydat = {data_precise.Close};
%                 end
                ydat = {data_precise.Last};
                d_precise = struct('t', {data_precise.Timestamp},...
                    'y', ydat);
            end
            if isempty(data_main)
                d_main = [];
            else
                tries = 0;
                while isempty(data_main) || ~any(contains(fields(data_main), 'Close'))
                    % This happens unpredictably... try again
                    obj.dispSelf()
                    warning('No Close field. Trying again. If no error is thrown, ignore this')
                    data_main = IQML('history', 'symbol', obj.symbol, 'dataType','interval',... 
                        'intervalSize', main_d_intr_sz, 'BeginDateTime', start, 'EndDateTime',...
                        stop, 'MaxItems', 4000, 'Timeout', 10);
                    assert(tries < 10)
                    tries = tries + 1;
                end
%                 try
%                     ydat = {data_main.Close};
%                 catch
%                     warning('Why is it returning tick data when i ask for interval?')
%                     ydat = {data_main.Last};
%                 end
                ydat = {data_main.Close};
                d_main = struct('t', {data_main.Timestamp},...
                    'y', ydat);
            end
            data = horzcat(d_precise, d_main);
            % (one more concatenation performed several lines later)
            
            lastTrade = obj.lastTradeHighRes(et);
            if isempty(lastTrade)
                disp("plotEvent2: lastTrade empty")
                obj.dispSelf();
                obj.viable = false;
            end
            
            % fetch
            if isempty(data)
                disp('plotEvent2: no price data near event time - not viable')
                obj.dispSelf()
                obj.viable = false;
            elseif obj.viable
                % ensure that there is price data before event time
                % NOTE: this untruthfully sets the time of the original
                % price to the time of the event (as to not stretch out the plot).
                earliest = datetime( extractBetween(data(1).t, 1, 19),'InputFormat','yyyy-MM-dd HH:mm:ss');
                if earliest > m_et
                    formed_et = datestr(m_et, 'yyyy-mm-dd HH:MM:ss');
                    toAdd = struct('t', {formed_et}, 'y', {lastTrade.Price});
                    data = horzcat(toAdd, data);
                end
                
                data = struct2table(data);
                % This field is required first for timetables. (below line
                % couldn't be done with a struct)
                data.t = extractBetween( data.t, 1, 19 );
                data.t = datetime(data.t,'InputFormat','yyyy-MM-dd HH:mm:ss');
                data = table2timetable(data);
                v = 'on';
                msg = 'Displaying figure';
                if ~strcmp(saveTo, 'None')
                    v = 'off';
                    msg = strcat("Saving figure as ",obj.symbol,num2str(obj.eventTime));
                end
                disp(msg);
                f = figure('visible',v);
                plot(data.t, data.y, 'c');
                hold on;
                plot(data.t, data.y, 'b.');
                % Add event reaction

                %{
                % (candle() Requires Financial Toolbox)
                % Data is now adequate for candle function
                %candle(data)
                %}
                %vline(m_et, 'g', "Article Published");
                line([m_et, m_et], [min(data.y), max(data.y)], 'Color','red','LineStyle','--');
                if isempty(obj.fourHrReact)
                    disp("The property, fourHrReact, is not yet defined. Retrieving now.");
                    [h,phl,l] = obj.eventReaction(4,0);
                    obj.fourHrReact = [h,phl,l];
                end
                t = strcat(obj.symbol, ": ", num2str(obj.fourHrReact()));
                title( t );  
                hold off;
                % save
                if ~strcmp(saveTo, 'None')
                    save1 = strcat("", saveTo,obj.symbol,num2str(obj.eventTime),".jpg");
                    save2 = strcat("", saveTo,obj.symbol,num2str(obj.eventTime));
                    saveas(gcf, save1);
                    saveas(gcf, save2);
                end
                %disp('MyIQML.plotEvent(): Done')
            end
        end
        
        % Uses plotEvent if no high Res data avail, and plotEvent2
        % otherwise
        function data=plotEventChooser(obj, saveTo)
            if ~exist('saveTo', 'var')
                saveTo = "None";
            end
            
            if obj.highResAvail(obj.eventTime)
                data = obj.plotEvent2(saveTo);
            else
                disp('plotEventChooser: no high res available')
                data = obj.plotEvent(saveTo);
            end
        end
        
        % Plots event based on number of ticks - could ne multiple days
        % input: (optional) path of folder in which to save plots
        function data=plotEvent(obj, saveTo)
            if ~exist('saveTo', 'var')
                saveTo = "None";
            end
            intrSz = 30;  % size of bars (seconds)
            
            % (IQFeed allows all second-intervals for 8 days and 60-second
            % intervals for 180 days)
            et = obj.eventTime;
            m_et = datetime(num2str(et),'InputFormat','yyyyMMddHHmmss');
            if (datetime - days(7)) > m_et
                intrSz = 60;
            end
            minutesBefore = 20;
            minutesAfter = 60*8;
            predata = obj.intervalHist(intrSz, "inf", et, minutesBefore*(60/intrSz) );
            postdata = obj.intervalHist(intrSz, et, "inf", minutesAfter*(60/intrSz) );
            % combine
            data = vertcat(predata,postdata);
            % fetch
            if isempty(data)
                warning('not viable')
                obj.dispSelf()
                obj.viable = false;
            else
                % prepare to plot
                %disp('MyIQML.plotEvent: Removing uneeded data')
                toRm = ["Symbol", "Datenum", "TotalVolume", "PeriodVolume", "NumberOfTrades"];
                for i=1:length(toRm)
                    data = rmfield(data, toRm(i));
                end

                data = struct2table(data);
                % This field is required first for timetables. (below line
                % couldn't be done with a struct)
                data.Timestamp = datetime(data.Timestamp,'InputFormat','yyyy-MM-dd HH:mm:ss');
                data = table2timetable(data);
                ydat = data.Close;
                v = 'on';
                msg = 'Displaying figure';
                if ~strcmp(saveTo, 'None')
                    v = 'off';
                    msg = strcat("Saving figure as ",obj.symbol,num2str(obj.eventTime));
                end
                disp(msg);
                f = figure('visible',v);
                plot(data.Timestamp, ydat, 'c');
                hold on;
                plot(data.Timestamp, ydat, 'b.');
                % Add event reaction

                %{
                % (candle() Requires Financial Toolbox)
                % Data is now adequate for candle function
                %candle(data)
                %}
                %vline(m_et, 'g', "Article Published");
                line([m_et, m_et], [min(ydat), max(ydat)], 'Color','red','LineStyle','--');
                if isempty(obj.fourHrReact)
                    warning("The property, fourHrReact, not yet defined. Retrieving now.");
                    [h,phl,l] = obj.eventReaction(4,0);
                    obj.fourHrReact = [h,phl,l];
                end
                t = strcat(obj.symbol, ": ", num2str(obj.fourHrReact()));
                title( t );  
                hold off;
                % save
                if ~strcmp(saveTo, 'None')
                    save1 = strcat("", saveTo,obj.symbol,num2str(obj.eventTime),".jpg");
                    save2 = strcat("", saveTo,obj.symbol,num2str(obj.eventTime));
                    saveas(gcf, save1);
                    saveas(gcf, save2);
                end
                
                % Prepare data to be returned
                if ~isempty(data)
                    try 
                        ydat = data.Close;
                    catch
                        ydat = data.Last;
                    end
                    data = struct('t', {data.Timestamp},...
                        'y', {ydat});
                end
            end
        end
        
        function dispSelf(obj)
            disp('----');
            disp("dispSelf: MyIQML Object with Properties:");
            disp(obj.symbol);
            disp(num2str(obj.eventTime));
            disp('----');
        end
        
        % Return highest and lowest percent changes between the first
        % post-event trade and the next [hour,min] specified.
        % Also return lowest change from initial occuring before this high.
        function [high, pre_high_low, low] = eventReaction(obj, hour, minute)
            buyTime = obj.subtractSeconds(obj.eventTime, -2);  % conservative measure
            last = obj.lastTradeHighRes(buyTime); % last trade
            start = obj.nextTrade(buyTime);  % so we know the time of the first trade
            if isempty(start) || isempty(last)
                high = 'N/A';
                pre_high_low = 'N/A';
                low = 'N/A';
                obj.viable = false;
                warning('Usually occurs because event is too recent. Havent double checked if this isnt creating any false positives')
                obj.dispSelf()
            else
                startPrice = last.Price;
                % calculate end time
                mdt = datetime(start.Timestamp,'InputFormat','yyyy-MM-dd HH:mm:ss');
                startT = str2double( datestr(mdt, 'yyyymmddHHMMss') );
                endT = obj.subtractMinutes( startT, -1*(60*hour + minute) );
                % sellable price at end
                data = obj.intervalHist(60,startT,endT,10000,true);
                if isempty(data)
                    high = 'N/A';
                    pre_high_low = 'N/A';
                    low = 'N/A';
                else
                    data = struct2table( data );
                    % append buyable price trade to beginning of data
                    newRow = table(last.Symbol, last.Timestamp, last.Datenum, ...
                        last.Price, last.Price, last.Price, last.Price, ...
                        last.TotalVolume, -1, 0, ...
                        'VariableNames', data.Properties.VariableNames);
                    data = [newRow;data];
                    low = min(data.Low);
                    [high, argmax] = max(data.High);
                    pre_high_low = min( data(1:argmax,:).Low );
                    %open = data.Open(1)
                    %close = data.Close(end)                
                    % calculate percent changes
                    high = obj.percentChange(startPrice, high);
                    pre_high_low = obj.percentChange(startPrice, pre_high_low);
                    low = obj.percentChange(startPrice, low);
                    % if there's no high, it's location is theoretically
                    % infinity, making PHL = L
                    if high == 0
                        pre_high_low = low;
                    end
                    % save data
                    if (60*hour + minute)/60 == 4
                        obj.fourHrReact = [high, pre_high_low, low];
                    end
                end
            end
        end        
        
        % Returns price and time of last trade before specified time (yyyyMMddhhmmss)
        % Directs to correct private lastTrade function
        % if outside market hours, uses tick resolution
        % out.Price = last traded price
        % This will return empty when lastTradeMinute returns empty
        function out = lastTrade(obj, iqdt)
            error('Use lastTradeHighRes instead')
            if ~exist('iqdt', 'var')
                iqdt = obj.eventTime;
            end
            Timestamp = "unnasigned";
            Datenum = "unnasigned";
            Price = "unnasigned";
            TotalVolume = "unnasigned";
            Symbol = "unnasigned";
            Resolution = "minute";
            data = [];
            highRes = obj.highResAvail();
            % If within 8 days or outside trading hours
            if highRes
                % Try calling recursive helper for tick-resolution function
                data = obj.lastTradeHelper(iqdt, iqdt);
                if isempty(data)
                    pause(1);
                    data = obj.lastTradeHelper(iqdt, iqdt);
                end
                if isempty(data)
                    highRes = false;
                    warning('Empty tick data returned from lastTradeHelper')
                    disp(obj)
                end
            end
            if highRes
                %disp(strcat("lastTrade: Using tick data for ",obj.symbol,", ",num2str(iqdt)))
                Resolution = "tick";
                try
                    Price = data.Last;
                catch
                    Price = data.Close;
                    warning('Only low-res data returned for ')
                    disp(obj)
                end
                Timestamp = data.Timestamp;
                Symbol = data.Symbol;
            else
                %disp(strcat("lastTrade: Using minute data ",obj.symbol,", ",num2str(iqdt)))
                % Call function for minute-resultion function
                data = obj.lastTradeMinute(iqdt);
                if isempty(data)
                    % Try again
                    data = obj.lastTradeMinute(iqdt);
                end
                if isempty(data)
                    warning('MyIQML.lastTrade(): lastTradeMinute returned empty')
                else
                    try
                        Price = data.Open;
                    catch err
                        Price = data.Last;
                    end
                    Timestamp = {data.Timestamp};  % didn't work without this
                    Symbol = {data.Symbol};
                end
            end
            if isempty(data)
                out = [];
            else
                Datenum = data.Datenum;
                TotalVolume = data.TotalVolume;
                out = table(Price, Timestamp, Datenum, TotalVolume, Symbol, Resolution);
            end
        end
        
        function out = tenMinTick(obj)
            et = obj.eventTime;
            m_et = datetime(num2str(et),'InputFormat','yyyyMMddHHmmss');
            
            intr = 1;
            start = et;
            stop = str2double( datestr(m_et + minutes(10), 'yyyymmddHHMMss') );
            max = 1000;
            out = obj.intervalHist(intr,start,stop,max);
        end
    end
          
    methods (Access = public) % private
        
        % Return true if high resolution data (8/180 day rule) is available
        % for a request right now of data at the time, iqdt.
        function out = highResAvail(obj, iqdt)
            % If no argumnet, default to event time
            if ~exist('iqdt', 'var')
                iqdt = obj.eventTime;
            end
            mdt = datetime(num2str(iqdt),'InputFormat','yyyyMMddHHmmss');
            nowDt = datetime + hours(3);  % current eastern time
            isWeekend = ismember( weekday(nowDt), [1,7] );
            mktO = datetime(nowDt.Year, nowDt.Month, nowDt.Day, 9, 30, 0);
            mktC = datetime(nowDt.Year, nowDt.Month, nowDt.Day, 16, 30, 0);
            isTradingHour = (nowDt > mktO) && (nowDt < mktC);
            isWeekend = ismember( weekday(nowDt), [1,7] );
            eightDays = (nowDt - mdt) > days(8);
            % If within 8 days or outside trading hours
            out = eightDays || isWeekend || ~isTradingHour;
        end
        
        % gets last traded price (open) before specified time (yyyyMMddhhmmss)
        function out = lastTradeMinute(obj, iqdt)
            if ~exist('iqdt', 'var')
                iqdt = obj.eventTime;
            end
            iqdt = char(string(iqdt));
            % Using 1-second bar intervals
            intervalS = 1;
            m_dt = datetime(num2str(iqdt),'InputFormat','yyyyMMddHHmmss');
            if (datetime - days(7)) > m_dt  % 8/180 day rule
                intervalS = 60;
            end
            % should always work unless invalid stock or internet is down
            try
                out = IQML('history', 'dataType','interval', 'intervalSize',intervalS, ...
                    'symbol', obj.symbol, 'EndDateTime', iqdt, 'MaxItems', 1, ...
                    'LabelAtBeginning', 1, 'Timeout', 10 ); 
            catch err
                disp(strcat("Failed to get some IQML data for ", string(obj.symbol)))
                disp(obj.symbol)
                out = []
            end
        end
            
        % Recursive helper for tick-data resolution last trade. This is only availble up to 180 days
        % outside of trading hours.
        % Returns empty if stock is too illiquid or if it hadn't IPO'd by the
        % date specified.
        function out = lastTradeHelper(obj, dt_original, dt)
            if ~obj.highResAvail
                warning("High resolution data unvailable. Will return empty data.")
            end
            dt = char(string(dt));
            dt_original = char(string(dt_original));
            data = IQML('history', 'dataType','ticks', ...
                'symbol', obj.symbol,... % single-symbol date-range parallelized
                'EndDateTime', dt, ...
                'MaxItems', 15, ...
                'dataDirection', -1, ...
                'Timeout', 10);
            if ~isstruct(data) || isempty(data)
                obj.dispSelf();
                warning('Couldnt get lastTradeHelper for above event. Maybe it didnt IPO yet or its too illiquid')
                out = [];
            else
                tries = 0;
                while isempty(data) || ~any(contains(fields(data), 'BasisForLast'))  
                    % Sometimes, IQML returns interval data instead of tick
                    % data (idk why). Interval data doesn't have the BasisForLast
                    % field.
                    warning('No basisForLast field. Trying again')
                    obj.dispSelf();
                    data = IQML('history', 'dataType','ticks', ...
                        'symbol', obj.symbol,... % single-symbol date-range parallelized
                        'EndDateTime', dt, ...
                        'MaxItems', 15, ...
                        'dataDirection', -1, ...
                        'Timeout', 10);
                    assert(tries < 10)
                    tries = tries + 1;
                end
                data = struct2table(data);
                b = cell2mat( string(data.BasisForLast) );
                index = find( (b~='O'), 1, 'first' ); % returns index of first occurence of non-'O' element
                if isempty(index)
                    lastTs = data( end, : ).Timestamp;
                    if iscell( lastTs )
                        % It's not a cell when data has more >1 row
                        lastTs = lastTs{1};
                    end
                    mdtNew = datetime( lastTs(1:19), 'InputFormat', 'yyyy-MM-dd HH:mm:ss'); 
                    mdt_o = datetime(num2str(dt_original),'InputFormat','yyyyMMddHHmmss');
                    if (mdt_o - mdtNew) > days(5)
                        out = [];
                        obj.dispSelf();
                        warning("couldnt find any trade within 5 days")
                    else
                        % look at next older chunk of trades
                        mdtOld = datetime(num2str(dt),'InputFormat','yyyyMMddHHmmss');
                        % make sure we won't look at same data again
                        if mdtOld == mdtNew
                            mdtNew = mdtOld - seconds(1);
                        end
                        iqdt = str2double( datestr(mdtNew, 'yyyymmddHHMMss') );
                        %disp(strcat("going older for ", obj.symbol, " ", obj.eventTime))
                        out = obj.lastTradeHelper(dt_original, iqdt);
                    end
                else
                    out = data(index,:);
                end
            end
        end
        
        % Old lastTradeHelper Function
        %{
        % Recursive helper for tick-data resolution last trade. This is only availble up to 180 days
        % outside of trading hours. During trading, it's available up to 8
        % days. If high-res is unavailable, returns empty.
        function out = lastTradeHelper(obj, dt_original, dt)
            dt = char(string(dt));
            dt_original = char(string(dt_original));
            try
                % open-beginning history
                data = IQML('history', 'dataType','ticks', ...
                    'symbol', obj.symbol,... % single-symbol date-range parallelized
                    'EndDateTime', dt, ...
                    'MaxItems', 15, ...
                    'dataDirection', -1);
            catch err
                disp(err);
                obj.dispSelf();
                error("catch 6: data shouldnt be empty with infinity start");
            end
            if isempty(data)
                % usually because high-res data is unavailable
                warning("Empty tick data returned from lastTradeHelper");
                out = [];
            else
                index = [];
                if any(contains(fields(data), 'BasisForLast'))
                    data = struct2table(data);
                    b = cell2mat( string(data.BasisForLast) );
                    index = find( (b~='O'), 1, 'first' ); % returns index of first occurence of non-'O' element
                else
                    warning('No basisForLast field for')
                    disp(obj)
                    index = 1;
                end
                if isempty(index)
                    lastTs = data( end, : ).Timestamp;
                    if iscell( lastTs )
                        lastTs = lastTs{1};
                    end
                    mdtNew = datetime( lastTs(1:19), 'InputFormat', 'yyyy-MM-dd HH:mm:ss'); 
                    mdt_o = datetime(num2str(dt_original),'InputFormat','yyyyMMddHHmmss');
                    if (mdt_o - mdtNew) > days(5)
                        obj.dispSelft();
                        error("error 7: couldnt find any trade within 5 days")
                    else
                        % look at next older chunk of trades
                        mdtOld = datetime(num2str(dt),'InputFormat','yyyyMMddHHmmss');
                        % make sure we won't look at same data again
                        if mdtOld == mdtNew
                            mdtNew = mdtOld - seconds(1);
                        end
                        iqdt = str2double( datestr(mdtNew, 'yyyymmddHHMMss') );
                        out = obj.lastTradeHelper(dt_original, iqdt);
                    end
                else
                    out = data(index,:);
                end
            end
        end
        %}
                
        % Return percent change from first to second number
        function out = percentChange(obj, first, second)
            if (first <= 0) || (second <= 0)
                error("Invalid input.")
            end
            out = 100.*(second - first)/first;
        end
                
        % Return soonest trade made after specified time (60sec OHLC)
        % Returns empty if it can't get data
        % TODO: upgrade to use high res when possible
        function out = nextTrade(obj, iqdt)
            start = iqdt;
            stop = start;
            d = 1;
            data = [];
            now_mdt = datetime() - minutes(2);
            start_mdt = datetime(num2str(start),'InputFormat','yyyyMMddHHmmss');
            while( isempty(data) && (start_mdt < now_mdt) )
                if d == 1
                    stop = obj.subtractMinutes(start, -15);
                else
                    stop = obj.subtractDays(start, -1);
                end
                data = obj.intervalHist(60, start, stop, 100000, 1);
                if d == 6
                    warning('Reached end of days search')
                end
                d = d + 1;
                start = stop;
                start_mdt = datetime(num2str(start),'InputFormat','yyyyMMddHHmmss');
            end
            if isempty(data)  % todo: test this part
                out = [];
            else
                out = data(1);
            end
        end
        
        % Return full tick data of (eventTime to eventTime + 6.5hrs)
        %function out = 
        
        % --- Recursive Wrapper for IQML history function ---
        % Output: Candle history for specified interval of time. Returns
        % empty if cannot get data
        % Inputs: 
        % latBeg - labelAtBeginning
        % intrSz - "ticks" or interval size, in seconds, of data bars returned
        % start/stop - endpoints for timestamps of data. Either start, 
        % stop, or both must be a IQdatetime number ("yyyyMMddhhmmss"). 
        % Start or stop can be "inf".
        % max - Max amount of data bars to return
        function out = intervalHist(obj, intrSz, start, stop, max, latBeg)
            direction = 1;
            out = [];
            if ~exist('latBeg','var')
                latBeg = 0;
            end
            start = char(string(start));
            stop = char(string(stop));
            % TODO: handle bad input (ie, wrong types or strings)
            if strcmp(stop, "inf") && strcmp(start, "inf")
                error('intervalHist: invalid input')
            end  
            if strcmp(intrSz,"ticks")
                assert(~strcmp(stop, "inf") && ~strcmp(start, "inf"), ...
                    "cannot use open-ended limits with tick requests");
                assert( obj.highResAvail(start),strcmp(...
                    "Tick data unavailable for stock of time ",string(start)) );
            end
            % check for requests for the future
            startMdt = datetime(num2str(start), 'InputFormat', 'yyyyMMddHHmmss');
            stopMdt = datetime(num2str(stop), 'InputFormat', 'yyyyMMddHHmmss');
            nowMdt = datetime() - minutes(2);
            if ~strcmp(stop, "inf") && ~strcmp(start, "inf")
                if startMdt > stopMdt
                    error("intervalHist invalid input: reverse time requested")
                end
            end
            if ~strcmp(stop, "inf") && (nowMdt < stopMdt)
                stop = str2double( datestr(nowMdt, 'yyyymmddHHMMss') );
            end
            if ~strcmp(start, "inf") && (nowMdt < startMdt)
                warning('Requested data from the future--returning empty')
                disp(startMdt)
                disp(nowMdt)
                out = [];
            else    
                % make request
                % open-ended history
                newStart = start;
                firstI = true;
                d = 0;
                newStart_mdt = startMdt;
                while( strcmp(stop,"inf") && (length(out) < max) && ...
                        (newStart_mdt < nowMdt) && (d < 4) )
                    if  firstI
                        mins = ceil( max*(intrSz/60) + 1 ) * -1;
                        newStop = obj.subtractMinutes( newStart, mins );
                        firstI = false;
                    else
                        newStop = obj.subtractDays( newStart, -1 );
                        d = d + 1;
                    end
                    % toAdd being empty is OK
                    toAdd = obj.intervalHist(intrSz, newStart, newStop, 100000);
                    newStart = newStop;
                    newStart_mdt = datetime(num2str(newStart), 'InputFormat', 'yyyyMMddHHmmss');
                    if isempty(out)
                        out = toAdd;
                    else
                        out = vertcat(out,toAdd);
                    end
                    if length(out) > max
                        out = out(1:max);
                    end
                    if d == 4
                        disp('Reached end of days search. Usually because data is sparse')
                        %obj.dispSelf();
                    end
                end
                try
                    % tick history (is always closed-end)
                    if strcmp(intrSz, "ticks")
                        out = IQML('history', 'symbol', obj.symbol, 'dataType', 'ticks', 'BeginDateTime',...
                                start, 'EndDateTime', stop, 'MaxItems', max);
                    % open-beginning history
                    elseif strcmp(start, "inf")
                        out = IQML('history', 'dataType','interval', 'intervalSize',intrSz, ...
                            'symbol', obj.symbol, 'EndDateTime', stop, ...
                            'MaxItems',  max, 'dataDirection', direction, 'labelAtBeginning' ...
                            , latBeg, 'Timeout', 10 );
                    elseif ~strcmp(stop, "inf")
                        % closed-edges history
                        out = IQML('history', 'dataType','interval', 'intervalSize',intrSz, ...
                            'symbol', obj.symbol, 'BeginDateTime', start, ...
                            'EndDateTime', stop, 'MaxItems',  max, 'dataDirection', direction, ...
                            'labelAtBeginning', latBeg, 'Timeout', 10 );
                    end
                catch err
                    if strcmp(err.identifier,'IQML:IQFeedError')
                        if ismember('NO_DATA', err.message)
                            % expected error
                            out = [];
                        end
                    else
                        % unexpected error
                        obj.dispSelf();
                        disp(err)
                        error("Catch 2: Unexpected Error");
                    end
                end
            end
        end
        
        % input: dt number
        % output: dt number - sec
        function out=subtractSeconds(obj, iqdt, sec)
            s = num2str(iqdt);
            mdt = datetime(s, 'InputFormat', 'yyyyMMddHHmmss');
            mdt = mdt - seconds(sec); % do the arithmetic
            out = str2double( datestr(mdt, 'yyyymmddHHMMss') );
        end
        
        % input: dt number
        % output: dt number - min
        function out=subtractMinutes(obj, iqdt, min)
            s = num2str(iqdt);
            mdt = datetime(s, 'InputFormat', 'yyyyMMddHHmmss');
            mdt = mdt - minutes(min); % do the arithmetic
            out = str2double( datestr(mdt, 'yyyymmddHHMMss') );
        end
        
        % input: dt number
        % output: dt number - days
        function out=subtractDays(obj, iqdt, numDays)
            s = num2str(iqdt);
            mdt = datetime(s, 'InputFormat', 'yyyyMMddHHmmss');
            mdt = mdt - days(numDays); % do the arithmetic
            out = str2double( datestr(mdt, 'yyyymmddHHMMss') );
        end
        
        % input: matlab datetime with Y,M,d,(optional more)
        % Todo: add feature that accounts for partial open days (ie, market
        % closes at 1 pm on Nov 27 and Dec 24), add other holidays or get financial toolbox.
        function out = isHoliday(obj, m_dt)
            % source: NasdaqTrader.com
            % list of holidays--time indicates when market closes
            holidayList = datetime({ ...
                '01-Jan-2020', ...
                '20-Jan-2020', ...
                '17-Feb-2020', ...
                '10-Apr-2020', ...
                '25-May-2020', ...
                '03-Jul-2020', ...
                '07-Sep-2020', ...
                '26-Nov-2020', ...
                '27-Nov-2020', ...
                '24-Dec-2020', ...
                '25-Dec-2020' }, ...
                'InputFormat', 'dd-MMM-yyyy')
            date = datetime(m_dt.Year, m_dt.Month, m_dt.Day);
            out = any( ismember(holidayList, m_dt) );
        end
        
    end % methods
    
end   
        